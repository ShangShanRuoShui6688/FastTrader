#ifndef _BROKER_TRADING_PARAMS_H_
#define _BROKER_TRADING_PARAMS_H_
#include <string>

///EnumMarginPriceTypeType是一个保证金价格类型类型;
enum EnumMarginPrice:char
{
	///昨结算价;
	MPT_PreSettlementPrice='1',
	///最新价;
	MPT_SettlementPrice='2',
	///成交均价;
	MPT_AveragePrice='3',
	///开仓价;
	MPT_OpenPrice='4',
};

///EnumAlgorithmType是一个盈亏算法类型;
/////////////////////////////////////////////////////////////////////////
enum EnumAlgorithm:char
{
	///浮盈浮亏都计算;
	AG_All='1',
	///浮盈不计，浮亏计;
	AG_OnlyLost='2',
	///浮盈计，浮亏不计;
	AG_OnlyGain='3',
	///浮盈浮亏都不计算;
	AG_None='4',
};


///TFtdcIncludeCloseProfitType是一个是否包含平仓盈利类型
/////////////////////////////////////////////////////////////////////////
enum EnumIncludeCloseProfit:char
{
	///包含平仓盈利;
	ICP_Include='0',
	///不包含平仓盈利;
	ICP_NotInclude='2',
};

///TFtdcOptionRoyaltyPriceTypeType是一个期权权利金价格类型类型
/////////////////////////////////////////////////////////////////////////
enum EnumOptionRoyaltyPrice:char
{
	///昨结算价;
	ORPT_PreSettlementPrice='1',
	///开仓价;
	ORPT_OpenPrice='4',
};

///经纪公司交易参数;
struct BrokerTradingParams
{
	///保证金价格类型;
	EnumMarginPrice	MarginPriceType;
	///盈亏算法;
	EnumAlgorithm	Algorithm;
	///可用是否包含平仓盈利;
	EnumIncludeCloseProfit	AvailIncludeCloseProfit;
	///币种代码;
	std::string	CurrencyID;
	///期权权利金价格类型;
	EnumOptionRoyaltyPrice	OptionRoyaltyPriceType;
};
#endif