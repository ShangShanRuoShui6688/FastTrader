#ifndef _TRADING_CODE_H_
#define _TRADING_CODE_H_
#include  <string>

/////////////////////////////////////////////////////////////////////////
///TFtdcUstpTradingRightType是一个交易权限类型
/////////////////////////////////////////////////////////////////////////
enum EnumTraderRight:char
{
	///可以交易
	TR_Allow='0',
	///只能平仓
	TR_CloseOnly='1',
	///不能交易
	TR_Forbidden='2',
};

/////////////////////////////////////////////////////////////////////////
///TFtdcUstpIsActiveType是一个是否活跃类型
/////////////////////////////////////////////////////////////////////////
enum EnumActiveType:char
{
	///不活跃
	UIA_NoActive='0',
	///活跃
	UIA_Active='1',
};

//交易编码;
struct TradingCode
{
	///经纪公司编号
	std::string	BrokerID;
	///交易所代码
	std::string	ExchangeID;
	///投资者编号
	std::string	InvestorID;
	///客户代码
	std::string	ClientID;
	///客户代码权限
	EnumTraderRight	ClientRight;
	///是否活跃
	EnumActiveType	IsActive;
};

#endif
