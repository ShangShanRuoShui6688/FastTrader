//#include "stdafx.h"
#include "DataTypes.h"
#include <cstdlib>
#include <cstring>

#include <cassert>
#include <iostream>




bool TimeIsNextSection(time_t oldTime,time_t newTime,int dwType)
{
	time_t tTemp=(newTime/dwType)*(dwType)+dwType;
	if (tTemp!=oldTime)
	{
		return true;
	}
	return false;
}

bool UpdateKDATAByREPORT( KData &kd,const Tick * pReport )
{
	assert( pReport );
	if( NULL == pReport )
		return false;
	bool bNewKData=false;
	
	//换算成整点时间(以K线周期的整数倍);
	//看在不在一个周期内;
	time_t tTemp=((pReport->UpdateTime+1)/(kd.dwType))*(kd.dwType)+kd.dwType;
	//转换成字符串时间;


	if (kd.InstrumentID[0]==0)
	{
		strcpy(kd.TradingDate,pReport->TradingDay);
		strcpy( kd.InstrumentID, pReport->InstrumentID().c_str());
		strcpy(kd.ExchangeID,pReport->ExchangeID);
		kd.OpenPrice		=	pReport->LastPrice;
		kd.HighestPrice=pReport->LastPrice;
		kd.LowestPrice=pReport->LastPrice;
		kd.ClosePrice=pReport->LastPrice;

		kd.Turnover	=	0;
		kd.Volume	=	0;

		kd.LastTurnover	=	pReport->Turnover;
		kd.LastVolume	=	pReport->Volume;

		kd.TradingTime=tTemp;
	}
	else
	{
		if (strcmp(kd.TradingDate,pReport->TradingDay)!=0)
		{
			//日期不一样了,开盘;
			kd.OpenPrice=pReport->LastPrice;
			kd.HighestPrice=pReport->LastPrice;
			kd.LowestPrice=pReport->LastPrice;
			kd.ClosePrice=pReport->LastPrice;

			kd.Turnover	=	pReport->Turnover;
			kd.Volume	=	pReport->Volume;

			kd.LastTurnover	=	pReport->Turnover;
			kd.LastVolume	=	pReport->Volume;

			strcpy(kd.TradingDate,pReport->TradingDay);
			kd.TradingTime=tTemp;
			bNewKData=true;
		}
		else
		{
// 			std::string tStr=time_t2str(pReport->UpdateTime);
// 			std::cout<<tStr<<"成交:"<<(pReport->Volume-kd.LastVolume)<<","<<(pReport->Turnover-kd.LastTurnover)<<std::endl;
//			std::cout<<"行情:"<<time_t2str(pReport->UpdateTime)<<","<<pReport->LastPrice<<std::endl;
// 				<<"("<<pReport->OpenPrice<<","<<pReport->ClosePrice<<","
// 				<<pReport->HighestPrice<<","<<pReport->LowestPrice<<")"<<std::endl;
			//下一个周期开始;
			if (kd.TradingTime!=tTemp)
			{
				//新的周期;
				kd.OpenPrice=pReport->LastPrice;
				kd.HighestPrice=pReport->LastPrice;
				kd.LowestPrice=pReport->LastPrice;
				kd.ClosePrice=pReport->LastPrice;

				kd.Volume = (pReport->Volume-kd.LastVolume);
				kd.Turnover =(pReport->Turnover-kd.LastTurnover);

				kd.LastTurnover	=	pReport->Turnover;
				kd.LastVolume	=	pReport->Volume;

				kd.TradingTime=tTemp;
				bNewKData=true;
			}
			else
			{
				kd.ClosePrice		=	pReport->LastPrice;
				//最高价与最低价的计算;
				if (pReport->LastPrice>kd.HighestPrice)
				{
					kd.HighestPrice=pReport->LastPrice;
				}
				if (pReport->LastPrice<kd.LowestPrice)
				{
					kd.LowestPrice=pReport->LastPrice;
				}
				kd.Turnover	 +=	(pReport->Turnover-kd.LastTurnover);
				kd.Volume	+=	(pReport->Volume-kd.LastVolume);

				kd.LastTurnover	=	pReport->Turnover;
				kd.LastVolume	=	pReport->Volume;
			}
		}
	}
	return bNewKData;
}

int convertTick2KData( const std::vector<Tick>& ticks,std::vector<KData>& kdatas, int dwType )
{
	kdatas.clear();
	if( ticks.size() <= 0 )
		return 0;
	//转换成分钟K线数据;
	if( ktypeNone >dwType && dwType <= ktypeMin60 )
	{
		//分配内存;
		kdatas.resize(ticks.size() + 1 );

		KData	kd;
		memset( &kd, 0, sizeof(kd));
		kd.dwType=dwType;
		//第一个K线数据;
        strcpy( kd.InstrumentID, ticks.at(0).InstrumentID().c_str());
		//第一个值如果毫秒数>0,那么忽略这个值;
		//09:15:01.500，这个是约定;
		kd.TradingTime=dwType*((ticks.at(0).UpdateTime)/dwType);
		kd.OpenPrice=ticks.at(0).LastPrice;
		kd.HighestPrice=ticks.at(0).LastPrice;
		kd.LowestPrice=ticks.at(0).LastPrice;
		kd.Volume=ticks.at(0).Volume;
		kd.Turnover=ticks.at(0).Turnover;
		//遍历数据,平均价自己算，定义不一样;
		size_t i=1;
		
		for( ; i<ticks.size(); i++ )
		{
			const Tick & tick = ticks.at(i);

			time_t temp= dwType*(tick.UpdateTime/dwType);//按周期划分,时间是周期的整数倍;
			if (temp!=kd.TradingTime)
			{
				//新的K线数据!!!;
				//本次的收盘价为上一个Tick的最新价,保存上一个Tick;
				kd.ClosePrice=ticks.at(i-1).LastPrice;
				kdatas.push_back( kd );
				//为下一个Tick，做准备;
				kd.OpenPrice=tick.LastPrice;
				kd.Volume=tick.Volume;
				kd.Turnover=tick.Turnover;
				kd.TradingTime=dwType*((tick.UpdateTime)/dwType);
			}
			else
			{
				//周期之内;
				if (tick.LastPrice>kd.HighestPrice)
				{
					kd.HighestPrice=tick.LastPrice;
				}
				if (tick.LastPrice<kd.LowestPrice)
				{
					kd.LowestPrice=tick.LastPrice;
				}
				kd.Volume += tick.Volume;//tick的Volume和Turnover已经做了处理;
				kd.Turnover += tick.Turnover;
			}
		}
		//最后一个Tick要单独处理;
		if ( ticks.size()==1)
		{
			kd.ClosePrice=ticks.at(0).LastPrice;
			kdatas.push_back( kd );
		}
		else
		{
			kd.ClosePrice=ticks.at(ticks.size()-1).LastPrice;
			kdatas.push_back( kd );
		}
		return kdatas.size();
	}
	return 0;
}
