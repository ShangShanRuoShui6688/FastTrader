#ifndef _SERVER_INFO_H_
#define _SERVER_INFO_H_
#include <string>
#include <vector>
#include <cstdint>

struct ServerAddr
{
	std::string protocol;
	std::string address;
	std::uint16_t port;

	std::string to_string(bool withProtocol=true)
	{
		if (protocol.empty())
		{
			protocol="tcp";
		}
		if (withProtocol)
		{
			return protocol+"://"+address+":"+std::to_string(port);
		}
		return address+":"+std::to_string(port);
	}

	ServerAddr()
	{
		port=0;
		protocol="tcp";
	}
	ServerAddr(const std::string& protocol_addr_port)
	{
		if (!from_string(protocol_addr_port))
		{
			protocol="tcp";
			address="";
			port=0;
		}
	}
	bool from_string(const std::string& protocol_addr_port)
	{
		//进行解析;
		const std::string ptok="://";
		std::string::size_type ppos=protocol_addr_port.find(ptok);
		if (ppos==std::string::npos)
		{
			protocol="tcp";
		}
		else
		{
			protocol=protocol_addr_port.substr(0,ppos);
		}
		if (ppos==std::string::npos)
		{
			ppos=0;
		}
		else
		{
			ppos=ppos+ptok.length();
		}
		std::string::size_type apos=protocol_addr_port.find(":",ppos);
		if (apos==std::string::npos)
		{
			return false;
		}
		address=protocol_addr_port.substr(ppos,apos-ppos);
		std::string sport=protocol_addr_port.substr(apos+1);
		port=atoi(sport.c_str());
		return true;
	}
};

enum enTradeResumeType
{
	enTertRestart = 0,
	enTertResume,
	enTertQuick
};

struct ServerInfo
{
	std::string id;
	std::string name;
	std::string api; //使用的接口:CTP,Femas,LTS;
	std::string market_level;//Level1,Level2,Fast等行情类型;
	std::vector<ServerAddr> trade_server_front;
	std::vector<ServerAddr> market_server_front;
	//LTS交易接口分查询和交易部份;
	std::vector<ServerAddr> query_server_front;
	//登录超时时间;
	int market_login_timeout;
	int trader_login_timeout;
	int query_login_timeout;
	enTradeResumeType public_topic_resume_type;
	enTradeResumeType private_topic_resume_type;
	//大部分情况都只有一个地址;
	std::string first_market_addr()
	{
		if (market_server_front.empty())
		{
			return "";
		}
		return market_server_front[0].to_string();
	}
	std::string first_trading_addr()
	{
		if (trade_server_front.empty())
		{
			return "";
		}
		return trade_server_front[0].to_string();
	}
	ServerInfo()
	{
		public_topic_resume_type = enTertQuick;
		private_topic_resume_type = enTertQuick;
		market_login_timeout = 30;
		trader_login_timeout = 30;
		query_login_timeout  = 30;
		market_level="Level1";
	}
};


#endif

