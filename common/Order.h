#ifndef _ORDER_H_
#define _ORDER_H_

#include <string>
#include "Enums.h"

///输入报单;
struct InputOrder:public ErrorMessage
{
	///经纪公司代码;
	std::string	BrokerID;
	///投资者代码;
	std::string	InvestorID;
	///合约代码;
	std::string	InstrumentID;
	///报单引用;
	std::string	OrderRef;
	///用户代码;
	std::string	UserID;
	///报单价格条件;
	EnumOrderPriceType	OrderPriceType;
	///买卖方向;
	EnumDirection	Direction;
	///组合开平标志;
    EnumOffsetFlag	CombOffsetFlag[5];
	///组合投机套保标志;
    EnumHedgeFlag	CombHedgeFlag[5];
	///价格;
	double	LimitPrice;
	///数量;
	int	VolumeTotalOriginal;
	///有效期类型;
	char	TimeCondition;
	///GTD日期;
	std::string	GTDDate;
	///成交量类型;
	char	VolumeCondition;
	///最小成交量;
	int	MinVolume;
	///触发条件;
	char	ContingentCondition;
	///止损价
	double	StopPrice;
	///强平原因
	// 	TThostFtdcForceCloseReasonType	ForceCloseReason;
	// 	///自动挂起标志
	// 	TThostFtdcBoolType	IsAutoSuspend;
	// 	///业务单元
	// 	TThostFtdcBusinessUnitType	BusinessUnit;
	///请求编号
	int	RequestID;
	// 	///用户强评标志
	// 	TThostFtdcBoolType	UserForceClose;
	// 	///互换单标志
	// 	TThostFtdcBoolType	IsSwapOrder;
	//额外的参数,由报单时程序填写;
	int FrontID;
	int SessionID;
	//由报单回报填写;
	std::string OrderSysID;
	std::string ExchangeID;
	//冻结的保证金;
	double FrozenMargin;
	//冻结的手续费;
	double FrozenCommission;
	///报单日期;
	std::string	InsertDate;
	///委托时间;
	std::string	InsertTime;
};





///报单;

struct Order:public InputOrder
{
	///强平原因;
	EnumForceCloseReason	ForceCloseReason;
	///自动挂起标志;
	int	IsAutoSuspend;
	///业务单元;
	std::string	BusinessUnit;
	///本地报单编号;
	std::string	OrderLocalID;
	///交易所代码;
	//std::string	ExchangeID;
	///会员代码;
	std::string	ParticipantID;
	///客户代码;
	std::string	ClientID;
	///合约在交易所的代码;
	std::string	ExchangeInstID;
	///交易所交易员代码;
	std::string	TraderID;
	///安装编号;
	int	InstallID;
	///报单提交状态;
	EnumOrderSubmitStatus	OrderSubmitStatus;
	///报单提示序号;
	int	NotifySequence;
	///交易日;
	std::string	TradingDay;
	///结算编号;
	int	SettlementID;
	///报单编号;
	//std::string	OrderSysID;
	///报单来源;
	EnumOrderSource	OrderSource;
	///报单状态;
	EnumOrderStatus	OrderStatus;
	///报单类型;
	EnumOrderType	OrderType;
	///今成交数量;
	int	VolumeTraded;
	///剩余数量;
	int	VolumeTotal;
	///报单日期;
	//std::string	InsertDate;
	///委托时间;
	//std::string	InsertTime;
	///激活时间;
	std::string	ActiveTime;
	///挂起时间;
	std::string	SuspendTime;
	///最后修改时间;
	std::string	UpdateTime;
	///撤销时间;
	std::string	CancelTime;
	///最后修改交易所交易员代码;
	std::string	ActiveTraderID;
	///结算会员编号;
	std::string	ClearingPartID;
	///序号;
	int	SequenceNo;
	///前置编号;
	//int	FrontID;
	///会话编号;
	//int	SessionID;
	///用户端产品信息;
	std::string	UserProductInfo;
	///状态信息;
	std::string	StatusMsg;
	///用户强评标志;
	int	UserForceClose;
	///操作用户代码;
	std::string	ActiveUserID;
	///经纪公司报单编号;
	int	BrokerOrderSeq;
	///相关报单;
	std::string	RelativeOrderSysID;
	///郑商所成交数量;
	int	ZCETotalTradedVolume;
	///互换单标志;
	int	IsSwapOrder;
	//席位号;
	std::string SeatID;
};

struct OrderPred 
{
	std::string OrderSysID;
	std::string OrderRef;
	OrderPred(const std::string& sysid,const std::string& ref)
	{
		OrderSysID=sysid;
		OrderRef=ref;
	}
	bool operator()(const Order& o)
	{
		if (o.OrderSysID!=OrderSysID)
		{
			return false;
		}
		int mOrderRef=atoi(OrderRef.c_str());
		int oOrderRef=atoi(o.OrderRef.c_str());
		return mOrderRef==oOrderRef;
	}
};

/// <summary>
/// TFtdcActionFlagType是一个操作标志类型
/// </summary>
enum EnumActionFlag:char
{
	///删除;
	AF_Delete='0',
	///修改;
	AF_Modify='3'
};

/// <summary>
/// 报单操作;
/// </summary>
struct InputOrderAction
{
	/// <summary>
	/// 经纪公司代码;
	/// </summary>
	std::string BrokerID;
	/// <summary>
	/// 投资者代码;
	/// </summary>
	std::string InvestorID;
	/// <summary>
	/// 报单操作引用;
	/// </summary>
	int OrderActionRef;
	/// <summary>
	/// 报单引用;
	/// </summary>
	std::string OrderRef;
	/// <summary>
	/// 请求编号;
	/// </summary>
	int RequestID;
	/// <summary>
	/// 前置编号;
	/// </summary>
	int FrontID;
	/// <summary>
	/// 会话编号;
	/// </summary>
	int SessionID;
	/// <summary>
	/// 交易所代码;
	/// </summary>
	std::string ExchangeID;
	/// <summary>
	/// 报单编号;
	/// </summary>
	std::string OrderSysID;
	/// <summary>
	/// 操作标志;
	/// </summary>
	EnumActionFlag ActionFlag;
	/// <summary>
	/// 合约代码;
	/// </summary>
	std::string InstrumentID;
	///报单日期;
	std::string	InsertDate;
	///委托时间;
	std::string	InsertTime;
};

#endif
