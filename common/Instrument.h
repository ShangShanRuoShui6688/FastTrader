#ifndef _INSTRUMENT_H_
#define _INSTRUMENT_H_

#include <string>
#include "Enums.h"
///合约;
struct Instrument
{
	///合约代码;
	std::string	InstrumentID;
	///交易所代码;
	std::string	ExchangeID;
	///合约名称;
	std::string	InstrumentName;
	///产品代码;
	std::string	ProductID;
	///产品类型;
	EnumProductClass	ProductClass;
	///交割年份;
	//int	DeliveryYear;
	///交割月;
	//int	DeliveryMonth;
	///市价单最大下单量;
	int	MaxMarketOrderVolume;
	///市价单最小下单量;
	int	MinMarketOrderVolume;
	///限价单最大下单量;
	int	MaxLimitOrderVolume;
	///限价单最小下单量;
	int	MinLimitOrderVolume;
	///合约数量乘数;
	int	VolumeMultiple;
	///最小变动价位;
	double	PriceTick;
	///创建日;
	std::string	CreateDate;
	///上市日;
	std::string	OpenDate;
	///到期日;
	std::string	ExpireDate;
	///开始交割日;
	std::string	StartDelivDate;
	///结束交割日;
	std::string	EndDelivDate;
	///合约生命周期状态;
	//TThostFtdcInstLifePhaseType	InstLifePhase;
	///当前是否交易;
	int	IsTrading;
	///持仓类型;
	EnumPositionType	PositionType;
	///持仓日期类型;
	//TThostFtdcPositionDateTypeType	PositionDateType;
	///多头保证金率;
	double	LongMarginRatio;
	///空头保证金率;
	double	ShortMarginRatio;
	///是否使用大额单边保证金算法;
	//TThostFtdcMaxMarginSideAlgorithmType	MaxMarginSideAlgorithm;
	///跌停板价;
	double	LowerLimitPrice;
	///涨停板价;
	double	UpperLimitPrice;
	///昨结算;
	double	PreSettlementPrice;
};

///产品
struct Product
{
	///产品代码
	std::string	ProductID;
	///产品名称
	std::string	ProductName;
	///交易所代码
	std::string	ExchangeID;
	///产品类型
	EnumProductClass	ProductClass;
	///合约数量乘数
	int	VolumeMultiple;
	///最小变动价位
	double	PriceTick;
	///市价单最大下单量
	int	MaxMarketOrderVolume;
	///市价单最小下单量
	int	MinMarketOrderVolume;
	///限价单最大下单量
	int	MaxLimitOrderVolume;
	///限价单最小下单量
	int	MinLimitOrderVolume;
	///持仓类型
	EnumPositionType	PositionType;
	///持仓日期类型
	EnumPositionDateType	PositionDateType;
	///平仓处理类型
	EnumCloseDealType	CloseDealType;
	///交易币种类型
	std::string	TradeCurrencyID;
	///质押资金可用范围
	EnumMortgageFundUseRange	MortgageFundUseRange;
	///交易所产品代码
	std::string	ExchangeProductID;
	///合约基础商品乘数
	double	UnderlyingMultiple;
};
#endif