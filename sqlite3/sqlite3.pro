TEMPLATE = lib
QT -= core gui GL
TARGET = sqlite3

CONFIG(debug,debug|release){
    DEFINES +=_DEBUG
    DESTDIR  =../build/debug/
    LIBS += -L../build/debug/
} else {
    DESTDIR = ../build/release/
    LIBS += -L../build/release/
}
macx{
    QMAKE_MAC_SDK = macosx10.11
}
include(sqlite3.pri)
