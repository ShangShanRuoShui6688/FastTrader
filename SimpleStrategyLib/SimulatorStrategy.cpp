#include "SimulatorStrategy.h"
#include "../FacilityBaseLib/InstrumentData.h"
#include "../FacilityBaseLib/Container.h"
#include "../FacilityBaseLib/DateTimeHelper.h"


SimulatorStrategy::SimulatorStrategy(void)
{
}


SimulatorStrategy::~SimulatorStrategy(void)
{
}


void SimulatorStrategy::SimuReset()
{
	SimuSetStatusInit();
	m_SimuCurrentTime	=	m_opparam.GetBeginTime( );
	m_SimuCurrentCash	=	m_opparam.m_nStartAmount;
	m_SimuOpRecord.RemoveAll();
	m_SimuNextOp.RemoveAll();
	m_SimuAssetSerial.RemoveAll();
}

void SimulatorStrategy::ClearCache()
{

}

void SimulatorStrategy::SetStrategy(boost::shared_ptr<IStrategy> pStrategy)
{
	m_Strategy=pStrategy;
}

bool SimulatorStrategy::SimuRun( /*simulator_monitor* pMonitor,*/void* cookie)
{
	assert(m_Strategy!=NULL);
	// 准备数据;
	if (!PrepareData(/*pMonitor, cookie*/))
		return false;
	m_Strategy->Start();
	ClearLastIntensity();
	boost::posix_time::ptime	tmCur;
	do	
	{
		// 模拟当前时间,第一次运行时，就是模拟的起始时间;
		FromInstrumentTimeDayOrMin(tmCur,SimuGetCurrentTime()/*.ToInstrumentTimeDay()*/);
		//对每个合约,取当时的数据;

		std::vector<KDATA> kdCur;

		for (auto instIter = m_InstrumentDataMap.begin(); instIter != m_InstrumentDataMap.end();++instIter)
		{
			kdata_container& kdata = instIter->second.first->GetKData(m_opparam.m_nKType);

			if (!kdata.empty() && kdata.size()> instIter->second.second)
			{
				kdCur.push_back(kdata[instIter->second.second]);
				++instIter->second.second;
			}
		}

		if (kdCur.empty())
		{
			break;
		}

		std::sort(kdCur.begin(), kdCur.end());

		for (size_t i = 0; i < kdCur.size();++i)
		{
			boost::shared_ptr<Tick> tick = boost::make_shared<Tick>();
			convert_KDATA_to_REPORT(&kdCur[i],tick.get());
			m_Strategy->OnTick(tick);
		}
		

		// 进度显示;
		DWORD	dwProgress	=	SimuGetCurrentProgress( STRATEGY_MAX_PROGRESS );
		double	dYield		=	SimuGetCurrentYield( );

	} while( SimuGotoNextTime());	// 模拟的下一个交易日;
	//完成;
// 	if( pMonitor )
// 		(*pMonitor)( SIMULATION_PROGRESS, STRATEGY_MAX_PROGRESS, "", cookie );
	//DEBUG_PRINTF("simulator_strategy::SimuRun-->Leave");
	return true;
}

bool SimulatorStrategy::PrepareData(/* simulator_monitor* pMonitor, void * cookie*/ )
{
	auto& reg_markets=m_Strategy->GetInstruments();

	if( m_techstocks.size() == reg_markets.size() )
		return true;
	ClearCache();

	
	int	nStockCount	=	0;
	for (auto i = reg_markets.begin(); i!=reg_markets.end(); ++i)	// 读取每一只备选合约的数据;
	{
		instrument_info	info;
		if( instrument_container::get_instance().GetInstrumentInfo( i->first.c_str(), &info ) )
		{
			nStockCount	++;
			m_InstrumentDataMap[i->first] = 
				std::make_pair(m_Strategy->GetDataStoreFace()->PrepareData(i->first, InstrumentData::dataK, m_opparam.m_nKType),0);
		}

//		DWORD	dwProgress	=	(DWORD)((i+1)*STRATEGY_MAXF_PROGRESS / reg_markets.size());
// 		if( pMonitor && !(*pMonitor)( SIMULATION_PROGRESS, dwProgress, "", cookie ) )
// 		{
// 			ClearCache( );
// 			nStockCount	=	0;
// 			break;
// 		}
	}

	return true;
// 	if( pMonitor )
// 		(*pMonitor)( SIMULATION_PROGRESS, STRATEGY_MAX_PROGRESS, "", cookie );
	//DEBUG_PRINTF("simulator_strategy::PrepareData-->Enter");
	//return ( m_techstocks.size() == reg_markets.size() );
}

void SimulatorStrategy::ClearLastIntensity()
{
	for( int i=0; i<m_techstocks.size(); i++ )
	{
		CTechInstrument techstock	=	m_techstocks.ElementAt(i);

		for( int nTech=0; nTech<techstock.m_techs.size(); nTech++ )
		{
			technical_indicator * pTech	=	(technical_indicator *)techstock.m_techs[nTech];
			if( pTech )
				pTech->clear_last_intensity();
		}
	}
}

time_t SimulatorStrategy::SimuGetCurrentTime()
{
	return m_SimuCurrentTime;
}

double SimulatorStrategy::SimuGetCurrentCash()
{
	return m_SimuCurrentCash;
}
// 策略模拟：当前进度;
DWORD SimulatorStrategy::SimuGetCurrentProgress( DWORD dwProgressMax )
{
	return m_opparam.GetProgress( m_SimuCurrentTime, dwProgressMax );
}
// 策略模拟：总资产序列;
CAssetSerialContainer & SimulatorStrategy::SimuGetAssetSerial( )
{
	return m_SimuAssetSerial;
}
// 策略模拟：给定时间的总资产
double SimulatorStrategy::SimuGetAsset( time_t tmCur )
{
	double	dAsset	=	0;
	if( m_techstocks.GetSumAsset( tmCur, m_SimuStockOwn, &dAsset ) )	// 计算总资产
	{
		dAsset	+=	m_SimuCurrentCash;

		ASSETSERIAL	serial;
		memset( &serial, 0, sizeof(serial) );
		serial.time		=	tmCur;
		serial.dAsset	=	dAsset;
		serial.dCash	=	m_SimuCurrentCash;
		m_SimuAssetSerial.SortInsert( serial );
		return dAsset;
	}
	else
	{
		for( int i=m_SimuAssetSerial.GetSize()-1; i>=0; i-- )
		{
			ASSETSERIAL serial	=	m_SimuAssetSerial.ElementAt(i);
			if( serial.time <= tmCur )
				return serial.dAsset;
		}
	}
	return m_opparam.m_nStartAmount;	// 初始总资产;
}
// 策略模拟：当前收益
double SimulatorStrategy::SimuGetCurrentYield( )
{
	if( m_opparam.m_nStartAmount > 0 )
		return STRATEGY_BASEF_YIELD * SimuGetAsset(m_SimuCurrentTime) / m_opparam.m_nStartAmount;
	return STRATEGY_BASEF_YIELD;
}
// 策略模拟：当前指数上涨多少
double SimulatorStrategy::SimuGetCurrentYieldIndexPercent( )
{
	time_t	sptmBegin( m_opparam.GetBeginTime() );
	time_t	sptmNow(m_SimuCurrentTime);
	DWORD	dateBegin	/*=	sptmBegin.ToInstrumentTimeDay()*/;
	DWORD	dateNow		/*=	sptmNow.ToInstrumentTimeDay()*/;
	instrument_info info;
	instrument_container::get_instance().GetCurrentInstrument(&info);
	InstrumentData instrument;
	instrument.SetInstrumentInfo(&info);
	kdata_container & kdata	=	instrument.GetKData(m_opparam.m_nKType);
	int	nIndexBegin	=	kdata.GetAboutIndexByDate( dateBegin );
	int	nIndexNow	=	kdata.GetAboutIndexByDate( dateNow );
	if( -1 == nIndexBegin || -1 == nIndexNow )
		return 0;

	if( kdata.at(nIndexBegin).ClosePrice < 1e-4 )
		return 0;

	double	dYield	=	((double)kdata.at(nIndexNow).ClosePrice) - kdata.at(nIndexBegin).ClosePrice;
	dYield	=	dYield / kdata.at(nIndexBegin).ClosePrice;

	return dYield;
	return 0.0;
}
// 策略模拟：当前收益百分数
double SimulatorStrategy::SimuGetCurrentYieldPercent( )
{
	return ( SimuGetCurrentYield() - STRATEGY_BASEF_YIELD ) / STRATEGY_BASEF_YIELD;
}
// 策略模拟：执行下一步操作计划
bool SimulatorStrategy::SimuOperateNextop( time_t tmCur, COpRecordContainer & nextop, CTechInstrument & techstock )
{
	for( int j=0; j<nextop.GetSize(); j++ )	// 每个计划依次执行
	{
		OPRECORD &	record	=	nextop.ElementAt(j);
		if( STRATEGY_OPTYPE_BUY != record.lOpType && STRATEGY_OPTYPE_SELL != record.lOpType )
			continue;

		if( tmCur >= record.time
			&& techstock.m_info.is_equal( InstrumentData::marketUnknown, record.szCode ) )
		{
			if( techstock.IsStopTrading(tmCur) )	// 停牌吗
			{
				time_t	tmNext;
				if( m_opparam.GetNextTradeTime(tmCur, tmNext) )
					record.time	=	tmNext;	// 下次再执行
				continue;
			}

			double	dPriceOK	=	record.dSharePrice;
			if( techstock.GetPriceOK( record.lOpType, tmCur, record.dwShare, record.dSharePrice, &dPriceOK ) )	// 成交价
			{
				record.time			=	tmCur;
				record.dSharePrice	=	dPriceOK;
				//record.dRateCost	=	record.dwShare * record.dSharePrice * m_rate.GetRate(techstock.m_info);
				SimuOperate( record );
			}
			else if( STRATEGY_OPTYPE_SELL == record.lOpType )	// 如果是卖出而没有成功，则顺延下一个交易日，直至卖出
			{
				time_t	tmNext;
				if( m_opparam.GetNextTradeTime(tmCur, tmNext) )
					record.time	=	tmNext;
				if( techstock.GetClosePrice(tmCur,&dPriceOK) )
					record.dSharePrice	=	dPriceOK * m_opparam.m_dSellMulti;
				continue;
			}
			nextop.RemoveAt(j);
			j --;
		}
	}
	return true;
}
// 策略模拟：操作执行，bTimeStrict表示是否严格遵守计划时间
bool SimulatorStrategy::SimuOperate( OPRECORD record, bool bTimeStrict )
{
	if( ! m_opparam.IsInTimeZones( record.time ) )
		return false;
	if( bTimeStrict && m_SimuOpRecord.GetSize() > 0 && record.time < m_SimuOpRecord.ElementAt(m_SimuOpRecord.GetSize()-1).time )
		return false;
	instrument_info	info;
	if( strlen(record.szCode)>0
		&& ( !instrument_container::get_instance().GetInstrumentInfo( record.szCode, &info )
		|| !info.is_valid() ) )
		return false;

	double	dAmount		=	record.dwShare * record.dSharePrice;
	double	dRateCost	=	record.dRateCost;
	if( STRATEGY_OPTYPE_BUY == record.lOpType )			// 买入
	{
		if( m_SimuCurrentCash < dAmount+dRateCost )
			return false;
		if( !m_SimuStockOwn.AddInstruemnt( info, record.dwShare, record.dSharePrice ) )
			return false;
		m_SimuCurrentCash	-=	(dAmount+dRateCost);
	}
	else if( STRATEGY_OPTYPE_SELL == record.lOpType )	// 卖出
	{
		if( !m_SimuStockOwn.RemoveInstrument( info, record.dwShare ) )
			return false;
		m_SimuCurrentCash	+=	(dAmount-dRateCost);
	}
	else if( STRATEGY_OPTYPE_ADDSTOCK == record.lOpType )// 添加股票
	{
		if( !m_SimuStockOwn.AddInstruemnt( info, record.dwShare, record.dSharePrice ) )
			return false;
	}
	else if( STRATEGY_OPTYPE_REMOVESTOCK == record.lOpType )// 移除股票
	{
		if( !m_SimuStockOwn.RemoveInstrument( info, record.dwShare ) )
			return false;
	}
	else if( STRATEGY_OPTYPE_ADDCASH == record.lOpType )	// 添加资金
	{
		m_SimuCurrentCash	+=	record.dSharePrice;
	}
	else if( STRATEGY_OPTYPE_REMOVECASH == record.lOpType ) // 移除资金
	{
		if( m_SimuCurrentCash < record.dSharePrice )
			return false;
		m_SimuCurrentCash	-=	record.dSharePrice;
	}
	else
		return false;

	// Record
	m_SimuOpRecord.Add( record );
	return true;
}
// 策略模拟：进入下一个交易日;
bool SimulatorStrategy::SimuGotoNextTime( )
{
	time_t	tmNext;
	if( m_opparam.GetNextTradeTime( m_SimuCurrentTime, tmNext )
		&& m_opparam.IsInTimeZones( tmNext ) )
	{
		m_SimuCurrentTime	=	tmNext;
		return true;
	}
	return false;
}

COpParam& SimulatorStrategy::GetOpParam()
{
	return m_opparam;
}

vector<std::string> SimulatorStrategy::GetInstruments()
{
// 	if (m_Strategy)
// 	{
// 		return m_Strategy->get_reg_markets();
// 	}
	return vector<std::string>();
}



void SimulatorStrategy::on_make_order( const string& uid, const string& instrument, 
	EnumDirection direct, EnumOffsetFlag posiDirect,double price,int volume )
{
	//下单;
	//计算用户资金;
	//每次调用该函数都要计算相关的值，并在用户信息里进行修改;
	OPRECORD op;
	char* exId=instrument_container::get_instance().GetExhcnageIDByInstrumentID(instrument.c_str());
	strcpy(op.dwMarket,exId);
	//修改后，调用 OnReceiverData通知回报和委托;

}

COpRecordContainer & SimulatorStrategy::SimuGetOpRecord()
{
	return m_SimuOpRecord;
}

void SimulatorStrategy::on_cancel_order( const std::string& uid, const std::string& instrument )
{
	
}

