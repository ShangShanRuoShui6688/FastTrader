#ifndef _SIMPLE_SIMULATION_H_
#define _SIMPLE_SIMULATION_H_

#include "SimpleStrategy.h"
#include "SimulatorStrategy.h"

class SIMPLESTRATEGYLIB_EXPORT simple_simulation
{
public:
	simple_simulation(void);
	virtual ~simple_simulation(void);
	void set_strategy(simple_strategy* pStrategy);
	SimulatorStrategy* get_strategy();
	bool download_data_if_need();
	void restart();
	void pause();
	void continus();
	void stop();
	void on_end(bool bFinished);
protected:
	SimulatorStrategy* m_startegy;
	bool m_stop_and_reset;
	//ģ���߳�;
	thread* m_thread;
	friend void thread_callback(void* thread_arg);
};

#endif

