#ifndef _SIMULATOR_STRATEGY_H_
#define _SIMULATOR_STRATEGY_H_

#include "IStrategy.h"
#include "../TechLib/Strategy.h"

class SIMPLESTRATEGYLIB_EXPORT SimulatorStrategy
{
public:
	SimulatorStrategy(void);
	virtual ~SimulatorStrategy(void);
	boost::shared_ptr<IStrategy> GetStrategy();
	void SetStrategy(boost::shared_ptr<IStrategy> pStrategy);

public:
	//状态的枚举量;
	enum	SimuStatus	{
		statusInit		=	0x01,
		statusRunning	=	0x02,
		statusPaused	=	0x03,
		statusFinished	=	0x04,
	};
	// Data Cache
	bool	PrepareData();
	void	ClearLastIntensity();
	void	ClearCache();
	CTechInstrumentContainer &	GetTechInstrumentContainer();
	bool	CanModifiedNow()	{	return (SimuIsStatusInit() || SimuIsStatusFinished() );	}

	CRateParam		&	GetRateParam();
	COpParam		&	GetOpParam();
	vector<std::string>	GetInstruments();
	//模拟状态维护;
	int		SimuGetCurStatus( )		{	return m_SimuCurrentStatus;	}
	bool	SimuIsStatusInit( )		{	return statusInit == m_SimuCurrentStatus;	}
	bool	SimuIsStatusRunning( )	{	return statusRunning == m_SimuCurrentStatus;	}
	bool	SimuIsStatusPaused( )	{	return statusPaused == m_SimuCurrentStatus;	}
	bool	SimuIsStatusFinished( )	{	return statusFinished == m_SimuCurrentStatus;	}
	void	SimuSetStatusInit( )	{	m_SimuCurrentStatus	=	statusInit;	}
	void	SimuSetStatusRunning( )	{	m_SimuCurrentStatus	=	statusRunning;	}
	void	SimuSetStatusPaused()	
	{	
		m_SimuCurrentStatus	=	statusPaused;	
	}
	void	SimuSetStatusFinished( ){	m_SimuCurrentStatus	=	statusFinished;	}
	// Simulation
	void	SimuReset();
	bool	SimuGotoNextTime();
	time_t	SimuGetCurrentTime();
	double	SimuGetCurrentCash();
	bool	SimuOperate( OPRECORD record, bool bTimeStrict = true );
	COpRecordContainer &	SimuGetOpRecord();
	COpRecordContainer &	SimuGetNextOp();
	CInstrumentOwnContainer &	SimuGetStockOwn();
	CAssetSerialContainer &	SimuGetAssetSerial();
	DWORD	SimuGetCurrentProgress( DWORD dwProgressMax = STRATEGY_MAX_PROGRESS );	//	Not Same as Real
	double	SimuGetAsset( time_t tmCur);
	double	SimuGetCurrentYield();
	double	SimuGetCurrentYieldIndexPercent();
	double	SimuGetCurrentYieldPercent();
	bool	SimuOperateNextop( time_t tmCur, COpRecordContainer & nextop, CTechInstrument & techstock );
	virtual	bool	SimuRun(/*simulator_monitor* pMonitor,*/void* cookie);
public:
	virtual void on_make_order( const string& uid, const string& instrument, EnumDirection direct, 
		EnumOffsetFlag posiDirect,double price,int volume );
	virtual void on_cancel_order( const std::string& uid, const std::string& instrument );
protected:
	SimulatorStrategy(const SimulatorStrategy& );
	SimulatorStrategy& operator =(const SimulatorStrategy& );
	COpParam		m_opparam;		// 操作规则;
	// 回测状态:起始，正在进行，暂停，完成;
	int				m_SimuCurrentStatus;	
	// 模拟当前时间;
	time_t m_SimuCurrentTime;
	// 模拟当前资金;
	double			m_SimuCurrentCash;	
	// 模拟当前拥有合约;
	CInstrumentOwnContainer	m_SimuStockOwn;
	// 模拟记录;
	COpRecordContainer	m_SimuOpRecord;	
	// 模拟下一步操作;
	COpRecordContainer	m_SimuNextOp;
	// 模拟资产值序列;
	CAssetSerialContainer	m_SimuAssetSerial;
	//策略;
	boost::shared_ptr<IStrategy> m_Strategy;

	CTechInstrumentContainer	m_techstocks;		// 备选股票，包含信息、数据、技术指标等;

	std::map<std::string, std::pair<boost::shared_ptr<InstrumentData>,size_t> > m_InstrumentDataMap;
	//用于控制暂停的条件变量;
	condition_variable m_pause_cond;
	//用于控制暂停的互斥量;
	mutex m_pause_mutex;
};

#endif

