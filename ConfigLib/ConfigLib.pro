CONFIG -= qt
TARGET = ConfigLib
TEMPLATE = lib
INCLUDEPATH += $$PWD/../sdk/include
INCLUDEPATH += ../common
INCLUDEPATH += /home/rmb338/boost_1_64_0
INCLUDEPATH += ../
DEFINES *= CONFIGLIB_EXPORTS
linux-g++|macx-g++{
QMAKE_LFLAGS= -m64 -Wall -DNDEBUG  -O2
QMAKE_CFLAGS = -arch x86_64 -lpthread
}

CONFIG(debug, debug|release) {
        DESTDIR = ../build/debug
} else {
        DESTDIR = ../build/release
}
HEADERS += \
    XmlParserBase.h \
    ViewConfig.h \
    RealTimeConfig.h \
    MultiSortConfig.h \
    KLineConfig.h \
    configlib.h \
    AllConfig.h \
    tinyxml/tinyxml.h \
    tinyxml/tinystr.h \
    BrokerConfigReader.h \
    DataStoreConfigReader.h \
    UserConfigReader.h \
    ConfigReader.h

SOURCES += \
    XmlParserBase.cpp \
    ViewConfig.cpp \
    RealTimeConfig.cpp \
    MultiSortConfig.cpp \
    KLineConfig.cpp \
    configlib.cpp \
    AllConfig.cpp \
    BrokerConfigReader.cpp \
    DataStoreConfigReader.cpp \
    UserConfigReader.cpp \
    ../tinyxml/tinyxmlerror.cpp \
    ../tinyxml/tinyxml.cpp \
    ../tinyxml/tinyxmlparser.cpp \
    ConfigReader.cpp
