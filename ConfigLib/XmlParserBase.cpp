/**
 * \file 
 * \author huazai
 * \brief 解析器基类的实现文件
 *
 */
#include "XmlParserBase.h"
#include <string>
#include <vector>
#include <sstream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
using namespace std;
using namespace configlib;

XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::XmlParserBase(void)
{
	document=NULL;
	root=NULL;
}
/**
 * \memberof XmlParserBase
 * \param[in] file_path XML文件的路径
 * \brief 构造函数
 * \remark 初始化Xerces库,读取根元素文件
 */

XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::XmlParserBase( string file_path )
{
	if (false==load(file_path))
	{
	}
}

bool XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::load( const std::string& file_path )
{
	if (NULL!=document)
	{
		if(strcmp(file_path.c_str(),xml_file_path.c_str())==0)
		{
			return true;
		}
	}
	xml_file_path=file_path;
	document=new TiXmlDocument();
	bool bLoaded=document->LoadFile(xml_file_path.c_str());
	if (false==bLoaded)
	{
		if (NULL==document->FirstChildElement())
		{
			TiXmlDeclaration *dec=new TiXmlDeclaration("1.0","gb2312","no");
			if(NULL==document->LinkEndChild(dec))
			{
				return false;
			}
		}
	}
	root=document->RootElement();
	if (NULL==root)
	{
		root=new TiXmlElement("root");
		root->SetAttribute("version",250);
		if(NULL==document->LinkEndChild(root))
		{
			return false;
		}
		document->SaveFile();
	}
	return true;
}

/**
 * \memberof XmlParserBase
 * \brief 析构函数
 * 根据引用计数，释放类库
 *
 */

XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::~XmlParserBase(void)
{
	if(NULL!=document)
	{
		delete document;
		document=NULL;
	}
}

/**
 * \memberof XmlParserBase
 * \param[in] parent_element 父元素结点
 * \return 子元素结点
 * \brief 返回某个元素结点的第一层子素结点
 *
 */

vector<TiXmlElement*>  
XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::GetSubElements( TiXmlElement* parent_element ) const
{
	static vector<TiXmlElement*> subElements2;
	subElements2.clear();
	if (NULL==parent_element)
	{
		return subElements2;
	}

	TiXmlElement* fChild=parent_element->FirstChildElement();
	for (;NULL!=fChild;fChild=fChild->NextSiblingElement())
	{
		if (fChild->Type()==TiXmlNode::TINYXML_ELEMENT)
		{
			subElements2.push_back(fChild);
		}
	}
	return subElements2;
}
/**
 * \memberof XmlParserBase
 * \param[in] element 某个元素结点
 * \return 某个元素结点的内部的文本
 * \brief 取得某个元素结点的内部文本
 */
string XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::GetInnerText( TiXmlElement* element ) const
{
	if (NULL==element)
	{
		return "";
	}
	return element->GetText();
}
/**
 * \memberof XmlParserBase
 * \param[in] tag_name 标签名
 * \return 如果找到，返回元素结点，否则返回空
 * \brief 根据标答名取得某个结点
 * \remark 如果有多个同名节点，只返回第一个满足条件的结点
 */

TiXmlElement* XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::GetElementByTagName( string tag_name ) const
{
	
	TiXmlElement* element=root->FirstChildElement(tag_name.c_str());
	return element;
}
/**
 *
 * \brief 向结点写文本
 *
 */

void XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::SetInnerText( TiXmlElement* element,const string& text )
{
	if (NULL==element)
	{
		return ;
	}
	TiXmlText NameContent(text.c_str());
	TiXmlNode* oldNode=element->FirstChild();
	element->ReplaceChild(oldNode,NameContent);
}
/**
 *
 *
 * \brief 向文本结点中写入数字值
 */

void XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::SetInnerText( TiXmlElement* element,const int& digit )
{
	char strDigit[16];
	sprintf(strDigit,"%d",digit);
	SetInnerText(element,strDigit);
}

/**
 *
 * \brief 在某个元素结点下查找指定结点名的结点
 */

TiXmlElement* XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::GetSubElementByTagName( TiXmlElement* parentElement,string tag_name )
{
	return parentElement->FirstChildElement(tag_name.c_str());
}

std::string XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::GetXmlFilePath() const
{
	return xml_file_path;
}

TiXmlDocument* XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::GetXmlDocument() const
{
	return document;
}

void XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::RemoveAllChildNodes( TiXmlElement* parentElement )
{
	parentElement->Clear();
}

void XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::save()
{
	document->SaveFile();
}
//取得某节点的属性值

string XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::GetAttributeValue(TiXmlElement* element,string attrName )
{
	if (NULL==element)
	{
		return "";
	}
	if(NULL==element->Attribute(attrName.c_str()))
	{
		return "";
	}
	string attr_val="";
	attr_val= element->Attribute(attrName.c_str());
	return attr_val;
}
//设置某节点的属性值
void XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::SetAttributeValue( TiXmlElement* element,string attrName,string attrValue )
{
	if (HasAttribute(element,attrName))
	{
		element->SetAttribute(attrName.c_str(),attrValue.c_str());
	}
}

void XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::SetAttributeValue( TiXmlElement* element,string attrName,int attrValue )
{
	if (HasAttribute(element,attrName))
	{
		element->SetAttribute(attrName.c_str(),attrValue);
	}
}


TiXmlElement* XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::GetRootNode()
{
	return root;
}

int XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::GetInnerInt( TiXmlElement* element )
{
	string text=GetInnerText(element);
	return atoi(text.c_str());
}

bool XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::HasAttribute( TiXmlElement* element,string attrName )
{
	return element->Attribute(attrName.c_str())!=NULL;
}


TiXmlElement* XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::CreateElementWithAttributes( 
	TiXmlElement* parentElement, 
	string nodeName ,
	map<string,string> attributes,
	string innerText )
{

	TiXmlElement* thisNode=new TiXmlElement(nodeName.c_str());
	map<string,string>::iterator iter=attributes.begin();
	for (;iter!=attributes.end();++iter)
	{
		thisNode->SetAttribute(iter->first.c_str(),iter->second.c_str());
	}
	TiXmlText* content=new TiXmlText(innerText.c_str());
	thisNode->LinkEndChild(content);
	parentElement->LinkEndChild(thisNode);
	return thisNode;
}


void XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>::RemoveChild(TiXmlElement* parentElement, TiXmlElement* subElement )
{
	parentElement->RemoveChild(subElement);
}

TinyXmlParser& AfxGetXmlParser()
{
	static TinyXmlParser parser;
	return parser;
}



