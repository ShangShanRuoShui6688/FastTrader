#ifndef _VIEW_CONFIG_H_
#define _VIEW_CONFIG_H_
#include "configlib.h"
#include <string>
using namespace std;
#include "XmlParserBase.h"
using namespace configlib;
enum
{
	//grays
	GRAY10 = 0x1A1A1A,
	GRAY20 = 0x333333,
	GRAY30 = 0x4D4D4D,
	GRAY40 = 0x666666,
	GRAY50 = 0x808080,
	GRAY60 = 0x999999,
	GRAY70 = 0xB3B3B3,
	GRAY80 = 0xCCCCCC,
	GRAY90 = 0xE5E5E5,

	//HTML4 COLOR KEYWORDS - VGA
	//@see http://www.w3.org/TR/css3-color/#html4
	AQUA  = 0x00FFFF,
	BLACK  = 0x000000,
	BLUE  = 0x0000FF,
	FUCHSIA  = 0xFF00FF,
	GRAY  = 0x808080,
	GREEN  = 0x008000,
	LIME  = 0x00FF00,
	MAROON  = 0x800000,
	NAVY  = 0x000080,
	OLIVE  = 0x808000,
	PURPLE  = 0x800080,
	RED  = 0xFF0000,
	SILVER  = 0xC0C0C0,
	TEAL  = 0x008080,
	WHITE  = 0xFFFFFF,
	YELLOW  = 0xFFFF00,

	//SVG COLOR KEYWORDS - X11 ( INCLUDES THE 16 HTML4 - VGA COLORS )
	//@see http://www.w3.org/TR/css3-color/#svg-color
	ALICE_BLUE  = 0xF0F8FF,
	ANTIQUE_WHITE  = 0xFAEBD7,
	AQUAMARINE  = 0x7FFFD4,
	AZURE  = 0xF0FFFF,
	BEIGE  = 0xF5F5DC,
	BISQUE  = 0xFFE4C4,
	BLANCHED_ALMOND  = 0xFFEBCD,
	BLUE_VIOLET  = 0x8A2BE2,
	BROWN  = 0xA52A2A,
	BURLY_WOOD  = 0xDEB887,
	CADET_BLUE  = 0x5F9EA0,
	CHARTREUSE  = 0x7FFF00,
	CHOCOLATE  = 0xD2691E,
	CORAL  = 0xFF7F50,
	CORNFLOWER_BLUE  = 0x6495ED,
	CORNSILK  = 0xFFF8DC,
	CRIMSON  = 0xDC143C,
	CYAN  = 0x00FFFF,
	DARK_BLUE  = 0x00008B,
	DARK_CYAN  = 0x008B8B,
	DARK_GOLDEN_ROD  = 0xB8860B,
	DARK_GRAY  = 0xA9A9A9,
	DARK_GREY  = 0xA9A9A9,
	DARK_GREEN  = 0x006400,
	DARK_KHAKI  = 0xBDB76B,
	DARK_MAGENTA  = 0x8B008B,
	DARK_OLIVE_GREEN  = 0x556B2F,
	DARK_ORANGE  = 0xFF8C00,
	DARK_ORCHID  = 0x9932CC,
	DARK_RED  = 0x8B0000,
	DARK_SALMON  = 0xE9967A,
	DARK_SEA_GREEN  = 0x8FBC8F,
	DARK_SLATE_BLUE  = 0x483D8B,
	DARK_SLATE_GRAY  = 0x2F4F4F,
	DARK_SLATE_GREY  = 0x2F4F4F,
	DARK_TURQUOISE  = 0x00CED1,
	DARK_VIOLET  = 0x9400D3,
	DEEP_PINK  = 0xFF1493,
	DEEP_SKY_BLUE  = 0x00BFFF,
	DIM_GRAY  = 0x696969,
	DIM_GREY  = 0x696969,
	DODGER_BLUE  = 0x1E90FF,
	FIRE_BRICK  = 0xB22222,
	FLORAL_WHITE  = 0xFFFAF0,
	FOREST_GREEN  = 0x228B22,
	GAINSBORO  = 0xDCDCDC,
	GHOST_WHITE  = 0xF8F8FF,
	GOLD  = 0xFFD700,
	GOLDEN_ROD  = 0xDAA520,
	GREY  = 0x808080,
	GREEN_YELLOW  = 0xADFF2F,
	HONEY_DEW  = 0xF0FFF0,
	HOT_PINK  = 0xFF69B4,
	INDIAN_RED  = 0xCD5C5C,
	INDIGO  = 0x4B0082,
	IVORY  = 0xFFFFF0,
	KHAKI  = 0xF0E68C,
	LAVENDER  = 0xE6E6FA,
	LAVENDER_BLUSH  = 0xFFF0F5,
	LAWN_GREEN  = 0x7CFC00,
	LEMON_CHIFFON  = 0xFFFACD,
	LIGHT_BLUE  = 0xADD8E6,
	LIGHT_CORAL  = 0xF08080,
	LIGHT_CYAN  = 0xE0FFFF,
	LIGHT_GOLDEN_ROD_YELLOW  = 0xFAFAD2,
	LIGHT_GRAY  = 0xD3D3D3,
	LIGHT_GREY  = 0xD3D3D3,
	LIGHT_GREEN  = 0x90EE90,
	LIGHT_PINK  = 0xFFB6C1,
	LIGHT_SALMON  = 0xFFA07A,
	LIGHT_SEA_GREEN  = 0x20B2AA,
	LIGHT_SKY_BLUE  = 0x87CEFA,
	LIGHT_SLATE_GRAY  = 0x778899,
	LIGHT_SLATE_GREY  = 0x778899,
	LIGHT_STEEL_BLUE  = 0xB0C4DE,
	LIGHT_YELLOW  = 0xFFFFE0,
	LIME_GREEN  = 0x32CD32,
	LINEN  = 0xFAF0E6,
	MAGENTA  = 0xFF00FF,
	MEDIUM_AQUA_MARINE  = 0x66CDAA,
	MEDIUM_BLUE  = 0x0000CD,
	MEDIUM_ORCHID  = 0xBA55D3,
	MEDIUM_PURPLE  = 0x9370D8,
	MEDIUM_SEA_GREEN  = 0x3CB371,
	MEDIUM_SLATE_BLUE  = 0x7B68EE,
	MEDIUM_SPRING_GREEN  = 0x00FA9A,
	MEDIUM_TURQUOISE  = 0x48D1CC,
	MEDIUM_VIOLET_RED  = 0xC71585,
	MIDNIGHT_BLUE  = 0x191970,
	MINT_CREAM  = 0xF5FFFA,
	MISTY_ROSE  = 0xFFE4E1,
	MOCCASIN  = 0xFFE4B5,
	NAVAJO_WHITE  = 0xFFDEAD,
	OLD_LACE  = 0xFDF5E6,
	OLIVE_DRAB  = 0x6B8E23,
	ORANGE  = 0xFFA500,
	ORANGE_RED  = 0xFF4500,
	ORCHID  = 0xDA70D6,
	PALE_GOLDEN_ROD  = 0xEEE8AA,
	PALE_GREEN  = 0x98FB98,
	PALE_TURQUOISE  = 0xAFEEEE,
	PALE_VIOLET_RED  = 0xD87093,
	PAPAYA_WHIP  = 0xFFEFD5,
	PEACH_PUFF  = 0xFFDAB9,
	PERU  = 0xCD853F,
	PINK  = 0xFFC0CB,
	PLUM  = 0xDDA0DD,
	POWDER_BLUE  = 0xB0E0E6,
	ROSY_BROWN  = 0xBC8F8F,
	ROYAL_BLUE  = 0x4169E1,
	SADDLE_BROWN  = 0x8B4513,
	SALMON  = 0xFA8072,
	SANDY_BROWN  = 0xF4A460,
	SEA_GREEN  = 0x2E8B57,
	SEA_SHELL  = 0xFFF5EE,
	SIENNA  = 0xA0522D,
	SKY_BLUE  = 0x87CEEB,
	SLATE_BLUE  = 0x6A5ACD,
	SLATE_GRAY  = 0x708090,
	SLATE_GREY  = 0x708090,
	SNOW  = 0xFFFAFA,
	SPRING_GREEN  = 0x00FF7F,
	STEEL_BLUE  = 0x4682B4,
	TAN  = 0xD2B48C,
	THISTLE  = 0xD8BFD8,
	TOMATO  = 0xFF6347,
	TURQUOISE  = 0x40E0D0,
	VIOLET  = 0xEE82EE,
	WHEAT  = 0xF5DEB3,
	WHITE_SMOKE  = 0xF5F5F5,
	YELLOW_GREEN  = 0x9ACD32,
};

struct work_tab_item
{
	int id;
	string name;
	string EName;
};
struct ColumnInfo{
	int m_ColumnId;
	int m_ColumnWidth;
	string m_ColumnEName;
	string m_ColumnName;
	string m_Express;//表达式
	bool isVisible;
	long planeColor; //平盘色
	long raiseColor; //上涨色
	long fallColor;  //下跌色
	long bkColor;    //背景色
};
enum{
	clrBK=0,
	clrTitle=1,
	clrBorder=2,
	clrText=3,
	clrRise=4,
	clrFall=5,
	clrPlane=6,
	clrFallEntity=7,
	clrAxis=8,
	clrReport=9,
	clrSelLine=10,
	//为K线特有
	clrNewKLine=11,
	clrDJ=12,
	clrCW=13,
	//列表特有
	clrTextBK=14,
	clrSelected=15,
	clrFixedText=16,
	clrFixedBK=17,
	clrListBK=16,

	clrLine1=18,
	clrLine2=19,
	clrLine3=20,
	clrLine4=21,
	clrLine5=22,

	
	clrPosition=23,
	clrVolume=24,
	clrTurnover=25,
	clrLastPrice=26,
	clrListHeaderBK=27,
	clrListHeaderText=28,
	clrPrice = 29,
};

enum
{
	fontText=0,//文本字体
	fontWorkTab=1,
	fontTitle=2
};
enum FONT_WITH_TYPE{
	FONT_WITH_NONE		=0x0,
	FONT_WITH_BOLD		=0x1,
	FONT_WITH_ITALIC	=0x2,
	FONT_WITH_UNDERLINE =0x4,
};
struct PaneFont{
	int Id;      //编号
	string EName;//英文名
	string Name;//字体名称
	int Width;
	int Height;
	int Flags;//加粗，斜体
};
struct PaneColor{
	int Id;
	string EName;
	string Name;
	long Color;
};
struct PaneString{
	int Id;
	string EName;
	string Name;
};
struct PaneConfig{
	int m_PaneId;
	string m_PaneEName;
	string m_PaneName;
	map<int,PaneFont> m_Fonts;
	map<int,PaneColor> m_Colors;
	map<string,string> m_Attr_2_Value;
};

class CONFIGLIB_API ViewConfig
{
public:

	enum klineMode{
		klineCandle=0x01,
		klineAmerican=0x02,
		klineTower=0x03
	};

	enum sg_drawtech{
		drawtechtype_line=0x01,
		drawtechtype_dot=0x02
	};
	enum ReportWhat{
		reportParam=0x01,
		reportCost=0x02,
		reportFlame=0x03,
		reportActivity=0x04
	};

	typedef struct ConfitSection
	{
		std::string section_name;
		int    section_id;
	}ConfigSection;

	enum section_id
	{
		client_config_id=1,
		main_frame_config_id=2,
		web_info_config_id=3,
	};
public:
	ViewConfig(void);
	virtual ~ViewConfig(void);
public:
	//加载配置
	virtual bool load(TinyXmlParser* parser,string section,int sectionId)
	{
		this->parser=parser;
		return NULL!=parser;
	}
	virtual bool save()=0;

	//virtual string getConfigFilePath();
	//virtual string getConfigFileShortName();
	//virtual string setConfigFilePath(string path);
	//virtual string setConfigFileShortName(string shortName);
protected:
	string section;//配置段的名称
	int sectionId;
	TinyXmlParser* parser;
};
//filePath--> Drive://Dir//ShortName.ext
//section begin
//...config and so on

//section end
extern ViewConfig::ConfitSection all_config_sections[];
#endif