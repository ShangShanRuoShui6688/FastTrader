//#include "stdafx.h"
#include "BrokerConfigReader.h"
#include "../tinyxml/tinyxml.h"
#include "../tinyxml/tinystr.h"


BrokerConfigReader::BrokerConfigReader(void)
	:ConfigReader()
{
}


BrokerConfigReader::~BrokerConfigReader(void)
{
}

BrokerConfigReader& BrokerConfigReader::GetInstance()
{
	static BrokerConfigReader g_BrokerConfigReader;
	return g_BrokerConfigReader;
}

bool BrokerConfigReader::LoadBrokers()
{
	//读取配置文件;
	bool bRet=false;
	string broker_tag_name="broker";
	TiXmlElement* elem=root->FirstChildElement(broker_tag_name.c_str());
	while (NULL!=elem)
	{
		do
		{
			string broker_name=GetAttribute(elem,"BrokerName");
			ServerInfo si;
			si.id=GetAttribute(elem,"BrokerID");
			if (si.id.empty())
			{
				break;
			}
			TiXmlElement* servers_elem=elem->FirstChildElement("Servers");
			if (!servers_elem)
			{
				break;
			}
			//读取服务器地址列表;
			TiXmlElement* front_server_elem=servers_elem->FirstChildElement("Server");
			if (!front_server_elem)
			{
				break;
			}
			do
			{
				TiXmlElement* name_elem=front_server_elem->FirstChildElement("Name");
				if (name_elem)
				{
					si.name=GetInnerText(name_elem);
				}
				else
				{
					continue;
				}
				//接口;
				TiXmlElement* api_elem = front_server_elem->FirstChildElement("Api");
				if (api_elem)
				{
					si.api = GetInnerText(api_elem);
				}
				if (si.api.empty())
				{
					si.api = "CTP";
				}
				//接口;
				TiXmlElement* level_elem = front_server_elem->FirstChildElement("MarketLevel");
				if (level_elem)
				{
					si.market_level = GetInnerText(level_elem);
				}
				//交易前置地址;
				TiXmlElement* trading_elem=front_server_elem->FirstChildElement("Trading");
				if (trading_elem)
				{
					si.trade_server_front.clear();
					//超时时间;
					std::string timeout = GetAttribute(trading_elem, "Timeout");
					si.trader_login_timeout = std::atoi(timeout.c_str());
					
					std::string public_topic_resume_type = 
						GetAttribute(trading_elem, "public_topic_resume_type");
					if (!public_topic_resume_type.empty())
					{
						si.public_topic_resume_type = (enTradeResumeType)std::stoi(public_topic_resume_type);
					}

					std::string private_topic_resume_type =
						GetAttribute(trading_elem, "private_topic_resume_type");
					if (!private_topic_resume_type.empty())
					{
						si.private_topic_resume_type = (enTradeResumeType)std::stoi(private_topic_resume_type);
					}
					std::vector<TiXmlElement*> trading_addrs=GetSubElements(trading_elem);
					for (std::size_t i=0;i<trading_addrs.size();++i)
					{
						si.trade_server_front.push_back(string(trading_addrs[i]->GetText()));
					}
				}
				
				//行情前置地址;
				trading_elem=front_server_elem->FirstChildElement("MarketData");
				if (trading_elem)
				{
					si.market_server_front.clear();
					//超时时间;
					std::string timeout = GetAttribute(trading_elem, "Timeout");
					si.market_login_timeout = std::atoi(timeout.c_str());

					std::vector<TiXmlElement*> market_addrs=GetSubElements(trading_elem);
					for (std::size_t i=0;i<market_addrs.size();++i)
					{
						si.market_server_front.push_back(string(market_addrs[i]->GetText()));
					}
				}
				
				//查询前置地址;
				trading_elem=front_server_elem->FirstChildElement("Query");
				if (trading_elem)
				{
					si.query_server_front.clear();
					//超时时间;
					std::string timeout = GetAttribute(trading_elem, "Timeout");
					si.query_login_timeout = std::atoi(timeout.c_str());

					std::vector<TiXmlElement*> qry_addrs=GetSubElements(trading_elem);
					for (std::size_t i=0;i<qry_addrs.size();++i)
					{
						si.query_server_front.push_back(string(qry_addrs[i]->GetText()));
					}
				}
				m_brokers.insert(make_pair(si.name,si));
				front_server_elem=front_server_elem->NextSiblingElement("Server");
			}while(front_server_elem!=NULL);
		}while(0);
		elem=elem->NextSiblingElement(broker_tag_name.c_str());
	}
	return true;
}

bool BrokerConfigReader::Load( const std::string& config_file/*="brokers.xml"*/, bool bForceLoad/*=false*/ )
{
	if(!ConfigReader::Load(config_file,bForceLoad))
	{
		return false;
	}
	if (!LoadBrokers())
	{
		return false;
	}
	return true;
}

bool BrokerConfigReader::Unload()
{
	bool bUnLoad=ConfigReader::Unload();
	m_brokers.clear();
	return bUnLoad;
}
