// CTP.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "CTP.h"
#include "CtpTrader.h"
#include "CtpMarket.h"
#include "../Log/logging.h"

/*EXTERN_C*/ boost::shared_ptr<Trader> CreateTrader()
{
	
	return boost::make_shared<CtpTrader>();
}

/*EXTERN_C*/ boost::shared_ptr<Market>  CreateMarket(const std::string& /*protocol*/)
{
	
	return boost::make_shared<CtpMarket>();
}