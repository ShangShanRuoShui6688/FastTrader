#include "Portfolio.h"
#include "../Log/logging.h"
#include "../SimpleStrategyLib/csv_output.h"
#include "../FacilityBaseLib/Container.h"
#include "UserApiMgr.h"
#include "Trader.h"
#include <boost/make_shared.hpp>

#include <boost/lambda/lambda.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>

#include <boost/serialization/serialization.hpp>
#include <boost/archive/tmpdir.hpp>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <fstream>

#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>

Portfolio::Portfolio(const std::string& Id)
	:m_Id(Id)
{
	m_TradingAccounts = boost::make_shared<std::vector<boost::shared_ptr<TradingAccount> > >();
	m_TradingAccount = boost::make_shared<TradingAccount>();
	m_TradingAccount->AccountID = Id;
	m_TradingAccount->Available = 0;
	m_TradingAccount->CloseProfit = 0;
	m_TradingAccount->Commission = 0;
	m_TradingAccount->CurrMargin = 0;
	m_TradingAccount->Deposit = 0;
	m_TradingAccount->PositionProfit = 0;
	m_TradingAccount->PreBalance = 0;
	m_TradingAccount->Withdraw = 0;

	m_TradingAccountsToday = boost::make_shared<std::vector<boost::shared_ptr<TradingAccount> > >();

	m_MyOrders = boost::make_shared<std::map<std::string, boost::shared_ptr<Order> > >();

	m_HistoryOrders = boost::make_shared<std::map<std::string, boost::shared_ptr<Order> > >();

	m_MyInputOrders = boost::make_shared<std::map<std::string, boost::shared_ptr<InputOrder> > >();

	m_MyTrades = boost::make_shared<std::map<std::string, boost::shared_ptr<Trade> > >();

	m_HistoryTrades = boost::make_shared<std::map<std::string, boost::shared_ptr<Trade> > >();

	m_MyInputOrderActions = boost::make_shared<std::map<std::string, boost::shared_ptr<InputOrderAction> > >();

	m_ClosePositions = boost::make_shared<std::vector<boost::shared_ptr<InvestorPosition> > >();

	m_MyPositions = boost::make_shared < std::vector<boost::shared_ptr<InvestorPosition> > >();
}


Portfolio::~Portfolio()
{

}



bool Portfolio::Update(boost::shared_ptr<InputOrderAction> orderAction)
{
	//保存Order;
	if (orderAction)
	{
		auto oaIter = m_MyInputOrderActions->find(orderAction->OrderRef);
		if (oaIter != m_MyInputOrderActions->end())
		{
			*oaIter->second = *orderAction;
			auto oiter = m_MyOrders->find(orderAction->OrderRef);
			if (oiter != m_MyOrders->end())
			{
				m_MyOrders->erase(oiter);
			}
			return true;
		}
		else
		{
			
		}
	}
	return false;
}


bool Portfolio::Update(boost::shared_ptr<Order> order, bool bHistory /*= false*/)
{
	if (!order)
	{
		return false;
	}
	if (order->OrderStatus == OST_Canceled)
	{
		//获取合约的品种;
		InstrumentInfo info;
		InstrumentContainer::get_instance().get_info_by_id(order->InstrumentID,&info );
		auto occIter = m_OrderCancelCount.find(info.ProductID);
		if (occIter == m_OrderCancelCount.end())
		{
			m_OrderCancelCount.insert(std::make_pair(info.ProductID, 1));
		}
		else
		{
			occIter->second++;
		}
	}

	//保存Order;
	if (IsMyOrder(order))
	{
		if (bHistory)
		{
			(*m_HistoryOrders)[order->OrderSysID] = order;
			return true;
		}
		auto ioIter = m_MyInputOrders->find(order->OrderRef);
		if (ioIter != m_MyInputOrders->end())
		{
			if (order->ErrorID != 0)
			{
				//报单出错,将冻结的资金归还;
				m_TradingAccount->Available += (ioIter->second->FrozenMargin + ioIter->second->FrozenCommission);
				return true;
			}
			if (order->OrderSysID.empty())
			{
				return true;
			}
			auto oIter = m_MyOrders->find(order->OrderSysID);
			if (oIter == m_MyOrders->end())
			{
				//报单占用;
				order->FrozenMargin = ioIter->second->FrozenMargin;
				order->FrozenCommission = ioIter->second->FrozenCommission;
				m_MyOrders->insert(std::make_pair(order->OrderSysID, order));
				//报单不再占用;
				//ioIter->second->FrozenMargin = 0;
				//ioIter->second->FrozenCommission = 0;
			}
			else
			{
				double FrozenMargin = oIter->second->FrozenMargin;
				double FrozenCommission = oIter->second->FrozenCommission;
				*oIter->second = *order;
				oIter->second->FrozenMargin = FrozenMargin;
				oIter->second->FrozenCommission = FrozenCommission;
			}

			if (order->OrderStatus == OST_Canceled)
			{
				//撤单,将冻结的资金返还给可用资金;
				m_TradingAccount->Available += (order->FrozenMargin + order->FrozenCommission);
				//order->FrozenMargin = 0;
				//order->FrozenCommission = 0;
				if (order->CombOffsetFlag[0]!=OF_Open)
				{
					//撤平仓单;
					//平仓的话,要冻结相应的仓位;
					auto posiIter = std::find_if(m_MyPositions->begin(), m_MyPositions->end(),
						[order](const boost::shared_ptr<InvestorPosition>& ip)
					{
						bool isFind = (ip->InstrumentID == order->InstrumentID);
						if (order->Direction == D_Buy)
						{
							//需要查找空头仓位;
							return isFind && ip->PosiDirection == PD_Short && (ip->ShortFrozen >= order->VolumeTotalOriginal);
						}
						else
						{
							return isFind && ip->PosiDirection == PD_Long && (ip->LongFrozen >= order->VolumeTotalOriginal);
						}
						return isFind;
					});
					if (posiIter != m_MyPositions->end())
					{
						auto posi = *posiIter;
						if (order->Direction == D_Buy)
						{
							//需要查找空头仓位;
							posi->ShortFrozen -= order->VolumeTotalOriginal;
						}
						else
						{
							posi->LongFrozen -= order->VolumeTotalOriginal;
						}
					}
				}
			}
			return true;
		}
		else
		{
			//LOGDEBUG("Order {} is Not in Strategy[{}] ", GetId(), order->OrderRef);
		}
	}
	return false;
}

bool Portfolio::Update(boost::shared_ptr<Trade> trade, bool bHistory /*= false*/)
{
	//保存自己的成交单;
	if (trade&& IsMyTrade(trade))
	{
		if (bHistory)
		{
			(*m_HistoryTrades)[trade->OrderSysID] = trade;
			return true;
		}

		auto oIter = m_MyOrders->find(trade->OrderSysID);
		if (oIter != m_MyOrders->end())
		{
			auto trader = UserApiMgr::GetInstance().GetTrader(oIter->second->UserID, oIter->second->BrokerID);
			if (!trader)
			{
				return false;
			}
			auto tIter = m_MyTrades->find(trade->OrderSysID);
			if (tIter == m_MyTrades->end())
			{
				m_MyTrades->insert(std::make_pair(trade->OrderSysID, trade));
			}
			else
			{
				*tIter->second = *trade;
			}
			auto pCommisionRate = trader->GetInstCommissionRate(trade->InstrumentID);
			//成交之后按成交的算;
			auto info = GetInstrumentInfo(trade->InstrumentID);
			auto& lastTick = info->m_reportLatest;
			int VolumeMultiple = info->VolumeMultiple;
			double MarginPrice = trade->Price;
			auto brokerTradingParam = GetBrokerTradingParams();
			if (brokerTradingParam->MarginPriceType == MPT_PreSettlementPrice)
			{
				//昨结算;
				MarginPrice = lastTick.PreSettlementPrice;
			}
			else if (brokerTradingParam->MarginPriceType == MPT_SettlementPrice)
			{
				//最新价;
				MarginPrice = lastTick.LastPrice;
			}
			else if (brokerTradingParam->MarginPriceType == MPT_AveragePrice)
			{
				//成交均价;
				MarginPrice = lastTick.AveragePrice;
			}
			else
			{
				MarginPrice = trade->Price;
			}
			double UsedCommission = 0;

			//成交后重新计算资金;
			if (trade->OffsetFlag == OF_Open)
			{
				//开仓占用保证金;
				double OpenUsedMargin =
					trade->Volume
					* MarginPrice
					* VolumeMultiple;

				auto pMarginRate = trader->GetInstMarginRate(trade->InstrumentID);

				if (trade->Direction == D_Buy)
				{
					OpenUsedMargin =
						OpenUsedMargin * pMarginRate->LongMarginRatioByMoney
						+
						OpenUsedMargin * pMarginRate->LongMarginRatioByVolume;
				}
				else
				{
					OpenUsedMargin =
						OpenUsedMargin * pMarginRate->ShortMarginRatioByMoney
						+
						OpenUsedMargin * pMarginRate->ShortMarginRatioByVolume;
				}
				//花费的手续费;

				//开仓手续费率;
				//（空头冻结量+多头冻结量） * 开仓手续费率  -- 按手数
				//	SUM（（空头冻结量 + 多头冻结量） * 报单价格 * 合约乘数 * 开仓手续费率） -- 按金额"
				UsedCommission =
					trade->Volume*MarginPrice*
					VolumeMultiple*pCommisionRate->OpenRatioByMoney
					+ trade->Volume*pCommisionRate->OpenRatioByVolume;

				//开仓;
				double PreFrozenMoney = (oIter->second->FrozenMargin + oIter->second->FrozenCommission);
				//将冻结的资金返还;
				m_TradingAccount->FrozenCommission -= oIter->second->FrozenCommission;
				m_TradingAccount->FrozenMargin -= oIter->second->FrozenMargin;

				m_TradingAccount->Available += PreFrozenMoney;

				//重新计算资金;
				m_TradingAccount->Commission += UsedCommission;
				m_TradingAccount->CurrMargin += OpenUsedMargin;

				m_TradingAccount->Available -= UsedCommission;
				m_TradingAccount->Available -= OpenUsedMargin;
				

				auto tradePosiDirection = trade->Direction == D_Buy ? PD_Long : PD_Short;

				auto positionIter = std::find_if(m_MyPositions->begin(), m_MyPositions->end(),
					[trade, tradePosiDirection](const boost::shared_ptr<InvestorPosition>& ip)
				{
					return ip->InstrumentID == trade->InstrumentID
						&&
						((ip->Position == 0) || (ip->PosiDirection == tradePosiDirection));
				});

				boost::shared_ptr<InvestorPosition> pPosition = Trade2Position(trade);

				pPosition->Commission += UsedCommission;
				pPosition->UseMargin += OpenUsedMargin;

				if (positionIter == m_MyPositions->end())
				{
					//没有这个的持仓,成交信息必须为开仓;
					m_MyPositions->push_back(pPosition);
				}
				else
				{
					auto position=*positionIter;
					//加仓;
					if (position->PositionDate == PSD_History)
					{
						//不可能开昨仓;
						position->PositionDate = PSD_Today;
					}
					//开仓;
					if (position->Position == 0)
					{
						position->PosiDirection = tradePosiDirection;
					}
					//加仓;
					int totalVolume = position->Position + trade->Volume;
					position->PositionCost = (position->Position*position->PositionCost + trade->Volume*trade->Price) / totalVolume;
					position->Position = totalVolume;
					position->TodayPosition += totalVolume;
				}
			}
			else if (trade->OffsetFlag == OF_CloseToday)
			{
				UsedCommission =
					trade->Volume*pCommisionRate->CloseTodayRatioByMoney
					+ trade->Volume*pCommisionRate->CloseTodayRatioByVolume;

				//平今;
				//double PreFrozenMoney = (oIter->second->FrozenMargin + oIter->second->FrozenCommission);
				//m_TradingAccount->back()->Available += PreFrozenMoney;
				m_TradingAccount->FrozenCommission -= oIter->second->FrozenCommission;
				//平仓手续费;

				m_TradingAccount->Commission += UsedCommission;
				m_TradingAccount->Available -= UsedCommission;

				auto tradePosiDirection = trade->Direction == D_Buy ? PD_Long : PD_Short;

				auto positionIter = std::find_if(m_MyPositions->begin(), m_MyPositions->end(),
					[trade, tradePosiDirection](const boost::shared_ptr<InvestorPosition>& ip)
				{
					//平仓取反向,而且要是今仓;
					return ip->InstrumentID == trade->InstrumentID
						&&
						((ip->Position == 0) || (ip->PosiDirection != tradePosiDirection))
						&&
						ip->PositionDate == PSD_Today;
				});
				//返还保证金;
				double CloseReturnMargin = 0;
				if (positionIter!=m_MyPositions->end())
				{
					auto position = *positionIter;
					//平今仓;
					int totalVolume = position->Position - trade->Volume;
					if (totalVolume == 0)
					{
						//仓位全部被平掉;
						CloseReturnMargin = position->UseMargin;
						position->PositionCost = 0;
						m_TradingAccount->PositionProfit = 0;
					}
					else
					{
						//部份平仓;
						CloseReturnMargin = position->UseMargin / position->Position * trade->Volume;

						if (position->PositionDate == PSD_History)
						{
							//昨仓;
							position->PositionCost = lastTick.PreSettlementPrice;
						}
						else
						{
							position->PositionCost = (trade->Volume*trade->Price - position->Position*position->PositionCost) / totalVolume;
						}

						double positionProfit = (lastTick.LastPrice - position->PositionCost)*
							position->Position*info->VolumeMultiple;
						if (position->PosiDirection == PD_Short)
						{
							//空头;
							positionProfit *= -1;
						}
						m_TradingAccount->PositionProfit -= position->PositionProfit;
						position->PositionProfit = positionProfit;
						m_TradingAccount->PositionProfit += position->PositionProfit;
					}

					position->TodayPosition = totalVolume;
					position->Position = totalVolume;
					position->CloseVolume = trade->Volume;

					//返还保证金;
					m_TradingAccount->CurrMargin -= CloseReturnMargin;
					m_TradingAccount->Available += CloseReturnMargin;

					//计算平仓盈亏;
					//逐日平仓盈亏;
					//"SUM（平昨量 *（平仓价 - 昨结算价）* 合约乘数）+SUM（平今量 *（平仓价 - 开仓价）* 合约乘数） -- 多头
					//	SUM（平昨量 *（昨结算价 - 平仓价）* 合约乘数） + SUM（平今量 *（开仓价 - 平仓价）* 合约乘数） -- 空头"

					//逐笔平仓盈亏;
					//"SUM（平仓量 * （平仓价 - 开仓价）* 合约乘数） -- 多头
					//	SUM（平仓量 * （开仓价 - 平仓价）* 合约乘数） -- 空头"

					//平多头;
					double closeProfitByTrade =
						trade->Volume*(trade->Price - position->OpenPrice)*VolumeMultiple;
					double closeProfitByDate =
						trade->Volume*(trade->Price - position->OpenPrice)*VolumeMultiple;

					if (trade->Direction == D_Buy)
					{
						//平空头;
						closeProfitByTrade *= -1;
						closeProfitByDate *= -1;
					}

					position->CloseProfitByDate += closeProfitByDate;
					position->CloseProfitByTrade += closeProfitByTrade;

					position->CloseProfit = (position->CloseProfitByDate /*+ position->CloseProfitByTrade*/);

					m_TradingAccount->CloseProfit += position->CloseProfit;

					m_TradingAccount->Available += position->CloseProfit;

					if (position->Position == 0)
					{
						m_ClosePositions->push_back(position);
						m_MyPositions->erase(positionIter);
					}
					LOGDEBUG("closeProfitByDate={},closeProfitByTrade={}", closeProfitByDate, closeProfitByTrade);
				}
			}
			else
			{
				UsedCommission =
					trade->Volume*pCommisionRate->CloseRatioByMoney
					+ trade->Volume*pCommisionRate->CloseRatioByVolume;
				//平仓;
				//double PreFrozenMoney = (oIter->second->FrozenMargin + oIter->second->FrozenCommission);
				//m_TradingAccount->back()->Available += PreFrozenMoney;
				m_TradingAccount->FrozenCommission -= oIter->second->FrozenCommission;
				//增加手续费;
				m_TradingAccount->Commission += UsedCommission;
				//可用资金减少;
				m_TradingAccount->Available -= UsedCommission;


				auto tradePosiDirection = trade->Direction == D_Buy ? PD_Long : PD_Short;

				auto positionIter = std::find_if(m_MyPositions->begin(), m_MyPositions->end(),
					[trade, tradePosiDirection](const boost::shared_ptr<InvestorPosition>& ip)
				{
					return ip->InstrumentID == trade->InstrumentID
						&&
						((ip->Position == 0) || (ip->PosiDirection != tradePosiDirection));
				});

				if (positionIter!=m_MyPositions->end())
				{
					//返还保证金;
					double CloseReturnMargin = 0;

					auto position = *positionIter;
					//昨仓;
					int totalVolume = position->Position - trade->Volume;
					if (totalVolume == 0)
					{
						CloseReturnMargin = position->UseMargin;
						position->PositionCost = 0;
						m_TradingAccount->PositionProfit = 0;
					}
					else
					{
						CloseReturnMargin = position->UseMargin / position->Position * trade->Volume;

						//重新计算持仓盈亏;
						if (position->PositionDate == PSD_History)
						{
							//昨仓;
							position->PositionCost = lastTick.PreSettlementPrice;
						}
						else
						{
							position->PositionCost = (trade->Volume*trade->Price - position->Position*position->PositionCost) / totalVolume;
						}

						double positionProfit = (lastTick.LastPrice - position->PositionCost)*
							position->Position*info->VolumeMultiple;
						if (position->PosiDirection == PD_Short)
						{
							//空头;
							positionProfit *= -1;
						}
						m_TradingAccount->PositionProfit -= position->PositionProfit;
						position->PositionProfit = positionProfit;
						m_TradingAccount->PositionProfit += position->PositionProfit;
					}

					position->YdPosition = totalVolume;
					position->Position = totalVolume;

					//返还保证金;
					m_TradingAccount->CurrMargin -= CloseReturnMargin;
					m_TradingAccount->Available += CloseReturnMargin;

					//计算平仓盈亏;
					//平多头;
					double closeProfitByTrade =
						trade->Volume*(trade->Price - position->OpenPrice)*VolumeMultiple;
					double closeProfitByDate =
						trade->Volume*(trade->Price - lastTick.PreSettlementPrice)*VolumeMultiple;

					if (trade->Direction == D_Buy)
					{
						//平空头;
						closeProfitByTrade *= -1;
						closeProfitByDate *= -1;
					}

					position->CloseProfitByDate += closeProfitByDate;
					position->CloseProfitByTrade += closeProfitByTrade;

					//平仓盈亏;
					position->CloseProfit = (position->CloseProfitByDate /*+ position->CloseProfitByTrade*/);

					m_TradingAccount->CloseProfit += position->CloseProfit;
					//可用资金加上平仓盈亏;
					m_TradingAccount->Available += position->CloseProfit;

					//交易更新持仓;
					if (position->Position == 0)
					{
						m_ClosePositions->push_back(*positionIter);
						m_MyPositions->erase(positionIter);
					}
					LOGDEBUG("closeProfitByDate={},closeProfitByTrade={}", closeProfitByDate, closeProfitByTrade);
				}
				//SavePositions();
			}
			return true;
		}
		else
		{
			//LOGDEBUG("Trade {} is Not Strategy[{}] ", GetId(), trade->OrderRef);
		}
	}
	return false;
}

bool Portfolio::Update(boost::shared_ptr<InputOrder> inputOrder)
{
	if (!inputOrder)
	{
		return false;
	}
	//冻结资金;
	//单一持仓开仓冻结量 * 最新价 * 合约乘数 * 保证金率
	auto inst_info = GetInstrumentInfo(inputOrder->InstrumentID);
	if (inst_info)
	{
		auto& tickPtr = inst_info->m_reportLatest;
		double FrozenMargin = 0;
			
		auto trader = GetTraderIDByFronIDSessionID(inputOrder->FrontID, inputOrder->SessionID);
		auto pMarginRate = trader->GetInstMarginRate(inputOrder->InstrumentID);

		auto pCommisionRate = trader->GetInstCommissionRate(inputOrder->InstrumentID);
		if (pMarginRate && pCommisionRate)
		{
			double FrozenCommission = 0;
			if (inputOrder->CombOffsetFlag[0] == OF_Open)
			{
				//开仓手续费率;
				//（空头冻结量+多头冻结量） * 开仓手续费率  -- 按手数
				//	SUM（（空头冻结量 + 多头冻结量） * 报单价格 * 合约乘数 * 开仓手续费率） -- 按金额"
				FrozenCommission +=
					inputOrder->VolumeTotalOriginal*inputOrder->LimitPrice*
					inst_info->VolumeMultiple*pCommisionRate->OpenRatioByMoney
					+ inputOrder->VolumeTotalOriginal*pCommisionRate->OpenRatioByVolume;
				double FrozenMargineMoney = inputOrder->VolumeTotalOriginal*
					tickPtr.LastPrice
					* inst_info->VolumeMultiple;
				//只有开仓才会冻结保证金;
				if (inputOrder->Direction == D_Buy)
				{
					FrozenMargin =
						FrozenMargineMoney * pMarginRate->LongMarginRatioByMoney
						+
						FrozenMargineMoney * pMarginRate->LongMarginRatioByVolume;
				}
				else
				{
					FrozenMargin =
						FrozenMargineMoney * pMarginRate->ShortMarginRatioByMoney
						+
						FrozenMargineMoney * pMarginRate->ShortMarginRatioByVolume;
				}
				inputOrder->FrozenMargin = FrozenMargin;
			}
			else
			{
				//平仓的话,要冻结相应的仓位;
				auto posiIter = std::find_if(m_MyPositions->begin(), m_MyPositions->end(),
					[inputOrder](const boost::shared_ptr<InvestorPosition>& ip)
				{
					bool isFind = (ip->InstrumentID == inputOrder->InstrumentID);
					if (inputOrder->Direction == D_Buy)
					{
						//需要查找空头仓位;
						return isFind && ip->PosiDirection == PD_Short && (ip->Position >= inputOrder->VolumeTotalOriginal);
					}
					else
					{
						return isFind && ip->PosiDirection == PD_Long && (ip->Position >= inputOrder->VolumeTotalOriginal);
					}
					return isFind;
				});
				if (posiIter != m_MyPositions->end())
				{
					auto posi = *posiIter;
					if (inputOrder->Direction == D_Buy)
					{
						//需要查找空头仓位;
						posi->ShortFrozen += inputOrder->VolumeTotalOriginal;
					}
					else
					{
						posi->LongFrozen += inputOrder->VolumeTotalOriginal;
					}
				}
				if (inputOrder->CombOffsetFlag[0] == OF_CloseToday)
				{
					FrozenCommission +=
						inputOrder->VolumeTotalOriginal*pCommisionRate->CloseTodayRatioByMoney
						+ inputOrder->VolumeTotalOriginal*pCommisionRate->CloseTodayRatioByVolume;
				}
				else
				{
					FrozenCommission +=
						inputOrder->VolumeTotalOriginal*pCommisionRate->CloseRatioByMoney
						+ inputOrder->VolumeTotalOriginal*pCommisionRate->CloseRatioByVolume;
					//平仓的话,要冻结相应的仓位;
				}
			}
			
			inputOrder->FrozenCommission = FrozenCommission;
			m_TradingAccount->FrozenCommission += FrozenCommission;
			m_TradingAccount->FrozenMargin += FrozenMargin;
			m_TradingAccount->Available -= (FrozenMargin + FrozenCommission);
		}
	}
	//加入到报单列表里面去;
	auto insertRes = m_MyInputOrders->insert(std::make_pair(inputOrder->OrderRef, inputOrder));
	return insertRes.second;
}

bool Portfolio::Update(boost::shared_ptr<Tick> tick)
{
	if (m_TradingAccountsToday->empty())
	{
		auto firstTradingAccount =boost::make_shared<TradingAccount>(*m_TradingAccount);
		firstTradingAccount->TradingDay = tick->UpdateTimeStr;
		m_TradingAccountsToday->push_back(firstTradingAccount);
	}
	if (!m_MyPositions->empty())
	{
		double totalPositionProfit = m_TradingAccount->PositionProfit, totalUseMargin = 0;
		boost::shared_ptr<InstrumentInfo> inst_info = GetInstrumentInfo(tick->InstrumentID());
		auto tickPtr = &(inst_info->m_reportLatest);
		//计算持仓盈亏;
		for (size_t i = 0; i < m_MyPositions->size(); ++i)
		{
			//"（最新价 - 持仓均价）* 持仓总量 * 合约乘数 -- 多头
			//	（持仓均价 - 最新价）* 持仓总量 * 合约乘数 -- 空头"
			boost::shared_ptr<InvestorPosition> ip = (*m_MyPositions)[i];
			if (ip->InstrumentID == tick->InstrumentID())
			{
				if (ip->PositionDate == PSD_History)
				{
					//昨仓;
					ip->PositionCost = tick->PreSettlementPrice;
				}
				ip->PreSettlementPrice = tick->PreSettlementPrice;
				double positionProfit = (tick->LastPrice - ip->PositionCost)*
					ip->Position*inst_info->VolumeMultiple;
				if (ip->PosiDirection == PD_Short)
				{
					//空头;
					positionProfit *= -1;
				}
				totalPositionProfit -= ip->PositionProfit;
				ip->PositionProfit = positionProfit;
				totalPositionProfit += positionProfit;
// 				LOGDEBUG("{} positionProfit={},UseMargin={},Commission={}", GetId(), positionProfit,
// 					ip->UseMargin, ip->Commission);
				break;
			}
		}
		m_TradingAccount->PositionProfit = totalPositionProfit;
		auto firstTradingAccount = boost::make_shared<TradingAccount>(*m_TradingAccount);
		firstTradingAccount->TradingDay = tick->UpdateTimeStr;
		m_TradingAccountsToday->push_back(firstTradingAccount);
	}
	return true;
}

bool Portfolio::Update(boost::shared_ptr<ParkedOrder> parkedOrder)
{
	(*m_MyParkOrders)[parkedOrder->ParkedOrderID] = parkedOrder;
	return true;
}

bool Portfolio::IsMyOrder(boost::shared_ptr<Order> order)
{
	return true;
}

bool Portfolio::IsMyTrade(boost::shared_ptr<Trade> trade)
{
	return true;
}

void Portfolio::SaveInputOrders()
{
	//保存报单信息;
	auto fileNameFmt = boost::format("%1%_InputOrders%2%");
	std::string binFileName = boost::str(fileNameFmt % GetId() % ".bin");
	std::ofstream ofs(binFileName, std::ios::binary);

	if (ofs.is_open())
	{
		boost::archive::binary_oarchive oa(ofs);

		oa << m_MyInputOrders;

		ofs.close();
	}
	//保存CSV;
	std::string csvFileName = boost::str(fileNameFmt % GetId() % ".csv");
	mini::csv::ofstream os(csvFileName, std::ios_base::app);
	os.set_delimiter(',', "$$");

	if (os.is_open())
	{
		for (auto iter = m_MyInputOrders->begin(); iter != m_MyInputOrders->end(); ++iter)
		{
			os << *(iter->second) << NEWLINE;
		}
	}
	os.flush();
	os.close();
}

void Portfolio::SaveInputOrderActions()
{
	auto fileNameFmt = boost::format("%1%_InputOrderActions%2%");
	std::string binFileName = boost::str(fileNameFmt % GetId() % ".bin");
	//保存报单信息;
	std::ofstream ofs(binFileName, std::ios::binary);

	if (ofs.is_open())
	{
		boost::archive::binary_oarchive oa(ofs);

		oa << m_MyInputOrderActions;

		ofs.close();
	}

	//保存CSV;
	std::string csvFileName = boost::str(fileNameFmt % GetId() % ".csv");
	mini::csv::ofstream os(csvFileName, std::ios_base::app);
	os.set_delimiter(',', "$$");

	if (os.is_open())
	{
		for (auto iter = m_MyInputOrderActions->cbegin(); iter != m_MyInputOrderActions->cend(); ++iter)
		{
			os << *(iter->second) << NEWLINE;
		}
	}
	os.flush();
	os.close();
}

void Portfolio::SaveOrders()
{
	auto fileNameFmt = boost::format("%1%_InputOrderActions%2%");
	std::string binFileName = boost::str(fileNameFmt % GetId() % ".bin");
	//保存报单信息;
	std::ofstream ofs(binFileName, std::ios::binary);

	if (ofs.is_open())
	{
		boost::archive::binary_oarchive oa(ofs);

		oa << m_MyOrders;

		ofs.close();
	}

	//保存CSV;
	std::string csvFileName = boost::str(fileNameFmt % GetId() % ".csv");
	mini::csv::ofstream os(csvFileName, std::ios_base::app);
	os.set_delimiter(',', "$$");

	if (os.is_open())
	{
		for (auto iter = m_MyOrders->begin(); iter != m_MyOrders->end(); ++iter)
		{
			os << *(iter->second) << NEWLINE;
		}
	}
	os.flush();
	os.close();
}


bool Portfolio::LoadClosedPositions(boost::shared_ptr<std::vector<boost::shared_ptr<InvestorPosition> > > positions)
{
	if (!positions)
	{
		return false;
	}
	auto fileNameFmt = boost::format("%1%_Positions%2%");
	std::string csvFileName = boost::str(fileNameFmt % GetId() % "_Closed.csv");
	mini::csv::ifstream is(csvFileName);
	is.set_delimiter(',', "$$");

	if (is.is_open())
	{
		while (is.read_line())
		{
			boost::shared_ptr<InvestorPosition> investorPosition = boost::make_shared<InvestorPosition>();
			is >> *investorPosition;
			positions->push_back(investorPosition);
		}
	}
	is.close();
	return true;
}


void Portfolio::SavePositions()
{
	auto fileNameFmt = boost::format("%1%_Positions%2%");
	std::string binFileName = boost::str(fileNameFmt % GetId() % ".bin");
	//保存报单信息;
	std::ofstream ofs(binFileName, std::ios::binary);

	if (ofs.is_open())
	{
		boost::archive::binary_oarchive oa(ofs);

		oa << m_MyPositions;

		ofs.close();
	}

	//将已平仓数据追加保存;
	{
		std::string csvFileName = boost::str(fileNameFmt % GetId() % "_Closed.csv");
		mini::csv::ofstream os(csvFileName, std::ios_base::app);
		os.set_delimiter(',', "$$");

		if (os.is_open())
		{
			for (auto iter = m_ClosePositions->begin(); iter != m_ClosePositions->end(); ++iter)
			{
				os << *(*iter) << NEWLINE;
			}
		}
		os.flush();
		os.close();
	}

	//保存CSV;
	std::string csvFileName = boost::str(fileNameFmt % GetId() % ".csv");
	mini::csv::ofstream os(csvFileName);
	os.set_delimiter(',', "$$");

	if (os.is_open())
	{
		for (auto iter = m_MyPositions->begin(); iter != m_MyPositions->end(); ++iter)
		{
			os << *(*iter) << NEWLINE;
		}
	}
	os.flush();
	os.close();
}

void Portfolio::SaveTrades()
{
	auto fileNameFmt = boost::format("%1%_Trades%2%");
	std::string binFileName = boost::str(fileNameFmt % GetId() % ".bin");

	//保存报单信息;
	std::ofstream ofs(binFileName, std::ios::binary);

	if (ofs.is_open())
	{
		boost::archive::binary_oarchive oa(ofs);

		oa << m_MyTrades;

		ofs.close();
	}
	//保存CSV;
	std::string csvFileName = boost::str(fileNameFmt % GetId() % ".csv");
	mini::csv::ofstream os(csvFileName, std::ios_base::app);
	os.set_delimiter(',', "$$");

	if (os.is_open())
	{
		for (auto iter = m_MyTrades->begin(); iter != m_MyTrades->end(); ++iter)
		{
			os << *(iter->second) << NEWLINE;
		}
	}
	os.flush();
	os.close();
}

void Portfolio::SaveTradingAccounts()
{
	//保存当前资金信息;loading
// 	auto fileNameFmt = boost::format("%1%_%2%%3%");
// 	std::string csvFileName = boost::str(fileNameFmt % GetId() % "TradingAccount" % ".csv");
// 	mini::csv::ofstream os(csvFileName,std::ios_base::app);
// 	os.set_delimiter(',', "$$");
// 	if (os.is_open())
// 	{
// 		os << *m_TradingAccount << NEWLINE;
// 		os.flush();
// 		os.close();
// 	}
	SaveTradingAccounts(m_TradingAccounts, "TradingAccount");
	SaveTradingAccounts(m_TradingAccountsToday, "TradingAccountToday");
}

bool Portfolio::SaveTradingAccounts(boost::shared_ptr<std::vector<boost::shared_ptr<TradingAccount> > > tradingAccounts, 
	const std::string& keyName)
{
	if (!tradingAccounts)
	{
		return false;
	}
	//保存资金信息;
	auto fileNameFmt = boost::format("%1%_%2%%3%");
	std::string csvFileName = boost::str(fileNameFmt % GetId() % keyName % ".csv");
	mini::csv::ofstream os(csvFileName);
	os.set_delimiter(',', "$$");
	if (os.is_open())
	{
		for (auto iter = tradingAccounts->begin(); iter != tradingAccounts->end(); ++iter)
		{
			os << *(*iter) << NEWLINE;
		}
		os.flush();
		os.close();
		return true;
	}
	return false;
}

void Portfolio::LoadOrders()
{
	//保存报单信息;
	std::ifstream ifs(GetId() + "_Orders.bin", std::ios::binary);

	if (ifs.is_open())
	{
		try
		{
			boost::archive::binary_iarchive oa(ifs);
			oa >> m_MyOrders;
		}
		catch (const boost::archive::archive_exception& e)
		{
			LOGDEBUG("Load Order Exception:{}", e.what());
		}
		ifs.close();
	}
}

void Portfolio::LoadPositions()
{
	//保存报单信息;
	std::ifstream ifs(GetId() + "_Positions.bin", std::ios::binary);
	if (ifs.is_open())
	{
		try
		{
			boost::archive::binary_iarchive oa(ifs);
			oa >> m_MyPositions;
			//如果有必要,修改今仓为昨仓;
			for (auto pIter = m_MyPositions->begin(); pIter != m_MyPositions->end(); ++pIter)
			{
				auto trader = UserApiMgr::GetInstance().GetTrader((*pIter)->InvestorID, (*pIter)->BrokerID);
				if (!trader)
				{
					continue;
				}

				LOGDEBUG("OpenDate:{},TradingDay:{}", (*pIter)->OpenDate, trader->GetTradingDay());
				if ((*pIter)->OpenDate != trader->GetTradingDay())
				{
					(*pIter)->PositionDate = PSD_History;
					(*pIter)->YdPosition += (*pIter)->TodayPosition;
					(*pIter)->TodayPosition = 0;
				}
			}
		}
		catch (const boost::archive::archive_exception& e)
		{
			LOGDEBUG("Load Positions Exception:{}", e.what());
		}
		ifs.close();
	}
}

void Portfolio::LoadTrades()
{
	//保存报单信息;
	std::ifstream ifs(GetId() + "_Trades.bin", std::ios::binary);

	if (ifs.is_open())
	{

		try
		{
			boost::archive::binary_iarchive oa(ifs);

			oa >> m_MyTrades;
		}
		catch (const boost::archive::archive_exception& e)
		{
			LOGDEBUG("Load Trades Exception:{}", e.what());
		}
	}
}


boost::shared_ptr<std::map<std::string, boost::shared_ptr<Order>> > Portfolio::GetOrders(bool bHistory/* = false*/)
{
	if (bHistory)
	{
		return m_HistoryOrders;
	}
	return m_MyOrders;
}

boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrder> > > Portfolio::GetInputOrders()
{
	return m_MyInputOrders;
}

boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrderAction> > > Portfolio::GetInputOrderActions()
{
	return m_MyInputOrderActions;
}

boost::shared_ptr<std::vector<boost::shared_ptr<InvestorPosition> > > Portfolio::GetPositions()
{
	return m_MyPositions;
}

boost::shared_ptr < std::vector<boost::shared_ptr<InvestorPosition> > > Portfolio::GetClosedPositions()
{
	boost::shared_ptr < std::vector<boost::shared_ptr<InvestorPosition> > > result =
		boost::make_shared < std::vector<boost::shared_ptr<InvestorPosition> > >();
	LoadClosedPositions(result);
	std::copy(m_ClosePositions->begin(),m_ClosePositions->end(),std::back_inserter(*result));
	return m_ClosePositions;
}

boost::shared_ptr < std::vector<boost::shared_ptr<TradingAccount> > > Portfolio::GetTradingAccounts()
{
	if (m_TradingAccounts->empty())
	{
		m_TradingAccounts->push_back(m_TradingAccount);
	}
	return m_TradingAccounts;
}

boost::shared_ptr<std::map<std::string, boost::shared_ptr<Trade> > > Portfolio::GetTrades(bool bHistory /*= false*/)
{
	if (bHistory)
	{
		return m_HistoryTrades;
	}
	return m_MyTrades;
}


boost::shared_ptr<InvestorPosition> Portfolio::Trade2Position(boost::shared_ptr<Trade> trade)
{
	if (!trade)
	{
		return nullptr;
	}
	if (trade->OrderSysID.empty())
	{
		return nullptr;
	}
	if (trade->OffsetFlag != OF_Open)
	{
		//不是开仓;
		return nullptr;
	}
	boost::shared_ptr<InvestorPosition> position = boost::make_shared<InvestorPosition>();
	position->YdPosition = 0;
	position->TodayPosition = trade->Volume;
	position->Position = trade->Volume;
	position->OpenVolume = trade->Volume;
	position->CloseVolume = 0;
	position->OpenCost = 0;
	position->Commission = 0;
	position->UseMargin = 0;
	position->OpenPrice = trade->Price;
	position->CloseProfitByTrade = 0;
	position->CloseProfitByDate = 0;
	position->PositionProfit = 0;
	position->CloseProfit = 0;

	position->ClientID = trade->ClientID;
	position->BrokerID = trade->BrokerID;
	position->InvestorID = trade->InvestorID;
	position->InstrumentID = trade->InstrumentID;
	position->ExchangeID = trade->ExchangeID;
	position->HedgeFlag = trade->HedgeFlag;
	position->PosiDirection = trade->Direction == D_Buy ? PD_Long : PD_Short;
	position->PositionDate = PSD_Today;

	position->SettlementID = trade->SettlementID;
	position->TradingDay = trade->TradingDay;
	position->OpenCost = trade->Price;
	position->PositionCost = trade->Price;
	position->OpenDate = trade->TradingDay;

	position->HedgeFlag = trade->HedgeFlag;
	return position;
}

std::string Portfolio::GetId()
{
	return m_Id;
}

boost::shared_ptr<InstrumentCommisionRate> Portfolio::GetInstCommissionRate(const std::string& instId)
{
	return nullptr;
}

boost::shared_ptr<InstrumentMarginRate> Portfolio::GetInstMarginRate(const std::string& instId)
{
	return nullptr;
}


boost::shared_ptr<BrokerTradingParams> Portfolio::GetBrokerTradingParams()
{
	return nullptr;
}

boost::shared_ptr<InstrumentInfo> Portfolio::GetInstrumentInfo(const std::string& InstId)
{
	boost::shared_ptr<InstrumentInfo> info = boost::make_shared<InstrumentInfo>();
	InstrumentContainer::get_instance().GetInstrumentInfo(InstId.c_str(), info.get());
	return info;
}


boost::shared_ptr<Trader> Portfolio::GetTraderIDByFronIDSessionID(int frontId, int SessionId)
{
	std::map<std::string, boost::shared_ptr<Trader> >& traders = UserApiMgr::GetInstance().GetTraders();
	auto titer = traders.begin();
	while (titer != traders.end())
	{
		if (titer->second)
		{
			if (titer->second->GetUser().FrontID == frontId && titer->second->GetUser().SessionID == SessionId)
			{
				return titer->second;
			}
		}
		++titer;
	}
	return nullptr;
}

void Portfolio::LoadTradingAccounts()
{
	//读取资金信息;
	auto fileNameFmt = boost::format("%1%_TradingAccount%2%");
	std::string csvFileName = boost::str(fileNameFmt % GetId() % ".csv");
	mini::csv::ifstream is(csvFileName);
	is.set_delimiter(',', "$$");
	if (is.is_open())
	{
		while (is.read_line())
		{
			boost::shared_ptr<TradingAccount> tradingAccount = boost::make_shared<TradingAccount>();
			is >> *tradingAccount;
			//交易日发生了变化,要进行结算;
			m_TradingAccounts->push_back(tradingAccount);
		}
	}
	is.close();
	if (!m_TradingAccounts->empty())
	{
		auto tradingDay = m_TradingAccount->TradingDay;
		*m_TradingAccount = *m_TradingAccounts->back();
		if (m_TradingAccounts->back()->TradingDay!=tradingDay)
		{
			m_TradingAccount->TradingDay = tradingDay;
			m_TradingAccounts->push_back(m_TradingAccount);
		}
		else
		{
			m_TradingAccounts->back() = m_TradingAccount;
		}
	}
	
}

boost::shared_ptr < std::vector<boost::shared_ptr<TradingAccount> > > Portfolio::GetTradingAccountsToday()
{
	return m_TradingAccountsToday;
}

boost::shared_ptr<std::map<std::string, boost::shared_ptr<ParkedOrder> > > Portfolio::GetParkOrders()
{
	return m_MyParkOrders;
}

int Portfolio::GetCancelCount(const std::string& InstrumentID)
{
	//先获取合约的品种;
	auto occIter = m_OrderCancelCount.find(InstrumentID);
	if (occIter == m_OrderCancelCount.end())
	{
		InstrumentInfo info;
		if (InstrumentContainer::get_instance().get_info_by_id(InstrumentID, &info))
		{
			occIter = m_OrderCancelCount.find(info.ProductID);
			if (occIter == m_OrderCancelCount.end())
			{
				return 0;
			}
			else
			{
				return occIter->second;
			}
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return occIter->second;
	}
}
