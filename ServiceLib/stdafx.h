// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
#ifndef _STDAFX_H_
#define _STDAFX_H_

#pragma warning(disable : 4786)	// too long identifiers.
#pragma warning(disable : 4244)
//今天编译Relese版本项目是遇到【LINK : warning LNK4089: all references to "GDI32.dll" discarded by /OPT:REF】
//							 这种警告解决办法：
//							 1.Project--Setting--Link 选项卡，在Project Options 处添加 /opt:noref
//							 2.Project--Setting--Link 选项卡，在Project Options 处添加 /IGNORE:4089
//							 3.在程序中添加 #pragma warning(disable:4089)

//							 注意：因为使用【/IGNORE:4089】生成的文件大小比【/opt:noref】小，个人推荐使用【/IGNORE:4089】

//在网上找到一些原因如下:
//						 这个警告只是简单的意味你错误的连接一个库(library),编译器查到你并没有使用其中的函数.这个警告在你做发布版(release builds)的时候是很平常的,因为/OPT:REF的连接器选项在发布版本(release builds)被设置成为命令连接器修正你的最终代码尽可能的避免连接无用的dlls.(这使得可执行代码的import地址表尽量的小).
#pragma warning(disable:4089)
#include <iostream>
#include <vector>
using namespace std;


//#include "../BasicLib/dbg.h"
//extern BASICLIB_API CDebugPrintf debug;


#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

#endif


