// DualMA.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"

#include "DualMAStrategy.h"
#include <boost/dll/alias.hpp>
#include <boost/dll.hpp>


boost::shared_ptr<IStrategy> CreateStrategy(const std::string& sid);
BOOST_DLL_ALIAS(CreateStrategy, create_strategy)