#include "stdafx.h"
#include "UOS.h"
#include "OSC.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CUOS
CUOS::CUOS()
{
	SetDefaultParameters();
}

CUOS::CUOS(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CUOS::~CUOS()
{
	clear();
}

void CUOS::SetDefaultParameters()
{
	m_nDays1 = 7;
	m_nDays2 = 14;
	m_nDays3 = 28;
	m_nMADays = 6;
	m_itsGoldenFork = ITS_BUY;
	m_itsDeadFork = ITS_SELL;
}

void CUOS::AttachParameters(CUOS & src)
{
	m_nDays1 = src.m_nDays1;
	m_nDays2 = src.m_nDays2;
	m_nDays3 = src.m_nDays3;
	m_nMADays = src.m_nMADays;
	m_itsGoldenFork = src.m_itsGoldenFork;
	m_itsDeadFork = src.m_itsDeadFork;
}

bool CUOS::IsValidParameters()
{
	return (VALID_DAYS(m_nDays1) && VALID_DAYS(m_nDays2) && VALID_DAYS(m_nDays3) && VALID_DAYS(m_nMADays)
		&& VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork));
}

void CUOS::clear()
{
	TechnicalIndicator::clear();
}

int CUOS::signal(size_t nIndex, uint32_t * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	int	nMaxDays = max(max(m_nDays1, m_nDays2), m_nDays3) + m_nMADays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if (!intensity_prepare(nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.4, 0.6))
		return ITS_NOTHING;

	double	dUOS;
	if (!calc(&dUOS, nIndex, false))
		return ITS_NOTHING;

	int	nSignal = GetForkSignal(nIndex, m_itsGoldenFork, m_itsDeadFork, pnCode);
	if (dUOS < dLiminalLow && nSignal == m_itsGoldenFork)
	{	// 低位金叉
		if (pnCode)	*pnCode = ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if (dUOS > dLiminalHigh && nSignal == m_itsDeadFork)
	{	// 高位死叉
		if (pnCode)	*pnCode = ITSC_DEADFORK;
		return m_itsDeadFork;
	}

	return	ITS_NOTHING;
}

bool CUOS::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo2(nStart, nEnd, pdMin, pdMax, this);
}

/***
OSC1 = m_nDays1日OSC
OSC2 = m_nDays2日OSC
OSC3 = m_nDays3日OSC
UOS = (OSC1+OSC2+OSC3)/3
*/
bool CUOS::calc(double * pValue, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (load_from_cache(nIndex, pValue))
		return true;

	double	dOSC1 = 0, dOSC2 = 0, dOSC3 = 0;
	COSC	osc1(m_pKData), osc2(m_pKData), osc3(m_pKData);
	osc1.m_nDays = m_nDays1;
	osc2.m_nDays = m_nDays2;
	osc3.m_nDays = m_nDays3;
	if (osc1.calc(&dOSC1, nIndex, false)
		&& osc2.calc(&dOSC2, nIndex, false)
		&& osc3.calc(&dOSC3, nIndex, false))
	{
		if (pValue)
			*pValue = (dOSC1 + dOSC2 + dOSC3) / 3;
		store_to_cache(nIndex, pValue);
		return true;
	}
	return false;
}

/***
计算UOS及其移动平均值
*/
bool CUOS::calc(double * pValue, double * pMA, int nIndex, bool bUseLast)
{
	return calc_ma(pValue, pMA, nIndex, bUseLast, m_nMADays);
}
