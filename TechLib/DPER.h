#pragma once
#include "TechLib.h"
#include "Technique.h"


//	去势百分比指标DPER
class TECH_API CDPER : public TechnicalIndicator
{
public:
	// Constructors
	CDPER();
	CDPER(KdataContainer * pKData);
	virtual ~CDPER();

public:
	virtual	void clear();

	// Attributes
	uint32_t		m_nDays;
	uint32_t		m_nMADays;
	uint32_t		m_nDetrendDays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters();
	void	AttachParameters(CDPER & src);
	virtual	bool	IsValidParameters();

	// Operations
	virtual	int		signal(size_t nIndex, uint32_t * pnCode = NULL);
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pValue, size_t nIndex, bool bUseLast);
	virtual	bool	calc(double * pValue, double * pMA, size_t nIndex, bool bUseLast);
};

