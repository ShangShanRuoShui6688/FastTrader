//#include "stdafx.h"
#include "MFI.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CMFI
CMFI::CMFI()
{
	SetDefaultParameters();
}

CMFI::CMFI(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CMFI::~CMFI()
{
	clear();
}

void CMFI::SetDefaultParameters()
{
	m_nDays = 10;
	m_itsLong = ITS_BUY;
	m_itsShort = ITS_SELL;
}

void CMFI::AttachParameters(CMFI & src)
{
	m_nDays = src.m_nDays;
	m_itsLong = src.m_itsLong;
	m_itsShort = src.m_itsShort;
}

bool CMFI::IsValidParameters()
{
	return (VALID_DAYS(m_nDays) && VALID_ITS(m_itsLong) && VALID_ITS(m_itsShort));
}

void CMFI::clear()
{
	TechnicalIndicator::clear();
}

int CMFI::signal(int nIndex, UINT * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	double	dValue = 0;
	if (!calc(&dValue, nIndex, false))
		return ITS_NOTHING;

	if (dValue <= 20)
	{	// 低位
		if (pnCode)	*pnCode = ITSC_LONG;
		return m_itsLong;
	}
	if (dValue >= 80)
	{	// 高位
		if (pnCode)	*pnCode = ITSC_SHORT;
		return m_itsShort;
	}
	return	ITS_NOTHING;
}

bool CMFI::min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax)
{
	if (pdMin)	*pdMin = 0;
	if (pdMax)	*pdMax = 100;
	return true;
}

/***
PMF 和 NMF 如下计算：

TP = (High+Low+Close)/3   当日的中间价

PMF = n日内，TP上涨日的 (TP*成交量) 之和。
NMF = n日内，TP下降日的 (TP*成交量) 之和。

MFI = 100 * PMF / (PMF + NMF)
备注：MR = PMF/NMF
*/
bool CMFI::calc(double * pValue, int nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (m_nDays > nIndex)
		return false;

	if (load_from_cache(nIndex, pValue))
		return true;

	double	dPMF = 0, dNMF = 0;
	int	nCount = 0;
	for (int k = nIndex; k >= 1; k--)
	{
		KDATA&	kd = m_pKData->at(k);
		KDATA&	kdLast = m_pKData->at(k - 1);
		double	dTP = (kd.HighestPrice + kd.LowestPrice + kd.ClosePrice) / 3.;
		double	dTPLast = (kdLast.HighestPrice + kdLast.LowestPrice + kdLast.ClosePrice) / 3.;
		if (dTP > dTPLast)
			dPMF += dTP * kd.Volume;
		if (dTPLast > dTP)
			dNMF += dTP * kd.Volume;

		nCount++;
		if (nCount == m_nDays)
		{
			if (fabs(dPMF + dNMF) < 1e-4)
				return false;
			if (pValue)	*pValue = 100 * dPMF / (dPMF + dNMF);
			store_to_cache(nIndex, pValue);
			return true;
		}
	}

	return false;
}
