#ifndef _TECH_OSC_H_
#define _TECH_OSC_H_

//	摆动量OSC
#include "TechLib.h"
#include "Technique.h"
//	简易波动指标EMV
class TECH_API  COSC : public TechnicalIndicator
{
public:
	// Constructors
	COSC( );
	COSC( KdataContainer * pKData );
	virtual ~COSC();

public:
	virtual	void clear( );

	// Attributes
	int		m_nDays;
	int		m_nMADays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	attach( COSC & src );
	virtual	bool	IsValidParameters( );

	// Operations
    virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
    virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
    virtual	bool	calc( double * pValue, size_t nIndex, bool bUseLast );
    virtual	bool	calc( double * pValue, double * pMA, size_t nIndex, bool bUseLast );
};
#endif
