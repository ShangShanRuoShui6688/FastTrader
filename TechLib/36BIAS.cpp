#include "36BIAS.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
//////////////////////////////////////////////////////////////////////
//	C36BIAS
C36BIAS::C36BIAS( )
{
	SetDefaultParameters( );
}

C36BIAS::C36BIAS( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

C36BIAS::~C36BIAS()
{
	clear( );
}

void C36BIAS::SetDefaultParameters( )
{
	m_itsSold	=	ITS_BUY;
	m_itsBought	=	ITS_SELL;
}

void C36BIAS::attach( C36BIAS & src )
{
	m_itsSold	=	src.m_itsSold;
	m_itsBought	=	src.m_itsBought;
}

bool C36BIAS::IsValidParameters( )
{
	return ( VALID_ITS(m_itsSold) && VALID_ITS(m_itsBought) );
}

void C36BIAS::clear( )
{
	TechnicalIndicator::clear( );
}

int C36BIAS::signal( size_t nIndex, uint32_t * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	int	nMaxDays	=	6;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if( !intensity_prepare( nIndex, pnCode, nMaxDays, 40, &dLiminalLow, &dLiminalHigh, 0.02, 0.98 ) )
		return ITS_NOTHING;

	double	d36BIAS;
	if( !calc( &d36BIAS, nIndex, false ) )
		return ITS_NOTHING;

	if( d36BIAS < dLiminalLow )
	{	// 超卖
		if( pnCode )	*pnCode	=	ITSC_OVERSOLD;
		return m_itsSold;
	}
	if( d36BIAS > dLiminalHigh )
	{	// 超买
		if( pnCode )	*pnCode	=	ITSC_OVERBOUGHT;
		return m_itsBought;
	}
	
	return	ITS_NOTHING;
}

bool C36BIAS::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax )
{
	return AfxGetMinMaxInfo1( nStart, nEnd, pdMin, pdMax, this );
}

/***
	3-6BIAS = 3日收盘价移动平均值 - 6日收盘价移动平均值
*/
bool C36BIAS::calc(double * pValue, size_t nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( load_from_cache( nIndex, pValue ) )
		return true;

	double	dMA1 = 0, dMA2 = 0;
	int	nCount	=	0;
	for( int k=nIndex; k>=0; k-- )
	{
		if( nCount < 3 )
			dMA1	+=	m_pKData->MaindataAt(k);
		if( nCount < 6 )
			dMA2	+=	m_pKData->MaindataAt(k);

		nCount	++;
		if( nCount >= 3 && nCount >= 6 )
		{
			if( pValue )
				*pValue	=	(dMA1/3 - dMA2/6);
			store_to_cache( nIndex, pValue );
			return true;
		}
	}

	return false;
}
