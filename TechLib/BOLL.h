#ifndef _TECH_BOLL_H_
#define _TECH_BOLL_H_
#include "TechLib.h"
#include "Technique.h"
//	���ִ�BOLL
class TECH_API CBOLL : public TechnicalIndicator
{
public:
	// Constructors
	CBOLL( );
	CBOLL( KdataContainer * pKData );
	virtual ~CBOLL();

public:
	virtual	void clear( );

	// Attributes
	double	m_dMultiUp;
	double	m_dMultiDown;
	uint32_t		m_nMADays;
	int		m_itsSupport;
	int		m_itsResistance;
	virtual	void	SetDefaultParameters( );
	void	attach( CBOLL & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pdMA, double * pdUp, double * pdDown, size_t nIndex, bool bUseLast);
};
#endif