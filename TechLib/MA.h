#ifndef _TECH_MA_H_
#define _TECH_MA_H_
#include "TechLib.h"
#include "Technique.h"
#include <vector>
/////////////////////////////////////////////////////////////////////////
//	K线属性类;
//	移动平均线MA;
class TECH_API CMA : public TechnicalIndicator
{
public:
	// Constructors
	CMA( );
	CMA( KdataContainer * pKData );
	virtual ~CMA();

public:
	virtual	void clear( );

	// Attributes
	enum MATypes {
		typeMA		=	0x01,
		typeEXPMA	=	0x02,
		typeSMA		=	0x03,
		typeDMA		=	0x04,
	};
	int				m_nType;
	std::vector<uint32_t>	m_adwMADays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	int		m_itsLong;
	int		m_itsShort;
	double  m_dWeight;
	virtual	void	SetDefaultParameters( );
	void	attach( CMA & src );
	virtual	bool	IsValidParameters( );

	// Operations;
    virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
    virtual	bool	min_max_info( size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
    virtual	bool	calc(double * pValue, size_t nIndex, size_t nDays, bool bUseLast );
};
#endif
