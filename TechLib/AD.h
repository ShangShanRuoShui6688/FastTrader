#pragma once
#include "Technique.h"

//	��/ɢָ��A/D
class TECH_API CAD : public TechnicalIndicator
{
public:
	// Constructors
	CAD( );
	CAD( KdataContainer * pKData );
	virtual ~CAD();

public:
	virtual	void clear( );

	// Attributes
	uint32_t				m_nDays;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CAD & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pValue, size_t nIndex, bool bUseLast);
};

