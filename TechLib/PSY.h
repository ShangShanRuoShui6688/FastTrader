#ifndef _TECH_PSY_H_
#define _TECH_PSY_H_
#include "TechLib.h"
#include "Technique.h"
//	������PSY
class TECH_API  CPSY : public TechnicalIndicator
{
public:
	// Constructors
	CPSY( );
	CPSY( KdataContainer * pKData );
	virtual ~CPSY();

public:
	virtual	void clear( );

	// Attributes
	int		m_nDays;
	int		m_itsSold;
	int		m_itsBought;
	virtual	void	SetDefaultParameters( );
	void	attach( CPSY & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( int nIndex, UINT * pnCode = NULL );
	virtual	bool	min_max_info( int nStart, int nEnd, double *pdMin, double *pdMax );
	virtual	bool	calc( double * pValue, int nIndex, bool bUseLast );
};
#endif