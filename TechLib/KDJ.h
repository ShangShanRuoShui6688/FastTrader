#pragma once
#include "TechLib.h"
#include "Technique.h"
//	随机指标KDJ
class TECH_API CKDJ : public TechnicalIndicator
{
public:
	// Constructors
	CKDJ( );
	CKDJ( KdataContainer * pKData );
	virtual ~CKDJ();

protected:
	bool	CalculateRSV( double * pValue, size_t nIndex );

public:
	virtual	void clear( );

	// Attributes
	enum modeJ	{
		mode3K2D	=	0x01,
		mode3D2K	=	0x02,
	};
	uint32_t		m_nRSVDays;
	uint32_t		m_nKDays;
	uint32_t		m_nDDays;
	int		m_nJ;		//	1 For 3K-2D, 2 For 3D-2K
	//K线和D线在25(first)以下在75(second)以上时信号较为准确;
	std::pair<int, int>     m_KDTrustValue;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	attach( CKDJ & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal(size_t nIndex, uint32_t * pnCode = NULL);
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
	virtual	bool	calc( double *pValue1, double *pValue2, double *pValue3, size_t nIndex, bool bUseLast );
};