#include "ASI.H"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
//////////////////////////////////////////////////////////////////////
//	CASI
CASI::CASI( )
{
	SetDefaultParameters( );
}

CASI::CASI( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CASI::~CASI()
{
	clear( );
}

void CASI::SetDefaultParameters( )
{
	m_nDays		=	6;
	m_itsDeviateOnBottom	=	ITS_BUY;
	m_itsDeviateOnTop		=	ITS_SELL;
}

void CASI::attach( CASI & src )
{
	m_nDays		=	src.m_nDays;
	m_itsDeviateOnBottom	=	src.m_itsDeviateOnBottom;
	m_itsDeviateOnTop		=	src.m_itsDeviateOnTop;
}

bool CASI::IsValidParameters( )
{
	return ( VALID_DAYS(m_nDays) && VALID_ITS(m_itsDeviateOnBottom) && VALID_ITS(m_itsDeviateOnTop) );
}

void CASI::clear( )
{
	TechnicalIndicator::clear( );
}

int CASI::signal( size_t nIndex, uint32_t * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	int	nMaxDays	=	m_nDays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if( !intensity_prepare( nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh ) )
		return ITS_NOTHING;

	if( is_deviate_on_bottom( nIndex, m_pdCache1, m_pdCache2 ) )
	{	// 底背离
		if( pnCode )	*pnCode	=	ITSC_DEVIATEONBOTTOM;
		return m_itsDeviateOnBottom;
	}
	if( is_deviate_on_top( nIndex, m_pdCache1, m_pdCache2 ) )
	{	// 顶背离
		if( pnCode )	*pnCode	=	ITSC_DEVIATEONTOP;
		return m_itsDeviateOnTop;
	}

	return	ITS_NOTHING;
}

bool CASI::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax )
{
	return AfxGetMinMaxInfo1( nStart, nEnd, pdMin, pdMax, this );
}

/***
	A = 今最高 - 昨收盘
	B = 今最低 - 昨最低
	C = 今最高 - 昨最低
	D = 昨收盘 - 昨开盘
	E = 今收盘 - 昨收盘
	F = 今收盘 - 昨开盘
	G = 昨收盘 - 昨开盘
	X = E + 1/(2F) + G
	K = A、B二者之间较大者
	比较A、B、C三者的大小
		若A大，则R = A+1/(2B)+1/(4D)
		若B大，则R = B+1/(2A)+1/(4D)
		若C大，则R = C+1/(4D)
	L = 3
	SI = 50·X·K/(R·L)
	ASI = N日SI之和
*/
bool CASI::calc( double * pValue, size_t nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );
	
	if( m_nDays > nIndex )
		return false;

	if( load_from_cache( nIndex, pValue ) )
		return true;

	double	dASI = 0;
	double	A, B, C, D, E, F, G;
	double	R, X, K, SI;
	int	nCount	=	0;
	for( int k=nIndex; k>=1; k-- )
	{
		KDATA	kd		=	m_pKData->at(k);
		KDATA	kdLast	=	m_pKData->at(k-1);
		A	=	fabs(((double)kd.HighestPrice) - kdLast.ClosePrice);
		B	=	fabs(((double)kd.LowestPrice) - kdLast.ClosePrice);
		C	=	fabs(((double)kd.HighestPrice) - kdLast.HighestPrice);
		D	=	fabs(((double)kdLast.ClosePrice) - kdLast.OpenPrice);
		E	=	((double)kd.ClosePrice) - kdLast.ClosePrice;
		F	=	((double)kd.ClosePrice) - kd.OpenPrice;
		G	=	((double)kdLast.ClosePrice) - kdLast.OpenPrice;

		if( fabs(A) < 1e-4 || fabs(B) < 1e-4 || fabs(D) < 1e-4 || fabs(F) < 1e-4 )
			continue;

		if( A >= B && A >= C )
			R	=	A + 1/(2*B) + 1/(4*D);
		else if( B >= A && B >= C )
			R	=	B + 1/(2*A) + 1/(4*D);
		else
			R	=	C + 1/(4*D);

		if( fabs(R) < 1e-4 )
			continue;

		X	=	E + 1/(2*F) + G;
		K	=	( A > B ? A : B );
		SI	=	X * K * 50 / (3*R);

		dASI	+=	SI;

		nCount	++;
		if( nCount == m_nDays )
		{
			if( pValue )	*pValue	=	dASI;
			store_to_cache( nIndex, pValue );
			return true;
		}
	}

	return false;
}
