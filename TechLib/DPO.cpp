#include "stdafx.h"
#include "DPO.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CDPO
CDPO::CDPO()
{
	SetDefaultParameters();
}

CDPO::CDPO(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CDPO::~CDPO()
{
	clear();
}

void CDPO::SetDefaultParameters()
{
	m_nDays = 10;
	m_nMADays = 6;
	m_itsGoldenFork = ITS_BUY;
	m_itsDeadFork = ITS_SELL;
}

void CDPO::AttachParameters(CDPO & src)
{
	m_nDays = src.m_nDays;
	m_nMADays = src.m_nMADays;
	m_itsGoldenFork = src.m_itsGoldenFork;
	m_itsDeadFork = src.m_itsDeadFork;
}

bool CDPO::IsValidParameters()
{
	return (VALID_DAYS(m_nDays) && VALID_DAYS(m_nMADays)
		&& VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork));
}

void CDPO::clear()
{
	TechnicalIndicator::clear();
}

int CDPO::signal(size_t nIndex, uint32_t * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	int	nMaxDays = 3 * m_nDays + m_nMADays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if (!intensity_prepare(nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.2, 0.7))
		return ITS_NOTHING;

	double	dDPO;
	if (!calc(&dDPO, nIndex, false))
		return ITS_NOTHING;

	int	nSignal = GetForkSignal(nIndex, m_itsGoldenFork, m_itsDeadFork, pnCode);
	if (dDPO < dLiminalLow && nSignal == m_itsGoldenFork)
	{	// 低位金叉
		if (pnCode)	*pnCode = ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if (dDPO > dLiminalHigh && nSignal == m_itsDeadFork)
	{	// 高位死叉
		if (pnCode)	*pnCode = ITSC_DEADFORK;
		return m_itsDeadFork;
	}

	return	ITS_NOTHING;
}

bool CDPO::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo2(nStart, nEnd, pdMin, pdMax, this);
}

/***
DPO = 今日收盘价 - N日前的（N+N）日平均收盘价
*/
bool CDPO::calc(double * pValue, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (3 * m_nDays - 2 > nIndex + 1)
		return false;

	if (load_from_cache(nIndex, pValue))
        return true;

	double	dCt = 0, dMA = 0;
	double	nCount = 0;
	for (int k = nIndex - m_nDays + 1; k >= 0; k--)
	{
		dMA += m_pKData->MaindataAt(k);

		nCount++;
		if (nCount == m_nDays + m_nDays)
		{
			dCt = m_pKData->MaindataAt(nIndex);
			dMA = dMA / (m_nDays + m_nDays);
			if (pValue)
				*pValue = (dCt - dMA);
			store_to_cache(nIndex, pValue);
            return true;
		}
	}

	return false;
}

/***
计算DPO及其移动平均值
*/
bool CDPO::calc(double * pValue, double * pMA, size_t nIndex, bool bUseLast)
{
	return calc_ma(pValue, pMA, nIndex, bUseLast, m_nMADays);
}
