#ifndef _TECH_MTM_H_
#define _TECH_MTM_H_
//	动量指标MTM
#include "TechLib.h"
#include "Technique.h"
//	简易波动指标EMV
class TECH_API  CMTM : public TechnicalIndicator
{
public:
	// Constructors
	CMTM( );
	CMTM( KdataContainer * pKData );
	virtual ~CMTM();

public:
	virtual	void clear( );

	// Attributes
	int		m_nDays;
	int		m_nMADays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	attach( CMTM & src );
	virtual	bool	IsValidParameters( );

	// Operations
    virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
    virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
    virtual	bool	calc( double * pValue, size_t nIndex, bool bUseLast );
    virtual	bool	calc( double * pValue, double *pMA, size_t nIndex, bool bUseLast );
};
#endif
