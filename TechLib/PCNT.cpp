#include "stdafx.h"
#include "PCNT.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CPCNT
CPCNT::CPCNT()
{
	SetDefaultParameters();
}

CPCNT::CPCNT(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CPCNT::~CPCNT()
{
	clear();
}

void CPCNT::SetDefaultParameters()
{
	m_nMADays = 6;
	m_itsGoldenFork = ITS_BUY;
	m_itsDeadFork = ITS_SELL;
}

void CPCNT::AttachParameters(CPCNT & src)
{
	m_nMADays = src.m_nMADays;
	m_itsGoldenFork = src.m_itsGoldenFork;
	m_itsDeadFork = src.m_itsDeadFork;
}

bool CPCNT::IsValidParameters()
{
	return (VALID_DAYS(m_nMADays) && VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork));
}

void CPCNT::clear()
{
	TechnicalIndicator::clear();
}

int CPCNT::signal(int nIndex, UINT * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	int	nMaxDays = m_nMADays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if (!intensity_prepare(nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.5, 0.5))
		return ITS_NOTHING;

	double	dPCNT;
	if (!calc(&dPCNT, nIndex, false))
		return ITS_NOTHING;

	int	nSignal = GetForkSignal(nIndex, m_itsGoldenFork, m_itsDeadFork, pnCode);
	if (dPCNT < dLiminalLow && nSignal == m_itsGoldenFork)
	{	// 低位金叉
		if (pnCode)	*pnCode = ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if (dPCNT > dLiminalHigh && nSignal == m_itsDeadFork)
	{	// 高位死叉
		if (pnCode)	*pnCode = ITSC_DEADFORK;
		return m_itsDeadFork;
	}

	return ITS_NOTHING;
}

bool CPCNT::min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo2(nStart, nEnd, pdMin, pdMax, this);
}

/***
今日收盘 - N日前收盘
PCNT = ———————————— × 100%
昨日收盘
*/
bool CPCNT::calc(double * pValue, int nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (load_from_cache(nIndex, pValue))
		return true;

	int	nDays = 1;	//	same as ROC with m_nDays==1
	if (nDays > nIndex)
		return false;

	double	dROC = 0;
	if (m_pKData->MaindataAt(nIndex - nDays) <= 0
		|| m_pKData->MaindataAt(nIndex) <= 0)
		return false;

	double	x = m_pKData->MaindataAt(nIndex);
	double	y = m_pKData->MaindataAt(nIndex - nDays);
	if (pValue)
		*pValue = (x - y) * 100 / y;
	store_to_cache(nIndex, pValue);
	return true;
}

/***
计算PCNT及其移动平均值
*/
bool CPCNT::calc(double * pValue, double * pMA, int nIndex, bool bUseLast)
{
	return calc_ma(pValue, pMA, nIndex, bUseLast, m_nMADays);
}
