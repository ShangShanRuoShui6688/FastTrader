#pragma once
#include "TechLib.h"
#include "Technique.h"
//	中间意愿指标CR
class TECH_API CCR : public TechnicalIndicator
{
public:
	// Constructors
	CCR( );
	CCR( KdataContainer * pKData );
	virtual ~CCR();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays;
	uint32_t		m_nMADaysA;
	uint32_t		m_nMADaysB;
	uint32_t		m_nMADaysC;
	uint32_t		m_nMADaysD;
	int		m_itsSold;
	int		m_itsBought;
	virtual	void	SetDefaultParameters( );
	void	attach( CCR & src );
	virtual	bool	IsValidParameters( );

	// Operations
    virtual	int		signal( size_t nIndex, size_t * pnCode = NULL );
    virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
    virtual	bool	calc( double * pValue, size_t nIndex, bool bUseLast );
    virtual	bool	calc( double * pValue, double * pA, double * pB, double * pC, double * pD, size_t nIndex, bool bUseLast );
};
