//#include "stdafx.h"
#include "NVI.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CNVI
CNVI::CNVI()
{
	SetDefaultParameters();
}

CNVI::CNVI(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CNVI::~CNVI()
{
	clear();
}

void CNVI::SetDefaultParameters()
{
	m_nMADays = 25;
	m_itsGoldenFork = ITS_BUY;
	m_itsDeadFork = ITS_SELL;
}

void CNVI::AttachParameters(CNVI & src)
{
	m_nMADays = src.m_nMADays;
	m_itsGoldenFork = src.m_itsGoldenFork;
	m_itsDeadFork = src.m_itsDeadFork;
}

bool CNVI::IsValidParameters()
{
	return (VALID_DAYS(m_nMADays) && VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork));
}

void CNVI::clear()
{
	TechnicalIndicator::clear();
}

int CNVI::signal(int nIndex, UINT * pnCode)
{
	prepare_cache(0, -1, false);
	// 金叉死叉
	return GetForkSignal(nIndex, m_itsGoldenFork, m_itsDeadFork, pnCode);
}

bool CNVI::min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo2(nStart, nEnd, pdMin, pdMax, this);
}

/***
NVI初值 = 100
如果今天成交量比昨日小 NVI = 前一日NVI + 100 * 涨跌幅 否则，NVI = 前一天NVI
*/
bool CNVI::calc(double * pValue, double *pMA, int nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	// Calculate
	if (m_nMADays > nIndex + 1)
		return false;

	if (load_from_cache(nIndex, pValue, pMA))
		return true;

	double	dValueNew = 0, dMANew = 0;
	if (bUseLast && pValue && pMA)
	{
		if (0 == nIndex)
			dValueNew = 100;
		else if (m_pKData->at(nIndex).Volume < m_pKData->at(nIndex - 1).Volume
			&& m_pKData->MaindataAt(nIndex - 1) > 1e-4 && m_pKData->MaindataAt(nIndex) > 1e-4)
			dValueNew = (*pValue) * m_pKData->MaindataAt(nIndex) / m_pKData->MaindataAt(nIndex - 1);
		else
			dValueNew = *pValue;
		dMANew = (*pMA) * (m_nMADays - 1) / (m_nMADays + 1) + dValueNew * 2 / (m_nMADays + 1);
		store_to_cache(nIndex, &dValueNew, &dMANew);
	}
	else
	{
		for (int k = 0; k <= nIndex; k++)
		{
			if (0 == k)
				dValueNew = 100;
			else if (m_pKData->at(k).Volume < m_pKData->at(k - 1).Volume
				&& m_pKData->MaindataAt(k - 1) > 1e-4 && m_pKData->MaindataAt(k) > 1e-4)
				dValueNew = dValueNew * m_pKData->MaindataAt(k) / m_pKData->MaindataAt(k - 1);

			if (0 == k)
				dMANew = dValueNew;
			else
				dMANew = dMANew * (m_nMADays - 1) / (m_nMADays + 1) + dValueNew * 2 / (m_nMADays + 1);

			store_to_cache(k, &dValueNew, &dMANew);
		}
	}

	if (pValue)	*pValue = dValueNew;
	if (pMA)		*pMA = dMANew;
	return true;
}
