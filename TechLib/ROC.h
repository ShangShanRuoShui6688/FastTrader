#ifndef _TECH_ROC_H_
#define _TECH_ROC_H_
//	变动率指标ROC
#include "TechLib.h"
#include "Technique.h"
//	简易波动指标EMV
class TECH_API  CROC : public TechnicalIndicator
{
public:
	// Constructors
	CROC( );
	CROC( KdataContainer * pKData );
	virtual ~CROC();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays;
	uint32_t		m_nMADays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	attach( CROC & src );
	virtual	bool	IsValidParameters( );

	// Operations
    virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
    virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
    virtual	bool	calc( double * pValue, size_t nIndex, bool bUseLast );
    virtual	bool	calc( double * pValue, double * pMA, size_t nIndex, bool bUseLast );
};
#endif
