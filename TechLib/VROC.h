#pragma once
#include "Technique.h"


//	成交量变动率指标VROC
class TECH_API CVROC : public TechnicalIndicator
{
public:
	// Constructors
	CVROC( );
	CVROC( KdataContainer * pKData );
	virtual ~CVROC();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays;
	uint32_t		m_nMADays;
	int		m_itsDeviateOnBottom;
	int		m_itsDeviateOnTop;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CVROC & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal(size_t nIndex, uint32_t * pnCode = NULL);
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pValue, size_t nIndex, bool bUseLast);
	virtual	bool	calc(double * pValue, double * pMA, size_t nIndex, bool bUseLast);
};

