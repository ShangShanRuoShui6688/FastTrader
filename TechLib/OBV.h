#ifndef _TECH_OBV_H_
#define _TECH_OBV_H_
//	能量线OBV
#include "TechLib.h"
#include "Technique.h"
//	简易波动指标EMV
class TECH_API  COBV : public TechnicalIndicator
{
public:
	// Constructors
	COBV( );
	COBV( KdataContainer * pKData );
	virtual ~COBV();

public:
	virtual	void clear( );

	// Attributes
	int		m_itsDeviateOnBottom;
	int		m_itsDeviateOnTop;
	virtual	void	SetDefaultParameters( );
	void	attach( COBV & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( int nIndex, UINT * pnCode = NULL );
	virtual	bool	min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax );
	virtual	bool	calc( double * pdOBV, int nIndex, bool bUseLast );
};
#endif