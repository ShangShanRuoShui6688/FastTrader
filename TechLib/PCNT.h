#pragma once
#include "Technique.h"

//	���ȱ�PCNT
class  CPCNT : public TechnicalIndicator
{
public:
	// Constructors
	CPCNT( );
	CPCNT( KdataContainer * pKData );
	virtual ~CPCNT();

public:
	virtual	void clear( );

	// Attributes
	int		m_nMADays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CPCNT & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( int nIndex, UINT * pnCode = NULL );
	virtual	bool	min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax );
	virtual	bool	calc( double * pValue, int nIndex, bool bUseLast );
	virtual	bool	calc( double * pValue, double * pMA, int nIndex, bool bUseLast );
};


