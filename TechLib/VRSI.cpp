//#include "stdafx.h"
#include "VRSI.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CVRSI
CVRSI::CVRSI()
{
	SetDefaultParameters();
}

CVRSI::CVRSI(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CVRSI::~CVRSI()
{
	clear();
}

void CVRSI::SetDefaultParameters()
{
	m_nDays = 10;
	m_itsDeviateOnBottom = ITS_BUYINTENSE;
	m_itsDeviateOnTop = ITS_SELLINTENSE;
	m_itsSold = ITS_BUY;
	m_itsBought = ITS_SELL;
}

void CVRSI::AttachParameters(CVRSI & src)
{
	m_nDays = src.m_nDays;
	m_itsDeviateOnBottom = src.m_itsDeviateOnBottom;
	m_itsDeviateOnTop = src.m_itsDeviateOnTop;
	m_itsSold = src.m_itsSold;
	m_itsBought = src.m_itsBought;
}

bool CVRSI::IsValidParameters()
{
	return (VALID_DAYS(m_nDays)
		&& VALID_ITS(m_itsDeviateOnBottom) && VALID_ITS(m_itsDeviateOnTop)
		&& VALID_ITS(m_itsSold) && VALID_ITS(m_itsBought));
}

void CVRSI::clear()
{
	TechnicalIndicator::clear();
}

int CVRSI::signal(size_t nIndex, uint32_t * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	int	nMaxDays = m_nDays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if (!intensity_prepare(nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh))
		return ITS_NOTHING;

	double	dNow;
	if (!calc(&dNow, nIndex, false))
		return ITS_NOTHING;

	if (is_deviate_on_bottom(nIndex, m_pdCache1, m_pdCache2))
	{	// 底背离
		if (pnCode)	*pnCode = ITSC_DEVIATEONBOTTOM;
		return m_itsDeviateOnBottom;
	}
	if (is_deviate_on_top(nIndex, m_pdCache1, m_pdCache2))
	{	// 顶背离
		if (pnCode)	*pnCode = ITSC_DEVIATEONTOP;
		return m_itsDeviateOnTop;
	}
	if (dNow < dLiminalLow)
	{	// 超卖
		if (pnCode)	*pnCode = ITSC_OVERSOLD;
		return m_itsSold;
	}
	if (dNow > dLiminalHigh)
	{	// 超买
		if (pnCode)	*pnCode = ITSC_OVERBOUGHT;
		return m_itsBought;
	}

	return	ITS_NOTHING;
}

bool CVRSI::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax)
{
	if (pdMin)	*pdMin = 0;
	if (pdMax)	*pdMax = 100;
	return true;
}

/***
VP = N日内成交量增加日的平均成交量
VQ = N日内成交量减少日的平均成交量
VRSI = 100 * VP / (VP+VQ)
*/
bool CVRSI::calc(double * pValue, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (m_nDays > nIndex)
		return false;

	if (load_from_cache(nIndex, pValue))
		return true;

	double	dVP = 0, dVQ = 0, dResult = 0;
	int	nCount = 0, p = 0, q = 0;
	for (int k = nIndex; k >= 1; k--)
	{
		if (m_pKData->MaindataAt(k) >= m_pKData->MaindataAt(k - 1))
		{
			dVP += m_pKData->at(k).Volume;
			p++;
		}
		else
		{
			dVQ += m_pKData->at(k).Volume;
			q++;
		}

		nCount++;
		if (nCount == m_nDays)
		{
			if (p > 0)	dVP = dVP / p;
			if (q > 0)	dVQ = dVQ / q;
			if (dVQ < 1e-4)
				dResult = 100;
			else
				dResult = 100 - 100. / (1 + dVP / dVQ);
			if (pValue)	*pValue = dResult;
			store_to_cache(nIndex, pValue);
			return true;
		}
	}

	return false;
}
