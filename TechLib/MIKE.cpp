#include "MIKE.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
//////////////////////////////////////////////////////////////////////
//	CMIKE
CMIKE::CMIKE( )
{
	SetDefaultParameters( );
}

CMIKE::CMIKE( KdataContainer * pKData )
: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CMIKE::~CMIKE()
{
	clear( );
}

void CMIKE::SetDefaultParameters( )
{
	m_nDays	=	12;
}

void CMIKE::attach( CMIKE & src )
{
	m_nDays	=	src.m_nDays;
}

bool CMIKE::IsValidParameters( )
{
	return VALID_DAYS( m_nDays );
}

void CMIKE::clear( )
{
	TechnicalIndicator::clear( );
}

/***
H:最高价	L:最低价	C:收盘价
TP ＝ (H＋L＋C)÷3
第一条窄通道的上下限计算如下：
弱阻力WR＝TP＋(TP－L)		弱支撑WS＝TP－(H－TP)
第二条通道的上下限计算如下：
中阻力MR＝TP＋(H－L)		中支撑MS＝TP－(H－L)
第三条阔通道的上下限计算如下：
强阻力SR＝H＋(H－L)			强支撑SS＝L－(H－L)
*/
bool CMIKE::CalculateMIKE(	double *pWR, double *pMR, double *pSR,
						  double *pWS, double *pMS, double *pSS, size_t nIndex )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( m_nDays * 2 > nIndex + 1 )
		return false;

	double	TP	=	(m_pKData->at(nIndex).HighestPrice + m_pKData->at(nIndex).LowestPrice + m_pKData->at(nIndex).ClosePrice)/3.0;
	double	minN = 0, min2N = 0, maxN = 0, max2N = 0;
	for( int k=nIndex; k>=0; k -- )
	{
		KDATA	kd	=	m_pKData->at(k);
		if( nIndex-k < m_nDays )
		{
			if( nIndex == k )	{	minN = kd.LowestPrice;	maxN = kd.HighestPrice;	}
			if( kd.LowestPrice < minN )		minN = kd.LowestPrice;
			if( kd.HighestPrice > maxN )	maxN = kd.HighestPrice;
		}
		if( nIndex-k < m_nDays*2 )
		{
			if( nIndex == k )	{	min2N = kd.LowestPrice;	max2N = kd.HighestPrice;	}
			if( kd.LowestPrice < min2N )	min2N = kd.LowestPrice;
			if( kd.HighestPrice > max2N )	max2N = kd.HighestPrice;
		}
		else
		{
			break;
		}
	}
	if( pWR )	*pWR	=	( TP + (TP - minN) ) ;
	if( pMR )	*pMR	=	( TP + (maxN - minN) ) ;
	if( pSR )	*pSR	=	( TP + (max2N - minN) ) ;
	if( pWS )	*pWS	=	( TP - (maxN - TP) ) ;
	if( pMS )	*pMS	=	( TP - (maxN - minN) ) ;
	if( pSS )	*pSS	=	( TP - (maxN - min2N) ) ;
	return true;
}
