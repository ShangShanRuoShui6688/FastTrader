#pragma once
#include "TechLib.h"
#include "Technique.h"


/////////////////////////////////////////////////////////////////////////
//	其他类
//	范围膨胀指数REI
class  CREI : public TechnicalIndicator
{
public:
	// Constructors
	CREI( );
	CREI( KdataContainer * pKData );
	virtual ~CREI();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays;
	int		m_itsLong;
	int		m_itsShort;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CREI & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal(size_t nIndex, uint32_t * pnCode = NULL);
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pValue, size_t nIndex, bool bUseLast);
};


