#include "BIAS.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"

//////////////////////////////////////////////////////////////////////
//	CBIAS
CBIAS::CBIAS( )
{
	SetDefaultParameters( );
}

CBIAS::CBIAS( KdataContainer * pKData )
: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CBIAS::~CBIAS()
{
	clear( );
}

void CBIAS::SetDefaultParameters( )
{
	m_nDays		=	10;
	m_itsSold	=	ITS_BUY;
	m_itsBought	=	ITS_SELL;
}

void CBIAS::attach( CBIAS & src )
{
	m_nDays		=	src.m_nDays;
	m_itsSold	=	src.m_itsSold;
	m_itsBought	=	src.m_itsBought;
}

bool CBIAS::IsValidParameters( )
{
	return ( VALID_DAYS(m_nDays) && VALID_ITS(m_itsSold) && VALID_ITS(m_itsBought) );
}

void CBIAS::clear( )
{
	TechnicalIndicator::clear( );
}

int CBIAS::signal(size_t nIndex, uint32_t * pnCode)
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	double	dBIAS;
	if( !calc( &dBIAS, nIndex, false ) )
		return ITS_NOTHING;

	if( dBIAS < -10 )
	{	// 超卖
		if( pnCode )	*pnCode	=	ITSC_OVERSOLD;
		return m_itsSold;
	}
	if( dBIAS > 5 )
	{	// 超买
		if( pnCode )	*pnCode	=	ITSC_OVERBOUGHT;
		return m_itsBought;
	}

	return	ITS_NOTHING;
}

bool CBIAS::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo1( nStart, nEnd, pdMin, pdMax, this );
}

/***
当日收盘价-N日移动平均值
N日乖离率 = —————————————— ×100%
N日移动平均值
*/
bool CBIAS::calc(double * pValue, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( m_nDays > nIndex+1 )
		return false;
	if( load_from_cache( nIndex, pValue ) )
		return true;

	double	dMA = 0;
	int	nCount	=	0;
	for (size_t k = nIndex; k >= 0; k--)
	{
		dMA	+=	m_pKData->MaindataAt(k);

		nCount	++;
		if( nCount == m_nDays )
		{
			double	dResult	=	0;
			dMA	=	dMA / m_nDays;
			if( dMA > 1e-4 )
				dResult	=	100. * ( m_pKData->MaindataAt(nIndex) - dMA ) / dMA;
			if( pValue )	*pValue	=	dResult;
			store_to_cache( nIndex, pValue );
			return true;
		}
	}
	return false;
}
