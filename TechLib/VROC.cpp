#include "stdafx.h"
#include "VROC.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CVROC
CVROC::CVROC()
{
	SetDefaultParameters();
}

CVROC::CVROC(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CVROC::~CVROC()
{
	clear();
}

void CVROC::SetDefaultParameters()
{
	m_nDays = 10;
	m_nMADays = 10;
	m_itsDeviateOnBottom = ITS_BUY;
	m_itsDeviateOnTop = ITS_SELL;
}

void CVROC::AttachParameters(CVROC & src)
{
	m_nDays = src.m_nDays;
	m_nMADays = src.m_nMADays;
	m_itsDeviateOnBottom = src.m_itsDeviateOnBottom;
	m_itsDeviateOnTop = src.m_itsDeviateOnTop;
}

bool CVROC::IsValidParameters()
{
	return (VALID_DAYS(m_nDays) && VALID_DAYS(m_nMADays)
		&& VALID_ITS(m_itsDeviateOnBottom) && VALID_ITS(m_itsDeviateOnTop));
}

void CVROC::clear()
{
	TechnicalIndicator::clear();
}

int CVROC::signal(size_t nIndex, uint32_t * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	int	nMaxDays = m_nDays + m_nMADays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if (!intensity_prepare(nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh))
		return ITS_NOTHING;

	if (is_deviate_on_bottom(nIndex, m_pdCache1, m_pdCache2))
	{	// 底背离
		if (pnCode)	*pnCode = ITSC_DEVIATEONBOTTOM;
		return m_itsDeviateOnBottom;
	}
	if (is_deviate_on_top(nIndex, m_pdCache1, m_pdCache2))
	{	// 顶背离
		if (pnCode)	*pnCode = ITSC_DEVIATEONTOP;
		return m_itsDeviateOnTop;
	}

	return	ITS_NOTHING;
}

bool CVROC::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo2(nStart, nEnd, pdMin, pdMax, this);
}

/***
今日成交量-n日前成交量
VROC =   ---------------------- * 100
今日成交量
*/
bool CVROC::calc(double * pValue, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (m_nDays > nIndex)
		return false;
	if (load_from_cache(nIndex, pValue))
		return true;

	if (m_pKData->at(nIndex - m_nDays).Volume <= 0
		|| m_pKData->at(nIndex).Volume <= 0)
		return false;

	double	x = m_pKData->at(nIndex).Volume;
	double	y = m_pKData->at(nIndex - m_nDays).Volume;
	if (pValue)
		*pValue = (x - y) * 100 / y;
	store_to_cache(nIndex, pValue);
	return true;
}

bool CVROC::calc(double * pValue, double * pMA, size_t nIndex, bool bUseLast)
{
	return calc_ma(pValue, pMA, nIndex, bUseLast, m_nMADays);
}

