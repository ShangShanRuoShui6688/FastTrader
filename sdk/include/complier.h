#ifndef _COMPLIER_H_
#define _COMPLIER_H_


#if defined(_MSC_VER)
#define __x86_x64 1
#endif

#if defined(_MSC_VER)
#	ifdef LIB_EXPORTS
#    define LIB_API __declspec(dllexport)
#  else
#    define LIB_API __declspec(dllimport)
#	endif
# else
#	 define LIB_API
#endif

#include <time.h>
#include <stdlib.h>
#include <memory.h>


#if defined(_WIN32)||(WIN32)
    #ifndef WIN32_LEAN_AND_MEAN
        #define WIN32_LEAN_AND_MEAN
    #endif

    #include<Windows.h>
    #include<winsock2.h>
    #include <nb30.h>
    #define EXTERN_C_API  extern "C" __declspec(dllexport)
    #define DLL_EXT ".dll"
        #define APP_EXT ".exe"
#else
#include <sys/time.h>
#ifdef  LINUX
#include <linux/hdreg.h>
inline unsigned long GetTickCount()
{
	struct timespec ts;

	clock_gettime(CLOCK_MONOTONIC, &ts);

	return (ts.tv_sec * 1000 + ts.tv_nsec / 1000000);
}
#else

inline unsigned long GetTickCount()
{
	unsigned long currentTime=0;
	struct timeval current;
	gettimeofday(&current, NULL);
	currentTime = current.tv_sec * 1000 + current.tv_usec/1000;
        return currentTime;
}
#endif //  LINUX

#define CALLBACK
#define COLORREF long
#define QT_WIN_CALLBACK CALLBACK

#define EXTERN_C_API  extern "C"
#define DLL_EXT ".so"
#define APP_EXT ""
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/utsname.h>
#include <errno.h>
#include <netdb.h>
#include <arpa/inet.h>
#define __cdecl
#define __stdcall


typedef long LRESULT;

typedef unsigned int       DWORD;
typedef  unsigned char      BYTE;
typedef unsigned short      WORD;
typedef unsigned int      UINT;
typedef unsigned long     LPARAM;
typedef unsigned int        WPARAM;
#define Sleep(n)            sleep((n)/1000);
#define RGB(r,g,b)          ((long)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16)))
#ifndef MAXBYTE
#define MAXBYTE  128

#define popen   _popen
#define pclose  _pclose
#define  stricmp strcasecmp
#define  strnicmp strncasecmp

#endif
#endif



#endif
