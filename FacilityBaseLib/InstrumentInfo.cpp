#include "InstrumentInfo.h"
//#include "DateTime.h"
#include <cassert>
#include <cmath>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
using namespace std;
#include "InstrumentData.h"
#include <boost/make_shared.hpp>



bool InstrumentInfo::is_valid()
{
	return (strlen(szCode)>0) /*&& (strlen(ExchangeID)>0)*/;
}

const char* InstrumentInfo::get_name()
{
	return szName;
}

const char* InstrumentInfo::get_id()
{
	return szCode;
}

InstrumentInfo::InstrumentInfo( const InstrumentInfo& src )
{
	clear();
	*this=src;
}

InstrumentInfo::InstrumentInfo()
{
	clear();
}


InstrumentInfo& InstrumentInfo::operator=(const InstrumentInfo& si)
{
	strncpy(szCode,si.szCode,sizeof(szCode));
	strncpy(szName,si.szName,sizeof(szName));
	strncpy(szExchange, si.szExchange, sizeof(szExchange));

	m_type = si.m_type;

	strncpy(m_szNameEnu, si.m_szNameEnu, sizeof(m_szNameEnu));
	strncpy(m_szShortName, si.m_szShortName, sizeof(m_szShortName));

	m_szDomain=si.m_szDomain;

	m_kdata = si.m_kdata;
	m_basedata = si.m_basedata;
	m_minute = si.m_minute;
	m_reportLatest = si.m_reportLatest;
	m_datetech = si.m_datetech;


	strncpy(ExchangeInstID,si.ExchangeInstID,sizeof(ExchangeInstID));
	strncpy(ProductID,si.ProductID,sizeof(ProductID));
	ProductClass=si.ProductClass;
	DeliveryYear=si.DeliveryYear;
	DeliveryMonth=si.DeliveryMonth;
	MaxMarketOrderVolume=si.MaxMarketOrderVolume;
	MinMarketOrderVolume=si.MinMarketOrderVolume;
	MinLimitOrderVolume=si.MinLimitOrderVolume;
	MaxLimitOrderVolume=si.MaxLimitOrderVolume;
	VolumeMultiple=si.VolumeMultiple;
	PriceTick=si.PriceTick;
	strncpy(CreateDate,si.CreateDate,sizeof(CreateDate));
	strncpy(OpenDate,si.OpenDate,sizeof(OpenDate));
	strncpy(ExpireDate,si.ExpireDate,sizeof(ExpireDate));
	strncpy(StartDelivDate,si.StartDelivDate,sizeof(StartDelivDate));
	strncpy(EndDelivDate,si.EndDelivDate,sizeof(EndDelivDate));
	InstLifePhase=si.InstLifePhase;
	IsTrading=si.IsTrading;
	PositionType=si.PositionType;
	LongMarginRatio=si.LongMarginRatio;
	ShortMarginRatio=si.ShortMarginRatio;
	strncpy(TradingDay,si.TradingDay,sizeof(TradingDay));
	PreSettlementPrice=si.PreSettlementPrice;
	SettlementPrice=si.SettlementPrice;
	PreClosePrice=si.PreClosePrice;
	PreOpenInterest=si.PreOpenInterest;
	OpenPrice=si.OpenPrice;
	HighestPrice=si.HighestPrice;
	LowestPrice=si.LowestPrice;
	OpenInterest=si.OpenInterest;
	ClosePrice=si.ClosePrice;
	//PreDelta=si.PreDelta;
	//CurrDelta=si.CurrDelta;
	AveragePrice=si.AveragePrice;
	LastPrice=si.LastPrice;
	Volume=si.Volume;
	Turnover=si.Turnover;
	UpperLimitPrice=si.UpperLimitPrice;
	LowerLimitPrice=si.LowerLimitPrice;
	UpdateTime=si.UpdateTime;
	memcpy(BidPrice,si.BidPrice,sizeof(si.BidPrice));
	memcpy(BidVolume,si.BidVolume,sizeof(si.BidVolume));
	memcpy(AskPrice,si.AskPrice,sizeof(si.AskVolume));
	memcpy(AskVolume,si.AskVolume,sizeof(si.AskVolume));
	UpdateMillisec=si.UpdateMillisec;

	return *this;
}

bool InstrumentInfo::is_equal( const char* exID,const char* szID )
{
	if (szID && 0==strcmp(szCode,szID))
	{
		if (NULL == exID || 0==strlen(exID))
		{
			return true;
		}
		else if (0==strcmp(exID,szExchange))
		{
			return true;
		}
		else
			return false;
	}
	return false;
}

bool InstrumentInfo::is_equal( DWORD dwMarket,const char* szID )
{
	return is_equal((char*)dwMarket,szID);
}

const char* InstrumentInfo::GetExchangeID()
{
	return szExchange;
}

void InstrumentInfo::clear()
{
	m_minute = boost::make_shared<MinuteContainer>();
	memset(szCode,0,sizeof(szCode));
	memset(szName,0,sizeof(szName));
	memset(&m_reportLatest,0,sizeof(m_reportLatest));
	memset(&m_szShortName,0,sizeof(m_szShortName));
	memset(ExchangeInstID, 0, sizeof(ExchangeInstID));
	memset(TradingDay, 0, sizeof(TradingDay));
	//PreDelta=0;
	//CurrDelta=0;
	PreClosePrice=0;
	PreSettlementPrice=0;

	LastPrice=0;
	OpenPrice=0;
	HighestPrice=0;
	LowestPrice=0;
	ClosePrice=0;
	Volume=0;
	Turnover=0;
	UpperLimitPrice=0;
	AveragePrice=0;
	PreSettlementPrice=0;
	SettlementPrice=0;
	memset(BidPrice,0,sizeof(BidPrice));
	memset(BidVolume,0,sizeof(BidVolume));
	memset(AskPrice,0,sizeof(AskPrice));
	memset(AskVolume,0,sizeof(AskVolume));
	UpdateTime = 0;
	UpdateMillisec=0;
	UpperLimitPrice=0;
	LowerLimitPrice=0;

	memset(m_szShortName,0,sizeof(m_szShortName));
	memset(m_szNameEnu,0,sizeof(m_szNameEnu));

}

bool InstrumentInfo::set_id( const char* exID,const char* szID )
{
	if (NULL==szID || strlen(szID)<=0)
	{
		clear();
		return false;
	}
	if (is_equal(exID,szID))
	{
		return is_valid();
	}
	clear();
	memset(szExchange, 0, sizeof(szExchange));
	memset(szCode,0,sizeof(szCode));
#ifdef _MSC_VER
	if (exID)
	{
		strncpy_s(szExchange, exID, sizeof(szExchange));
	}
	if (szID)
	{
		strncpy_s(szCode, szID, sizeof(szCode));
	}
	
#else
	if (exID)
	{
        strcpy(szExchange, exID);
	}
	
    strncpy(szCode,szID,sizeof(szCode));
#endif

	return is_valid();
}
void InstrumentInfo::set_name( const char* szName )
{
	memset(this->szName,0,sizeof(this->szName));
	if (NULL==szName)
	{
		return ;
	}
#ifdef _MSC_VER
	strncpy_s(this->szName,szName,min(sizeof(this->szName)-1,strlen(szName)));
#else
    strncpy(this->szName,szName,min(sizeof(szName)-1,strlen(szName)));
#endif
}

void InstrumentInfo::SetExchangeID( const char* exID )
{
	if (NULL==exID)
	{
		return ;
	}
	memset(szExchange, 0, sizeof(szExchange));
#ifdef _MSC_VER
	strncpy_s(szExchange, exID, min(sizeof(szExchange) - 1, strlen(exID)));
#else
	strncpy(szExchange, exID, min(sizeof(szExchange) - 1, strlen(exID)));
#endif
}



bool InstrumentInfo::StatTechIndex( DWORD dwDate )
{
	//assert( 0 != dwDate );

	if( m_kdata.size() == 0 )
		return false;

	int	nIndex	=	-1;
	if( -1 == dwDate )
		nIndex	=	m_kdata.size() - 1;
// 	else
// 		nIndex	=	m_kdata.GetIndexByDate( dwDate );

	if( -1 == nIndex )
	{
		m_datetech	=	0;
		LastPrice		=	0;
		OpenPrice		=	0;
		HighestPrice		=	0;
		LowestPrice		=	0;
		ClosePrice	=	0;
		Turnover	=	0;
		Volume	=	0;
		return false;
	}
	else
	{
		KDATA	& kd	=	m_kdata.at( nIndex );
		m_datetech	=	kd.TradingDate;
		if( nIndex > 0 )
			LastPrice	=	m_kdata.at(nIndex-1).ClosePrice;
		else
			LastPrice	=	kd.OpenPrice;
		OpenPrice		=	kd.OpenPrice;
		HighestPrice	=	kd.HighestPrice;
		LowestPrice		=	kd.LowestPrice;
		ClosePrice	=	kd.ClosePrice;
		Turnover	=	kd.Turnover;
		Volume	=	kd.Volume;
		
		return true;
	}
}

bool InstrumentInfo::StatBaseIndex( DWORD dwDate )
{
	//assert( 0 != dwDate );
	if( m_basedata.size() == 0 )
		return false;

	int	nIndex	=	m_basedata.size()-1;
	for( int i=m_basedata.size()-1; i>=0; i-- )
	{
		if( m_basedata.at(i)->m_date <= dwDate )
		{
			nIndex	=	i;
			break;
		}
	}
	/*
	BASEDATA	& block		=	m_basedata.at(nIndex);

	
	strncpy( m_szDomain, block.m_szDomain, min(sizeof(m_szDomain),sizeof(block.m_szDomain)) );
	strncpy( m_szProvince, block.m_szProvince, min(sizeof(m_szProvince),sizeof(block.m_szProvince)) );
	m_datebase				=	block.m_date;
	m_reporttype			=	block.m_reporttype;
	if( block.m_fErate_dollar > 1e-6 )
		m_fErate_dollar			=	block.m_fErate_dollar;
	if( block.m_fErate_hkdollar > 1e-6 )
		m_fErate_hkdollar		=	block.m_fErate_hkdollar;

	m_fRatio_liquidity		=	block.m_fRatio_liquidity;
	m_fRatio_quick			=	block.m_fRatio_quick;
	m_fVelocity_receivables	=	block.m_fVelocity_receivables;

	m_fVelocity_merchandise	=	block.m_fVelocity_merchandise;
	m_fMain_income			=	block.m_fMain_income;
	m_fCash_ps				=	block.m_fCash_ps;
	
	m_fProfit_margin		=	block.m_fProfit_margin;
	m_fNetasset_yield		=	block.m_fNetasset_yield;

	m_datebegin				=	block.m_datebegin;
	m_fShare_count_total	=	block.m_fShare_count_total;
	m_fShare_count_a		=	block.m_fShare_count_a;
	m_fShare_count_b		=	block.m_fShare_count_b;
	m_fShare_count_h		=	block.m_fShare_count_h;
	m_fShare_count_national	=	block.m_fShare_count_national;
	m_fShare_count_corp		=	block.m_fShare_count_corp;
	m_fProfit_psud			=	block.m_fProfit_psud;
	m_fAsset				=	block.m_fAsset;
	m_fRatio_holderright	=	block.m_fRatio_holderright;
	m_fRatio_longdebt		=	block.m_fRatio_longdebt;
	m_fRatio_debt			=	block.m_fRatio_debt;

	m_fNetasset_ps			=	block.m_fNetasset_ps;
	m_fNetasset_ps_regulate	=	block.m_fNetasset_ps_regulate;
	m_fEps					=	block.m_fEps;
	m_fEps_deduct			=	block.m_fEps_deduct;
	m_fNet_profit			=	block.m_fNet_profit;
	m_fMain_profit			=	block.m_fMain_profit;
	m_fTotal_profit			=	block.m_fTotal_profit;

	m_fProfit_inc			=	block.m_fProfit_inc;
	m_fIncome_inc			=	block.m_fIncome_inc;
	m_fAsset_inc			=	block.m_fAsset_inc;
	*/
// 	LONG		m_fYield_average;		// 
// 	LONG		m_fYield_stddev;		// 
// 	LONG		m_fBeite;				// 
	/*
	BASEDATA	& blkLatest	=	m_basedata.ElementAt(m_basedata.GetSize()-1);
	m_fYield_average		=	blkLatest.m_fYield_average;
	m_fYield_stddev			=	blkLatest.m_fYield_stddev;
	m_fBeite				=	blkLatest.m_fBeite;
	*/
/*** stat
	m_fProfit_inc			=	0;
	m_fIncome_inc			=	0;
	LONG	nProfitCount = 0;
	LONG	nIncomeCount = 0;
	float	fProfitLast = 0, fProfitSum = 0;
	float	fIncomeLast = 0, fIncomeSum = 0;
	for( int i=0; i<m_basedata.GetSize(); i++ )
	{
		BASEDATA	& base	=	m_basedata.ElementAt(i);
		if( fabs(fProfitLast) > 1e-4 )
		{
			nProfitCount	++;
			fProfitSum	+=	(float) ( (base.m_fNet_profit)/nProfitLast - 1 );
		}
		if( fabs(fIncomeLast) > 1e-4 )
		{
			nIncomeCount	++;
			fIncomeSum	+=	(float)( (base.m_fMain_income)/nIncomeLast - 1 );
		}

		fProfitLast	=	base.m_fNet_profit;
		fIncomeLast	=	base.m_fMain_income;
	}
	if( nProfitCount > 0 )
		m_fProfit_inc	=	(float)(((double)fProfitSum)/nProfitCount);
	if( nIncomeCount > 0 )
		m_fIncome_inc	=	(float)(((double)fIncomeSum)/nIncomeCount);
*/
	return true;
}


int InstrumentInfo::GetAccuracy()
{
	//取出小数部份;
	
	double intPriceTick;
	double dPriceTick=modf(PriceTick,&intPriceTick);
	if (dPriceTick<=0)
	{
		return 0;
	}
	int accuracy=0;
	while(dPriceTick<1.0)
	{
		dPriceTick*=10;
		accuracy++;
	}
	return accuracy;
}

void InstrumentInfo::SetShortName( const char* szShortName )
{
	std::size_t len = strlen(szShortName);
	if ( len <= 0)
	{
		string sName(szName);
		string sTemp = AfxMakeSpellCode(sName, 0x0);
		strncpy(m_szShortName, sTemp.c_str(), std::min<size_t>(sizeof(m_szShortName) - 1, sTemp.length()));
	}
	else
	{
		strncpy(m_szShortName, szShortName, std::min<size_t>(sizeof(m_szShortName) - 1, len));
	}
}

const char* InstrumentInfo::GetShortName()
{
	if (strlen(m_szShortName)<=0)
	{
		string sName(szName);
		string sTemp=AfxMakeSpellCode(sName,0x0);
		strncpy(m_szShortName,sTemp.c_str(),min(sizeof(m_szShortName)-1,sTemp.length()));
	}
	return m_szShortName;
}

void InstrumentInfo::SetDomain(const std::string & szDomain)
{
	m_szDomain = szDomain;
}

std::string InstrumentInfo::GetDomain()
{
	return m_szDomain;
}

bool InstrumentInfo::GetSellBuyRatio( double *pdRatio, double *pdDiff )
{
	double	dBuyVolume	= BidVolume[0];
	dBuyVolume	+=	BidVolume[1];
	dBuyVolume	+=	BidVolume[2];
	dBuyVolume	+=	BidVolume[3];
	double	dSellVolume	= AskVolume[0];
	dSellVolume	+=	AskVolume[1];
	dSellVolume	+=	AskVolume[2];
	dSellVolume	+=	AskVolume[3];

	double	dRatio	=	0;
	//dRatio	=	(dBuyVolume-dSellVolume)/(dBuyVolume+dSellVolume);
	if( dBuyVolume + dSellVolume > 1e-4 )
		dRatio	=	200*dBuyVolume/(dBuyVolume+dSellVolume)-100;
	double	dDiff		=	dBuyVolume - dSellVolume;
	if( pdRatio )
		*pdRatio	=	dRatio;
	if( pdDiff )
		*pdDiff		=	dDiff;
	return true;
}

bool InstrumentInfo::GetRS( double * pValue, DWORD dateCur, int nDays )
{
	return m_kdata.GetRS(pValue,dateCur,nDays);
}

bool InstrumentInfo::GetRatioVolume( double * pValue, DWORD dateCur, int nDays )
{
	return m_kdata.GetRatioVolume(pValue,dateCur,nDays);
}

bool InstrumentInfo::GetAverage( double* pValue )
{
	if (Volume>1e-4)
	{
		int nCount=0;
		double average=((double)(Turnover))/Volume;
		while (average<LowestPrice && nCount<10)
		{
			average*=10;
			nCount++;
		}
		while (average>HighestPrice&& nCount<20)
		{
			average/=10;
			nCount++;
		}
		if (average<LowestPrice)
		{
			average=(OpenPrice+HighestPrice+ClosePrice)/4;
		}
		if (pValue)
		{
			*pValue=average;
		}
		return true;
	}
	return false;
}

bool InstrumentInfo::GetDiff( double *pValue,DWORD dateCur,int nDays )
{

	if (1==nDays && m_datetech==dateCur)
	{
		if (LastPrice<=1e-4 || ClosePrice <1e-4)
		{
			return false;
		}
		if (pValue)
		{
			*pValue=ClosePrice-PreSettlementPrice;
		}
		return true;
	}
	return m_kdata.GetDiff(pValue,dateCur,nDays);
}

double InstrumentInfo::GetDiff( int nDays/*=1*/ )
{
	double pValue=0.0f;
	bool bGet= GetDiff(&pValue,m_datetech,nDays);
	return bGet?pValue:0.0f;
}

double InstrumentInfo::GetDiffPercent( int nDays/*=1*/ )
{
	double pValue=0.0f;
	bool bGet= GetDiffPercent(&pValue,m_datetech,nDays);
	return bGet?pValue:0.0f;
}

bool InstrumentInfo::GetDiffPercent( double* pValue,DWORD dateCur,int nDays )
{
	if (1==nDays && m_datetech==dateCur)
	{
		if (LastPrice<=1e-4 || ClosePrice<1e-4)
		{
			return false;
		}
		if (pValue)
		{
			*pValue=100.*ClosePrice/PreSettlementPrice-100;
		}
		return true;
	}
	return m_kdata.GetDiffPercent(pValue,dateCur,nDays);
}



bool InstrumentInfo::GetScope( double* pValue,DWORD dateCur,int nDays )
{

	if (1==nDays && m_datetech==dateCur)
	{
		if (LastPrice <= 1e-4 || HighestPrice <= 1e-4 || LowestPrice<= 1e-4)
		{
			return false;
		}
		if (pValue)
		{
			*pValue=100.*(HighestPrice-LowestPrice)/LastPrice;
		}
		return true;
	}
	return m_kdata.GetScope(pValue,dateCur,nDays);
}

bool InstrumentInfo::GetRatioChangeHand( double* pValue,double dVolume )
{
	double dShareCurrency=0.0;
	if (fabs(dShareCurrency) < 1e-4)
	{
		return false;
	}
	if (pValue)
	{
		*pValue=100.*dVolume/dShareCurrency;
	}
	return true;
}

bool InstrumentInfo::GetRatioChangeHand( KdataContainer& kdata,double* pValue,DWORD dateCur,int nDays )
{
	double dVolume =0;
	if (!kdata.GetVolumeSum(&dVolume,dateCur,nDays))
	{
		return false;
	}
	return GetRatioChangeHand(pValue,dVolume);
}

bool InstrumentInfo::GetRatioChangeHand( double* pValue,DWORD dateCur,int nDays )
{
	return GetRatioChangeHand(m_kdata,pValue,dateCur,nDays);
}

bool InstrumentInfo::GetShareCurrency( double* pValue )
{

	return false;
}

bool InstrumentInfo::is_stock_index() const
{
	return string(this->szCode).find("IF",0)==0;
}

std::vector<std::string>& InstrumentInfo::get_type_names()
{
	static std::vector<std::string> g_inst_type_names;
	if (g_inst_type_names.empty())
	{
		std::string InstrumentProductIDNames[]={
			"",  "IF","IC","IH","TF","T",
			"CU","AL","ZN","PB","RU","FU","RB","RB","WR","AU","AG","BU","HC","NI","SN",
			"A", "B", "C", "M", "P", "L", "V", "J","JM", "I", "JD","FB","BB","PP","CS",
			"PM","WH","SR","CF","TA","OL","RI","MA","FG","RS","RM","TC","JR","LR","SF","SM"
		};
		g_inst_type_names.assign(InstrumentProductIDNames,
			InstrumentProductIDNames+sizeof(InstrumentProductIDNames)/sizeof(std::string));
	}
	
	return g_inst_type_names;
}

int InstrumentInfo::get_type()
{
	return m_type;
}
bool InstrumentInfo::update( InstrumentInfo & info, boost::shared_ptr<BASEDATA> pBasedata )
{
		assert( NULL!=pBasedata );
		if( !pBasedata )
			return false;
		if( strlen(pBasedata->szCode) > 0 )
			info.set_id(pBasedata->szExchange,pBasedata->szCode);
		if( strlen(pBasedata->szName) > 0  )
			info.set_name( pBasedata->szName );
#ifdef _MSC_VER
		strncpy_s(
            info.szCode,
            pBasedata->szCode,
			sizeof(pBasedata->szCode)
			);
		strncpy_s(
            info.szExchange,
			pBasedata->szExchange,
			sizeof(pBasedata->szExchange)
			);
		strncpy_s(info.StartDelivDate,pBasedata->StartDelivDate,sizeof(pBasedata->StartDelivDate));
		strncpy_s(info.EndDelivDate,pBasedata->EndDelivDate,sizeof(pBasedata->EndDelivDate));
		strncpy_s(info.ProductID,pBasedata->ProductID,sizeof(pBasedata->ProductID));

		strncpy_s(info.CreateDate,pBasedata->CreateDate,sizeof(pBasedata->CreateDate));
		strncpy_s(info.ExchangeInstID,pBasedata->ExchangeInstID,sizeof(pBasedata->ExchangeInstID));
		strncpy_s(info.szName,pBasedata->szName,sizeof(pBasedata->szName));
		strncpy_s(info.OpenDate,pBasedata->OpenDate,sizeof(pBasedata->OpenDate));
		strncpy_s(info.ExpireDate,pBasedata->ExpireDate,sizeof(pBasedata->ExpireDate));
#else
		strncpy(
            info.szCode,
            pBasedata->szCode,
            sizeof(pBasedata->szCode)
			);
		strncpy(
            info.szExchange,
            pBasedata->szExchange,
            sizeof(pBasedata->szExchange)
			);
		strncpy(info.StartDelivDate,pBasedata->StartDelivDate,sizeof(pBasedata->StartDelivDate));
		strncpy(info.EndDelivDate,pBasedata->EndDelivDate,sizeof(pBasedata->EndDelivDate));
		strncpy(info.ProductID,pBasedata->ProductID,sizeof(pBasedata->ProductID));


		strncpy(info.CreateDate,pBasedata->CreateDate,sizeof(pBasedata->CreateDate));
		strncpy(info.ExchangeInstID,pBasedata->ExchangeInstID,sizeof(pBasedata->ExchangeInstID));
		strncpy(info.szName,pBasedata->szName,sizeof(pBasedata->szName));
		strncpy(info.OpenDate,pBasedata->OpenDate,sizeof(pBasedata->OpenDate));
		strncpy(info.ExpireDate,pBasedata->ExpireDate,sizeof(pBasedata->ExpireDate));
#endif
		info.IsTrading=pBasedata->IsTrading;
		info.DeliveryMonth=pBasedata->DeliveryMonth;
		info.DeliveryYear=pBasedata->DeliveryYear;


		info.VolumeMultiple=pBasedata->VolumeMultiple;
		info.ShortMarginRatio=pBasedata->ShortMarginRatio;
		info.LongMarginRatio=pBasedata->LongMarginRatio;
		info.ProductClass=pBasedata->ProductClass;
		info.PriceTick=pBasedata->PriceTick;
		info.PositionType=pBasedata->PositionType;
		info.PositionDateType=pBasedata->PositionDateType;
		info.MinMarketOrderVolume=pBasedata->MinMarketOrderVolume;
		info.MaxMarketOrderVolume=pBasedata->MaxMarketOrderVolume;
		info.MinLimitOrderVolume=pBasedata->MinLimitOrderVolume;
		info.MaxLimitOrderVolume=pBasedata->MaxLimitOrderVolume;
		info.InstLifePhase=pBasedata->InstLifePhase;

		info.m_basedata.push_back(pBasedata);
		return true;
}

bool InstrumentInfo::update(InstrumentInfo & info, Instrument * pBasedata)
{
	assert(NULL != pBasedata);
	if (!pBasedata)
		return false;
	info.set_id(pBasedata->ExchangeID.c_str(), pBasedata->InstrumentID.c_str());
	info.set_name(pBasedata->InstrumentName.c_str());
	strncpy(info.StartDelivDate, pBasedata->StartDelivDate.c_str(), sizeof(info.StartDelivDate));
	strncpy(info.EndDelivDate, pBasedata->EndDelivDate.c_str(), sizeof(info.EndDelivDate));
	strncpy(info.ProductID, pBasedata->ProductID.c_str(), sizeof(info.ProductID));

	strncpy(info.CreateDate, pBasedata->CreateDate.c_str(), sizeof(info.CreateDate));
	//strncpy_s(info.ExchangeInstID, pBasedata->ExchangeInstID, sizeof(pBasedata->ExchangeInstID));
	strncpy(info.szName, pBasedata->InstrumentName.c_str(), sizeof(info.szName));
	strncpy(info.OpenDate, pBasedata->OpenDate.c_str(), sizeof(pBasedata->OpenDate));
	strncpy(info.ExpireDate, pBasedata->ExpireDate.c_str(), sizeof(pBasedata->ExpireDate));

	info.IsTrading = pBasedata->IsTrading;
	//info.DeliveryMonth = pBasedata->DeliveryMonth;
	//info.DeliveryYear = pBasedata->DeliveryYear;


	info.VolumeMultiple = pBasedata->VolumeMultiple;
	info.ShortMarginRatio = pBasedata->ShortMarginRatio;
	info.LongMarginRatio = pBasedata->LongMarginRatio;
	info.ProductClass = pBasedata->ProductClass;
	info.PriceTick = pBasedata->PriceTick;
	info.PositionType = pBasedata->PositionType;
	//info.PositionDateType = pBasedata->PositionDateType;
	info.MinMarketOrderVolume = pBasedata->MinMarketOrderVolume;
	info.MaxMarketOrderVolume = pBasedata->MaxMarketOrderVolume;
	info.MinLimitOrderVolume = pBasedata->MinLimitOrderVolume;
	info.MaxLimitOrderVolume = pBasedata->MaxLimitOrderVolume;
	//info.InstLifePhase = pBasedata->InstLifePhase;

	//info.m_basedata.add(*pBasedata);
	return true;
}

bool  InstrumentInfo::update( InstrumentInfo & info,const Tick * pReport )
{
		assert(nullptr!=pReport );
		if( !pReport )
			return false;
// 		if (info.m_status)
// 		{
// 			if (info.m_status->InstrumentStatus!=IS_Continous)
// 			{
// 				return false;
// 			}
// 		}
		if( strlen(pReport->szCode) > 0 )
			info.set_id(pReport->szExchange, pReport->szCode);
		if( strlen(pReport->szName) > 0  )
			info.set_name( pReport->szName );
		if( pReport->LastPrice > 1e-4 )	
			info.LastPrice		=	pReport->LastPrice;
		info.OpenPrice		=	pReport->OpenPrice;
		info.HighestPrice		=	pReport->HighestPrice;
		info.LowestPrice			=	pReport->LowestPrice;
		info.ClosePrice		=	pReport->LastPrice;
		info.Volume		=	pReport->Volume;
		info.PreSettlementPrice =pReport->PreSettlementPrice;
		info.SettlementPrice =pReport->SettlementPrice;
		info.LowerLimitPrice=pReport->LowerLimitPrice;
		info.UpperLimitPrice=pReport->UpperLimitPrice;
		info.PreClosePrice = pReport->PreClosePrice;
		info.BidPrice[0]		=	pReport->BidPrice[0];
		info.BidPrice[1]		=	pReport->BidPrice[1];
		info.BidPrice[2]		=	pReport->BidPrice[2];
		info.BidPrice[3]		=	pReport->BidPrice[3];
		info.BidPrice[4]		=	pReport->BidPrice[4];
		info.BidVolume[0]	=	pReport->BidVolume[0];
		info.BidVolume[1]	=	pReport->BidVolume[1];
		info.BidVolume[2]	=	pReport->BidVolume[2];
		info.BidVolume[3]	=	pReport->BidVolume[3];
		info.BidVolume[4]	=	pReport->BidVolume[4];
		info.AskPrice[0]	    =	pReport->AskPrice[0];
		info.AskPrice[1]	    =	pReport->AskPrice[1];
		info.AskPrice[2]	    =	pReport->AskPrice[2];
		info.AskPrice[3]	    =	pReport->AskPrice[3];
		info.AskPrice[4] 	=	pReport->AskPrice[4];
		info.AskVolume[0]	=	pReport->AskVolume[0];
		info.AskVolume[1]	=	pReport->AskVolume[1];
		info.AskVolume[2]	=	pReport->AskVolume[2];
		info.AskVolume[3]	=	pReport->AskVolume[3];
		info.AskVolume[4]	=	pReport->AskVolume[4];

		info.UpdateTime = pReport->UpdateTime;
		info.UpdateMillisec = pReport->UpdateMillisec;
		strcpy(info.UpdateTimeStr, pReport->UpdateTimeStr);

		if (strcmp("CZCE", pReport->szExchange) == 0)
		{
			info.Turnover		=	pReport->Turnover*(info.VolumeMultiple<=0?1:info.VolumeMultiple);
			info.AveragePrice   =   pReport->AveragePrice;
		}
		else if (strcmp("SHFE", pReport->szExchange) == 0 || strcmp("DCE", pReport->szExchange) == 0)
		{
			info.AveragePrice   =   pReport->AveragePrice*(info.VolumeMultiple<=0?1:info.VolumeMultiple);
			info.Turnover	= pReport->Turnover;
		}
		else
		{
			info.AveragePrice = pReport->AveragePrice;
			info.Turnover		=	pReport->Turnover;
		}
		strcpy(info.TradingDay, pReport->TradingDay);
		//info.m_kdata.update(pReport);
		info.m_datetech	=	std::stoi(pReport->TradingDay);

		memcpy( &info.m_reportLatest, pReport, sizeof(info.m_reportLatest) );
		return true;
}

bool InstrumentInfo::update( InstrumentInfo & info,MINUTE* pMinute )
{
	assert(NULL!=pMinute );
	if( !pMinute )
		return false;

	if( strlen(pMinute->szCode) > 0 )
		info.set_id(pMinute->szExchange,pMinute->szCode);
// 	if( strlen(pMinute->szName) > 0  )
// 		info.SetInstrumentName( pMinute->szName );
	if( pMinute->LastPrice > 1e-4 )	info.LastPrice		=	pMinute->LastPrice;
	//info.OpenPrice		=	pMinute->OpenPrice;
	info.HighestPrice		=	pMinute->HighestPrice;
	info.LowestPrice			=	pMinute->LowestPrice;
	info.ClosePrice		=	pMinute->LastPrice;
	info.Volume		=	pMinute->Volume;
	info.Turnover		=	pMinute->Turnover;

	//info.m_fBidPrice[0]		=	pMinute->BidPrice[0];
	//info.m_fBidPrice[1]		=	pMinute->BidPrice[1];
	//info.m_fBidPrice[2]		=	pMinute->BidPrice[2];
	//info.m_fBidPrice[3]		=	pMinute->BidPrice[3];
	//info.m_fBidPrice[4]		=	pMinute->BidPrice[4];
	//info.m_fBidVolume[0]	=	pMinute->BidPrice[0];
	//info.m_fBidVolume[1]	=	pMinute->BidVolume[1];
	//info.m_fBidVolume[2]	=	pMinute->BidVolume[2];
	//info.m_fBidVolume[3]	=	pMinute->BidVolume[3];
	//info.m_fBidVolume[4]	=	pMinute->BidVolume[4];
	//info.m_fAskPrice[0]	    =	pMinute->AskPrice[0];
	//info.m_fAskPrice[1]	    =	pMinute->AskPrice[1];
	//info.m_fAskPrice[2]	    =	pMinute->AskPrice[2];
	//info.m_fAskPrice[3]	    =	pMinute->AskPrice[3];
	//info.m_fAskPrice[4] 	=	pMinute->AskPrice[4];
	//info.m_fAskVolume[0]	=	pMinute->AskVolume[0];
	//info.m_fAskVolume[1]	=	pMinute->AskVolume[1];
	//info.m_fAskVolume[2]	=	pMinute->AskVolume[2];
	//info.m_fAskVolume[3]	=	pMinute->AskVolume[3];
	//info.m_fAskVolume[4]	=	pMinute->AskVolume[4];

	//KDATA	kd;
	//UpdateKDATAByREPORT( kd, pMinute );

	//int nLen = info.m_kdata.GetSize();
	//if( nLen > 0 && info.m_kdata.ElementAt(nLen-1).m_date == kd.m_date )
	//	info.m_kdata.SetAt( nLen-1, kd );
	//else
	//	info.m_kdata.Add( kd );
	//info.m_datetech	=	kd.m_date;

	memcpy( &(info.m_reportLatest), pMinute, sizeof(info.m_reportLatest) );
	return true;
}

bool InstrumentInfo::market_value( double * pValue )
{
	*pValue= (LastPrice * VolumeMultiple);
	return true;
}

bool InstrumentInfo::GetDiffPercentMin5( double* pValue )
{
	return m_minute->GetDiffPercentMin5(pValue);
}

void InstrumentInfo::ResolveTypeAndMarket()
{
	std::vector<std::string> type_names = get_type_names();
	int instType = InstrumentData::typeNone;
	for (std::size_t i = 0; i < type_names.size(); ++i)
	{
		if (stricmp(type_names[i].c_str(), ProductID) == 0)
		{
			m_type = i;
			break;
		}
	}
}

void InstrumentInfo::set_type(int type)
{
	m_type = type; 
}

void InstrumentInfo::SetNameEnu(const char* szNameEnu)
{
	memset(m_szNameEnu, 0, sizeof(m_szNameEnu));
	if (nullptr == szNameEnu)
	{
		return;
	}
	strncpy(m_szNameEnu, szNameEnu, std::min<size_t>(sizeof(m_szNameEnu) - 1, strlen(szNameEnu)));
}

const char* InstrumentInfo::GetNameEnu()
{
	return m_szNameEnu;
}

void InstrumentInfo::SetNameChs(const char* szName)
{
	memset(this->szName, 0, sizeof(this->szName));
	if (nullptr == szName)
		return;
	strncpy(this->szName, szName, std::min<size_t>(sizeof(this->szName) - 1, strlen(szName)));
}

const char* InstrumentInfo::GetNameChs()
{
	return this->szName;
}

boost::shared_ptr<Instrument> InstrumentInfo::toInstrument()
{
	boost::shared_ptr<Instrument> pInstrument = boost::make_shared<Instrument>();
	pInstrument->CreateDate = this->CreateDate;
	pInstrument->EndDelivDate = this->EndDelivDate;
	pInstrument->ExchangeID = this->szExchange;
	pInstrument->ExpireDate = this->ExpireDate;
	pInstrument->InstrumentID = this->szCode;
	pInstrument->InstrumentName = this->szName;
	pInstrument->IsTrading = this->IsTrading;
	pInstrument->LongMarginRatio = this->LongMarginRatio;
	pInstrument->LowerLimitPrice = this->LowerLimitPrice;
	pInstrument->MaxLimitOrderVolume = this->MaxLimitOrderVolume;
	pInstrument->MaxMarketOrderVolume = this->MaxMarketOrderVolume;
	pInstrument->MinLimitOrderVolume = this->MinLimitOrderVolume;
	pInstrument->MinMarketOrderVolume = this->MinMarketOrderVolume;
	pInstrument->OpenDate = this->OpenDate;
	pInstrument->PositionType = (EnumPositionType)this->PositionType;
	pInstrument->PreSettlementPrice = this->PreSettlementPrice;
	pInstrument->PriceTick = this->PriceTick;
	pInstrument->ProductClass = (EnumProductClass)this->ProductClass;
	pInstrument->ProductID = this->ProductID;
	pInstrument->ShortMarginRatio = this->ShortMarginRatio;
	pInstrument->StartDelivDate = this->StartDelivDate;
	pInstrument->UpperLimitPrice = this->UpperLimitPrice;
	pInstrument->VolumeMultiple = this->VolumeMultiple;
	return pInstrument;
}



bool convert_MINUTE_to_REPORT(  MINUTE * pMinute,Tick * pReport )
{
	assert( pReport && pMinute );
	if( NULL == pReport || NULL == pMinute )
		return false;

	memset( pReport, 0, sizeof(MINUTE) );
#ifdef _MSC_VER
	strncpy_s(pReport->szExchange, pMinute->szExchange, sizeof(pMinute->szExchange));
	strncpy_s( pReport->szCode, pMinute->szCode, std::min<size_t>(sizeof(pReport->szCode)-1,sizeof(pMinute->szCode)) );
#else
	strncpy(pReport->szExchange,pMinute->szExchange,sizeof(pMinute->szExchange));
	strncpy( pReport->szCode, pMinute->szCode, min(sizeof(pReport->szCode)-1,sizeof(pMinute->szCode)) );
#endif
	if (pMinute->Type <= 0)
	{
		pMinute->Type = ktypeMin;
	}
	time_t	temp = pMinute->Type * (pMinute->TradingTime / pMinute->Type);
	if( temp < pMinute->TradingTime )
		temp += pMinute->Type;
	strcpy(pMinute->Tradingday, pReport->TradingDay);
	pReport->UpdateTime		=	temp;
	pReport->LastPrice		=	pMinute->LastPrice;
	pReport->HighestPrice	=	pMinute->HighestPrice;
	pReport->LowestPrice		=	pMinute->LowestPrice;
	pReport->Volume	=	pMinute->Volume;
	pReport->Turnover	=	pMinute->Turnover;
	return true;
}

bool convert_REPORT_to_MINUTE(const Tick * pReport, MINUTE * pMinute )
{
	assert( pReport && pMinute );
	if (nullptr == pReport || nullptr == pMinute)
		return false;

	int mType = pMinute->Type;
	memset( pMinute, 0, sizeof(MINUTE) );

	pMinute->Type = mType;	// 1 min
	if (pMinute->Type <= 0)
	{
		pMinute->Type=ktypeMin;
	}
#ifdef _MSC_VER
	strncpy_s(pMinute->szExchange, pReport->szExchange, min(sizeof(pMinute->szExchange), sizeof(pReport->szExchange)));
	strncpy_s( pMinute->szCode, pReport->szCode, min(sizeof(pMinute->szCode)-1,sizeof(pReport->szCode)) );
#else
	strncpy(pMinute->szExchange,pReport->szExchange,sizeof(pReport->szExchange));
	strncpy( pMinute->szCode, pReport->szCode, min(sizeof(pMinute->szCode)-1,sizeof(pReport->szCode)) );
#endif
	time_t	temp = pMinute->Type * (pReport->UpdateTime / pMinute->Type);
// 	if( temp < pReport->TradingTime )
// 		temp	+=	60;
	strcpy(pMinute->Tradingday, pReport->TradingDay);
	pMinute->TradingTime		=	temp;
	pMinute->LastPrice		=	pReport->LastPrice;
	pMinute->HighestPrice	=	pReport->LastPrice;
	pMinute->LowestPrice		=	pReport->LastPrice;
	pMinute->Volume	=	pReport->Volume;
	pMinute->Turnover	=	pReport->Turnover;
	return true;
}


bool convert_KDATA_to_REPORT(const KDATA * pKDATA, Tick * pReport)
{
	assert(pReport && pKDATA);
	if (nullptr == pReport || nullptr == pKDATA)
		return false;
	strncpy(pReport->szExchange, pKDATA->szExchange, sizeof(pReport->szExchange));
	strncpy(pReport->szCode, pKDATA->szCode, min(sizeof(pKDATA->szCode) - 1, sizeof(pReport->szCode)));
	pReport->UpdateTime = pKDATA->TradingTime;
	pReport->UpdateMillisec = 0;
	strcpy(pReport->TradingDay, pKDATA->TradingDay);
	pReport->LastPrice = pKDATA->ClosePrice ;
	pReport->HighestPrice = pKDATA->HighestPrice ;
	pReport->LowestPrice = pKDATA->LowestPrice ;
	pReport->Volume = pKDATA->Volume ;
	pReport->Turnover = pKDATA->Turnover ;
	pReport->OpenInterest = pKDATA->OpenInterest;
	pReport->PreSettlementPrice = pKDATA->PreSettlementPrice;
	return true;
}

// update KDATA by Report data
bool UpdateKDATAByREPORT( KDATA &kd,const Tick * pReport )
{
	assert( pReport );
	if( NULL == pReport )
		return false;
	memset( &kd, 0, sizeof(kd) );

	strcpy(kd.szExchange, pReport->szExchange);
	strncpy( kd.szCode, pReport->szCode, min(sizeof(kd.szCode)-1,sizeof(pReport->szCode)) );

	kd.TradingTime	=	pReport->UpdateTime;
// 	if( 0 == pReport->UpdateTime || -1 == pReport->UpdateTime )
// 		kd.TradingDate	=	DateTime::current_time().ToInstrumentTimeDay();
// 	else
// 		kd.TradingDate	=	DateTime(pReport->UpdateTime).ToInstrumentTimeDay();
	kd.TradingDate = std::stoi(pReport->TradingDay);
	strcpy(kd.TradingDay,pReport->TradingDay);
	kd.OpenPrice		=	pReport->OpenPrice;
	kd.HighestPrice		=	pReport->HighestPrice;
	kd.LowestPrice		=	pReport->LowestPrice;
	kd.ClosePrice		=	pReport->LastPrice;
	kd.Turnover	=	pReport->Turnover;
	kd.Volume	=	pReport->Volume;
	kd.PreClosePrice = pReport->PreClosePrice;
	return true;
}

