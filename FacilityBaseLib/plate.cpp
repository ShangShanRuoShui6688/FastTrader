#include "plate.h"
#include <complier.h>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include "InstrumentInfo.h"

//////////////////////////////////////////////////////////////////////
// class CDomain

Plate::Plate( )
{
}

Plate::Plate( const Plate &src )
{
	*this	=	src;
}

Plate::~Plate( )
{
}

bool Plate::add(const std::string& id)
{
	return add(id.c_str());
}
//添加合约到板块;
bool Plate::add(  const char*  lpszStockCode )
{
	if( NULL == lpszStockCode || strlen(lpszStockCode) <= 0 )
		return false;

	for( size_t k=0; k<size(); k++ )
	{
		int nCmp = stricmp(m_code_list[k].c_str(),(lpszStockCode));
		if( 0 == nCmp )
			return false;
	}

	m_code_list.push_back( lpszStockCode );
	return true;
}

bool Plate::add_sort(  const char*  lpszStockCode )
{
	if( NULL == lpszStockCode || strlen(lpszStockCode) <= 0 )
		return false;

	for( size_t k=0; k<m_code_list.size(); k++ )
	{
		int nCmp = stricmp(m_code_list[k].c_str(),lpszStockCode);
		if( 0 == nCmp )
			return false;
		if( nCmp > 0 )
		{
			m_code_list[k]=lpszStockCode;
			return true;
		}
	}

	m_code_list.push_back( lpszStockCode );
	return true;
}

bool Plate::remove(  const char*  lpszStockCode )
{
	if( NULL == lpszStockCode || strlen(lpszStockCode) <= 0 )
		return false;

	for( size_t k=0; k<m_code_list.size(); k++ )
	{
		if( 0 == stricmp(m_code_list[k].c_str(),(lpszStockCode)) )
		{
			m_code_list.erase(m_code_list.begin()+k);
			return true;
		}
	}
	return false;
}

Plate &Plate::operator = ( const Plate &src )
{
	m_strName		=	src.m_strName;
	m_code_list =src.m_code_list;
	return *this;
}

struct fxj_block_t {
	WORD	wMarket;
	char	szCode[10];
};

// int plate::AddFxjDomain( const char* lpszFile )
// {
// // 	CFile file;
// // 	DWORD	dwMagic;
// // 	if( !file.Open( lpszFile, CFile::modeRead )
// // 		|| sizeof(dwMagic) != file.Read(&dwMagic,sizeof(dwMagic)) || dwMagic != 0xFF5100A5 )
// // 		return 0;
// // 
// // 	int nCount = 0;
// // 	struct fxj_block_t stock;
// // 	while( sizeof(stock) == file.Read(&stock,sizeof(stock)) )
// // 	{
// // 		if( 0 == stock.szCode[6] )
// // 		{
// // 			CString sCode = CString(stock.szCode);
// // 			if( sCode.GetLength() == 4 )
// // 				sCode = _T("00") + sCode;
// // 			if( sCode.GetLength() == 6 )
// // 				AddInstrument( sCode );
// // 		}
// // 	}
// 
// 	return size();
// }

void Plate::copy(const std::vector<std::string>& instruments )
{
	m_code_list.resize(instruments.size());
	std::copy(instruments.begin(),instruments.end(),m_code_list.begin());
}

int Plate::find( const char* lpszCode )
{
	std::vector<std::string>::iterator reg_it=
		std::find(m_code_list.begin(),m_code_list.end(),lpszCode);
	if (reg_it==m_code_list.end())
	{
		return -1;
	}
	return reg_it-m_code_list.begin();
}

std::size_t Plate::size()
{
	return m_code_list.size();
}

void Plate::clear()
{
	m_code_list.clear();
}

std::string Plate::operator[]( std::size_t index )
{
	return m_code_list[index];
}

std::vector<std::string>& Plate::get()
{
	return m_code_list;
}

void Plate::remove( std::size_t index )
{
	m_code_list.erase(m_code_list.begin()+index);
}

Plate::Plate( const std::vector<std::string>& astr )
{
	m_code_list=astr;
}

std::string Plate::get_id() const
{
	return m_strId;
}

void Plate::set_id( const std::string& plate_id )
{
	m_strId=plate_id;
}


plate_container::plate_container()
{
}

plate_container::plate_container(const plate_container & src )
    :std::vector<Plate>()
{
	*this	=	src;
}

plate_container::~plate_container( )
{
	clear();
}

plate_container & plate_container::operator = (const plate_container & src )
{
	resize( src.size() );
	for( size_t i=0; i<src.size(); i++ )
	{
		Plate domain = src[i];
		(*this)[i]= domain;
	}
	return *this;
}

int plate_container::add( const char* lpszDomainName )
{
	if( NULL == lpszDomainName || strlen(lpszDomainName) <=0  )
		return -1;
	Plate	newdomain;
	newdomain.m_strName	=	lpszDomainName;
	return add( newdomain );
}

//添加不重复的板块;
int plate_container::add( Plate & newdomain )
{
	if( 0 == newdomain.m_strName.length() )
		return -1;

	for( size_t i=0; i<size(); i++ )
	{
		Plate & domain = (*this)[i];
		if( 0 ==stricmp(newdomain.m_strName.c_str(), domain.m_strName.c_str() ) )
			return -1;
	}
	push_back( newdomain );
	return size()-1;
}
//如果要加入的板块已存在，则替换之，否则添加到板块列表;
int plate_container::replace( Plate & newdomain )
{
	if( 0 == newdomain.m_strName.length() )
		return -1;

	for( size_t i=0; i<size(); i++ )
	{
		Plate & domain = (*this)[i];
		if( 0 ==stricmp(newdomain.m_strName.c_str(), domain.m_strName.c_str() ) )
		{
			(*this)[i]=newdomain;
			return i;
		}
	}
	push_back( newdomain );
	return size()-1;
}

int plate_container::replace( plate_container & newdomains )
{
	int nCount = 0;
	for( size_t i=0; i<newdomains.size(); i++ )
	{
		if( replace( newdomains[i] ) >= 0 )
			nCount ++;
	}
	return nCount;
}

bool plate_container::remove( const char* lpszDomainName )
{
	if( NULL == lpszDomainName || strlen(lpszDomainName) <= 0 )
		return false;

	for( size_t i=0; i<size(); i++ )
	{
		Plate & domain = (*this)[i];
		if( 0 == strcmp(domain.m_strName.c_str(), lpszDomainName ) )
		{
			erase(begin()+ i );
			//array_container<plate>::remove(i);
			return true;
		}
	}

	return false;
}

bool plate_container::add_code( const char* lpszDomainName,  const char*  lpszStockCode )
{
    if( NULL == lpszDomainName || strlen(lpszDomainName) <= 0
        || NULL == lpszStockCode || strlen(lpszStockCode) <= 0 )
		return false;

	for( size_t i=0; i<size(); i++)
	{
		Plate	& domain	=	(*this)[i];
		if( 0 == stricmp(domain.m_strName.c_str(),lpszDomainName ) )
		{
			return domain.add( lpszStockCode );
		}
	}

	return false;
}

bool plate_container::add_code( const char* lpszDomainName, std::vector<std::string> & astr )
{
    if( NULL == lpszDomainName || strlen(lpszDomainName) <= 0 )
		return false;

	for( size_t i=0; i<size(); i++ )
	{
		Plate	& domain	=	(*this)[i];
		if( 0 == stricmp(domain.m_strName.c_str(), lpszDomainName ) )
		{
			for( size_t j=0; j<astr.size(); j++ )
				domain.add(astr[j].c_str() );
			return true;
		}
	}

	return false;
}

bool plate_container::remove( const char* lpszDomainName, const char* lpszStockCode )
{
    if( NULL == lpszDomainName || strlen(lpszDomainName) <=0
        || NULL == lpszStockCode || strlen(lpszStockCode) <= 0 )
		return false;

	for( size_t i=0; i<size(); i++ )
	{
		Plate	& domain	=	(*this)[i];
		if( 0 == stricmp(domain.m_strName.c_str(), lpszDomainName ) )
		{
			for( size_t k=0; k<domain.size(); k++ )
			{
				if( 0 == stricmp(domain[k].c_str(),lpszStockCode) )
				{
					domain.remove(k);
					return true;
				}
			}
			break;
		}
	}

	return false;
}

bool plate_container::remove_plate( const char* lpszDomainName )
{
    if( NULL == lpszDomainName || strlen(lpszDomainName) <= 0 )
		return false;

	for( size_t i=0; i<size(); i++ )
	{
		Plate	& domain	=	(*this)[i];
		if( 0 == stricmp(domain.m_strName.c_str(), lpszDomainName ) )
		{
			domain.clear();
			return true;
		}
	}

	return false;
}

bool plate_container::get_codes( const char* lpszDomain, std::vector<std::string> &astr )
{
    if( NULL == lpszDomain || strlen(lpszDomain) <= 0 )
		return false;

	for( size_t i=0; i<size(); i++ )
	{
		Plate	& domain	=	(*this)[i];
		if( 0 ==stricmp(domain.m_strName.c_str(),( lpszDomain )) )
		{
			//astr.Copy( domain );
			astr=domain.get();
			return true;
		}
	}

	return false;
}

bool plate_container::get_plates( vector<string> & astr )
{
	astr.resize( size() );

	for( size_t i=0; i<size(); i++ )
	{
		Plate	& domain	=	(*this)[i];
		string	str	=	domain.m_strName;
		astr[i]=str;
	}
	return true;
}

bool plate_container::get_all_plate_info( InstrumentContainer * pContainer, DWORD dwDate )
{
	assert( pContainer );
	if( NULL == pContainer )
		return false;

 //   DWORD	dwDateLatest = -1;
//    instrument_container::get_instance().GetLatestTechDate( &dwDateLatest );
	// 
//    for( size_t nDomain=0; nDomain<size(); nDomain++ )
//        {
//            plate	&	domain	=	at(nDomain);
//            instrument_container	cntn;
//            if( cntn.RetrieveSpecify( domain ) )
//            {
//                if( -1 != dwDate && dwDateLatest != dwDate )
//                {
//                    for( int i=0; i<cntn.GetSize(); i++ )
//                    {
//                        instrument_info	& info	=	cntn.at(i);
//                        info.StatBaseIndex( dwDate );
//                        info.StatTechIndex( dwDate );
//                    }
//                }
//                // cntn.SetMap( );
//                cntn.SetAverage( );

//                instrument_info	info	=	cntn.GetAverage( );
//                info.SetStockName( domain.m_strName );
//                info.SetStockShortName( domain.m_strName );
//                pContainer->Add( info );
//            }
//        }
//        pContainer->SetMap();
//        pContainer->SetAverage();
	return true;
}

#define	SF_MAX_DOMAINFILE_LENGTH	0x10000000

bool plate_container::load( const char* lpszFileName )
{
	if( NULL == lpszFileName || strlen(lpszFileName) <= 0 )
		return false;

	bool	bOK	=	false;
	std::fstream file;
	file.open( lpszFileName,ios::in|ios::out );
	if( file.is_open() )
	{
		uintmax_t dwLen = boost::filesystem::file_size(lpszFileName);
		if( 0 == dwLen || dwLen > SF_MAX_DOMAINFILE_LENGTH )
		{
			file.close();
			return false;
		}
		Plate	domain;
		std::string	rString;
 		while( std::getline(file,rString) )
 		{
			int	nIndex	=	rString.find( "\r" );
			if( -1 == nIndex )
				nIndex = rString.find("\n");
			if( -1 != nIndex )
				rString	=	rString.substr(0, nIndex );
			//rString.TrimLeft();
			//rString.TrimRight();

			boost::algorithm::trim_left(rString);
			boost::algorithm::trim_right(rString);

			if( rString.length() > 0 )
			{
				if( domain.m_strName.empty() )	// 新版块名称;
					domain.m_strName	=	rString;
				else
					domain.add( rString );
			}
			else
			{	// 版块结束;
				if( !domain.m_strName.empty() )
					add( domain );
				domain.m_strName.clear();
				domain.clear();
			}
 		}
		if( !domain.m_strName.empty() )
			push_back( domain );
		domain.m_strName.clear();
		domain.clear();

		file.close();
		bOK	=	true;
	}

	return bOK;
}

bool plate_container::save( const char* lpszFileName )
{
	if( NULL == lpszFileName || strlen(lpszFileName) <= 0 )
		return false;

	std::fstream file;
	std::string STRING_CRLF=("\r\n");
	file.open( lpszFileName, ios::out );
	if( file.is_open() )
	{
		for( size_t i=0; i<size(); i++ )
		{
			Plate	& domain = (*this)[i];
			std::string	strDomain	=	domain.m_strName;
			file.write( strDomain.c_str(), strDomain.length());
			file.write( STRING_CRLF.c_str(),STRING_CRLF.length() );
			for( size_t k=0; k<domain.size(); k++ )
			{
				std::string	strStock	=	domain[k];
				if( strStock.length() > 0 )
				{
					file.write( strStock.c_str(), strStock.length() );
					file.write( STRING_CRLF.c_str(),STRING_CRLF.length() );
				}
			}
			file.write( STRING_CRLF.c_str(), STRING_CRLF.length() );
		}

		file.close();
		return true;
	}
	return false;
}

plate_container &plate_container::get_domain_container()
{
	static	plate_container	g_domaincontainer;
	return g_domaincontainer;
}

plate_container &plate_container::get_group_container( )
{
	static	plate_container	g_groupcontainer;
	if (g_groupcontainer.empty())
	{
		//为空;
		Plate p;
		p.m_strName="我的板块";
		p.set_id("myplate");
		g_groupcontainer.add(p);
	}
	return g_groupcontainer;
}
