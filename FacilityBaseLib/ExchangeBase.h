#ifndef _EXCHANGE_BASE_H_
#define _EXCHANGE_BASE_H_
#pragma once
#include "Exchange.h"
#include "platform.h"
#include "facilitybaselib.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/thread/mutex.hpp>

//交易所类;
class FACILITY_API ExchangeBase
{
public:
	ExchangeBase();
	explicit ExchangeBase(const exchange_t& exchange);
	ExchangeBase(const exchange_t& exchange,
		std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> >& dayTimeSegs,
		std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> >& nightTimeSegs);
	~ExchangeBase();

	std::string get_exchange_id();
	std::string get_exchange_name();
	int get_exchange_property();
	int get_market_id();
	void update(const exchange_t& exchange);
	boost::shared_ptr<exchange_t> toExchange();
public:
	//获取白天的开盘时间;
	bool get_day_open_time(boost::posix_time::time_duration& timeDuration);
	//获取天的的收盘时间;
	bool get_day_close_time(boost::posix_time::time_duration& timeDuration);
	//获取白天的交易时间段;
	bool get_day_trade_segments(std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> >& dayTimeSegs);
	//设置白天的交易时段;
	bool set_day_trade_segments(std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> >& dayTimeSegs);

	//获取夜盘的开盘时间;
	bool get_night_open_time(boost::posix_time::time_duration& timeDuration);
	//获取夜盘的收盘时间;
	bool get_night_close_time(boost::posix_time::time_duration& timeDuration);
	//获取夜盘的交易时间段;
	bool get_night_trade_segments(std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> >& dayTimeSegs);
	//设置夜盘交易时间段;
	bool set_night_trade_segments(std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> >& nightTimeSegs);

	//获取下一个交易节点;
	bool get_next_trade_segment(const boost::posix_time::time_duration& input, boost::posix_time::time_duration& output,bool& bTimeIsOpen);

	//获取一天之中的交易秒数;
	int	GetTradeSecondsOfOneDay();
	//取得下一个时间;
	DWORD	GetInstrumentTimeNext(DWORD dwDate, int ktype, DWORD dwYear = -1);
	//取得最近的有效的交易时间;
	time_t	GetTimeTradeLatest(time_t tmTradeFirstToday);

	//double	GetTimeTradeRatioOfOneDay(DateTime tTradeLatestDay, DateTime tNow);

	time_t	GetTradeOffsetToTime(int offset, time_t tmDay);

	time_t	GetLatestTradeTime(time_t tmNow);

	bool	InTradeTime(time_t tm, int nInflateSeconds = 180);

	bool	FromInstrumentTimeDay(DWORD dwDate);
	bool	FromInstrumentTimeMin(DWORD dwDate, DWORD dwYear = -1);
	bool	FromInstrumentTime(DWORD dwDate, bool bDayOrMin, DWORD dwYear = -1);
	void get_trading_day(std::string& date) const;
	//获取交易日,DOS日期格式;20161130
	DWORD get_trading_day() const;
	//设置交易日;
	void set_trading_day(DWORD dosDate);
	//设置交易日;
	void set_trading_day(const std::string& date);
protected:
	boost::gregorian::date m_TradingDay;
	boost::mutex m_Mutex;
	//组合优于继承;
	exchange_t m_Exchange;
	//交易时间段;
	std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> > m_DaySegments;
	std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> > m_NightSegments;
};


#endif

