#include "ExchangeBase.h"

#include <boost/lexical_cast.hpp>
#include <boost/make_shared.hpp>
#include <numeric>
#include "../Log/logging.h"
#include <boost/date_time/posix_time/posix_time.hpp>

ExchangeBase::ExchangeBase()
{
	memset(&m_Exchange, 0, sizeof(m_Exchange));
}

ExchangeBase::ExchangeBase(const exchange_t & exchange)
{
	m_Exchange = exchange;
}

ExchangeBase::ExchangeBase(const exchange_t & exchange,
	std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> >& dayTimeSegs,
	std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> >& nightTimeSegs)
{
	m_Exchange = exchange;
	set_day_trade_segments(dayTimeSegs);
	set_night_trade_segments(nightTimeSegs);
}


ExchangeBase::~ExchangeBase()
{

}

std::string ExchangeBase::get_exchange_id()
{
	return std::string(m_Exchange.ExchangeID);
}

std::string ExchangeBase::get_exchange_name()
{
	return std::string(m_Exchange.ExchangeName);
}

int ExchangeBase::get_exchange_property()
{
	return m_Exchange.ExchangeProperty;
}

int ExchangeBase::get_market_id()
{
	return m_Exchange.dwMarket;
}

void ExchangeBase::update(const exchange_t & exchange)
{
	m_Exchange.dwMarket = exchange.dwMarket;
	strncpy(m_Exchange.ExchangeID, exchange.ExchangeID,sizeof(m_Exchange.ExchangeID));
	strncpy(m_Exchange.ExchangeName, exchange.ExchangeName, sizeof(m_Exchange.ExchangeName));
	m_Exchange.ExchangeProperty=exchange.ExchangeProperty;
	//更新日盘时间;
	if (exchange.DayTimeSegments[0][0] != exchange.DayTimeSegments[0][1])
	{
		m_DaySegments.clear();
		size_t nCountDay = sizeof(m_Exchange.DayTimeSegments)/sizeof(m_Exchange.DayTimeSegments[0]);
		for (size_t i = 0; i < nCountDay; i++)
		{
			if (exchange.DayTimeSegments[i][0] != exchange.DayTimeSegments[i][1])
			{
				m_Exchange.DayTimeSegments[i][0] = exchange.DayTimeSegments[i][0];
				m_Exchange.DayTimeSegments[i][1] = exchange.DayTimeSegments[i][1];
				std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> time_seg =
				{ boost::posix_time::seconds(exchange.DayTimeSegments[i][0]),boost::posix_time::seconds(exchange.DayTimeSegments[i][1]) };
				m_DaySegments.push_back(time_seg);
			}
		}
	}
	
	//更新夜盘时间;
	if (exchange.NightTimeSegments[0][0] != exchange.NightTimeSegments[0][1])
	{
		m_DaySegments.clear();
		size_t nCountNight = sizeof(m_Exchange.NightTimeSegments) / sizeof(m_Exchange.NightTimeSegments[0]);
		for (size_t i = 0; i < nCountNight; i++)
		{
			if (exchange.NightTimeSegments[i][0] != exchange.NightTimeSegments[i][1])
			{
				m_Exchange.NightTimeSegments[i][0] = exchange.NightTimeSegments[i][0];
				m_Exchange.NightTimeSegments[i][1] = exchange.NightTimeSegments[i][1];
				std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> time_seg =
				{ boost::posix_time::seconds(exchange.NightTimeSegments[i][0]),boost::posix_time::seconds(exchange.NightTimeSegments[i][1]) };
				m_NightSegments.push_back(time_seg);
			}
		}
	}
}

bool ExchangeBase::get_day_open_time(boost::posix_time::time_duration & timeDuration)
{
	if (m_DaySegments.empty())
	{
		return false;
	}
	timeDuration=m_DaySegments.begin()->first;
	return true;
}

bool ExchangeBase::get_day_close_time(boost::posix_time::time_duration & timeDuration)
{
	if (m_DaySegments.empty())
	{
		return false;
	}
	timeDuration = m_DaySegments.rbegin()->second;
	return false;
}

bool ExchangeBase::get_day_trade_segments(std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration>>& dayTimeSegs)
{
	if (m_DaySegments.empty())
	{
		return false;
	}
	dayTimeSegs = m_DaySegments;
	return true;
}

bool ExchangeBase::set_day_trade_segments(std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration>>& dayTimeSegs)
{
	size_t segCount = std::min<size_t>(sizeof(m_Exchange.DayTimeSegments),dayTimeSegs.size());
	if (segCount <= 0)
	{
		return false;
	}
	for (size_t i = 0; i < segCount; i++)
	{
		m_Exchange.DayTimeSegments[i][0] = dayTimeSegs[i].first.total_seconds();
		m_Exchange.DayTimeSegments[i][1] = dayTimeSegs[i].second.total_seconds();
	}
	m_DaySegments = dayTimeSegs;
	return true;
}

bool ExchangeBase::get_night_open_time(boost::posix_time::time_duration & timeDuration)
{
	if (m_NightSegments.empty())
	{
		return false;
	}
	timeDuration = m_NightSegments.begin()->second;
	return false;
}

bool ExchangeBase::get_night_close_time(boost::posix_time::time_duration & timeDuration)
{
	if (m_NightSegments.empty())
	{
		return false;
	}
	timeDuration = m_NightSegments.rbegin()->second;
	return false;
}

bool ExchangeBase::get_night_trade_segments(std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration>>& dayTimeSegs)
{
	if (m_NightSegments.empty())
	{
		return false;
	}
	dayTimeSegs = m_NightSegments;
	return true;
}

bool ExchangeBase::set_night_trade_segments(std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration>>& nightTimeSegs)
{
	size_t segCount = std::min<size_t>(sizeof(m_Exchange.NightTimeSegments), nightTimeSegs.size());
	if (segCount <= 0)
	{
		return false;
	}
	for (size_t i = 0; i < segCount; i++)
	{
		m_Exchange.NightTimeSegments[i][0] = nightTimeSegs[i].first.total_seconds();
		m_Exchange.NightTimeSegments[i][1] = nightTimeSegs[i].second.total_seconds();
	}
	m_NightSegments = nightTimeSegs;
	return true;
}

bool ExchangeBase::get_next_trade_segment(const boost::posix_time::time_duration & input, boost::posix_time::time_duration & output,bool& bTimeIsOpen)
{
	//先查看日盘时间段;

	for (size_t i = 0; i < m_DaySegments.size(); i++)
	{
		std::string sz_time_open = boost::posix_time::to_iso_string(m_DaySegments[i].first);
		std::string sz_time_close = boost::posix_time::to_iso_string(m_DaySegments[i].second);
		std::string sz_time_input = boost::posix_time::to_iso_string(input);
		LOGDEBUG("day {} open {},close {},input {}",i,sz_time_open,sz_time_close,sz_time_input);
		if (input <= m_DaySegments[i].first)
		{
			output = m_DaySegments[i].first;
			bTimeIsOpen = true;
			return true;
		}
		else if (input > m_DaySegments[i].first && input <= m_DaySegments[i].second)
		{
			output = m_DaySegments[i].second;
			bTimeIsOpen = false;
			return true;
		}
	}
	//再查看夜盘时间段;
	for (size_t i = 0; i < m_NightSegments.size(); i++)
	{
		std::string sz_time_open = boost::posix_time::to_iso_string(m_NightSegments[i].first);
		std::string sz_time_close = boost::posix_time::to_iso_string(m_NightSegments[i].second);
		std::string sz_time_input = boost::posix_time::to_iso_string(input);
		//LOGDEBUG("night {} open {},close {},input {}",i,sz_time_open, sz_time_close, sz_time_input);
		if (input <= m_NightSegments[i].first)
		{
			output = m_NightSegments[i].first;
			bTimeIsOpen = true;
			return true;
		}
		else if (input > m_NightSegments[i].first && input <= m_NightSegments[i].second)
		{
			output = m_DaySegments[i].second;
			bTimeIsOpen = false;
			return true;
		}
		else
		{
			//不在当天的交易时间段内;

		}
	}
	return false;
}

int ExchangeBase::GetTradeSecondsOfOneDay()
{
	//日盘交易时间;
	int secs_spans[4] =
	{
		m_Exchange.DayTimeSegments[0][1] - m_Exchange.DayTimeSegments[0][0],
		m_Exchange.DayTimeSegments[1][1] - m_Exchange.DayTimeSegments[1][0],
		m_Exchange.DayTimeSegments[2][1] - m_Exchange.DayTimeSegments[2][0],
		m_Exchange.DayTimeSegments[3][1] - m_Exchange.DayTimeSegments[3][0],
	};

	int total_seconds = std::accumulate(secs_spans, secs_spans+sizeof(secs_spans), 0, std::plus<int>());

	return total_seconds;
	//switch (exType)
	//{
	//case EIDT_SHFE:
	//case EIDT_CZCE:
	//case EIDT_DCE:
	//case EIDT_INE:
	//{
	//	int timeSecSections[] = {
	//		//09.00-10.15;
	//		GetTimeSection(2,exType).GetTotalSeconds() - GetTimeSection(1,exType).GetTotalSeconds(),//第一个交易节的时间;
	//																								//10.15-10.30;
	//		GetTimeSection(3,exType).GetTotalSeconds() - GetTimeSection(2,exType).GetTotalSeconds(),//第二个交易节的时间;
	//		//10.30-11.30;
	//		GetTimeSection(4,exType).GetTotalSeconds() - GetTimeSection(3,exType).GetTotalSeconds(),//第三个交易节的时间;
	//																			//11.30-13.30;
	//		GetTimeSection(5,exType).GetTotalSeconds() - GetTimeSection(4,exType).GetTotalSeconds(),//第四个交易节的时间;
	//		//13.30-15.00;
	//		GetTimeSection(6,exType).GetTotalSeconds() - GetTimeSection(5,exType).GetTotalSeconds()//第五个交易节的时间;
	//	};
	//	//中场休息时间;
	//	int timeSpaceTime[] = { timeSecSections[1] - timeSecSections[0],timeSecSections[4] - timeSecSections[3] };
	//	return timeSecSections[0] + timeSecSections[1] + timeSecSections[2] + timeSecSections[3] + timeSecSections[4] -
	//		timeSpaceTime[0] - timeSpaceTime[1];
	//}
	//break;
	//case EIDT_CFFEX:
	//{
	//	int timeSecSections[] = {
	//		//09.15-11.30;
	//		GetTimeSection(2,exType).GetTotalSeconds() - GetTimeSection(1,exType).GetTotalSeconds(),//第一个交易节的时间;
	//		//11.30-13.00;
	//		GetTimeSection(3,exType).GetTotalSeconds() - GetTimeSection(2,exType).GetTotalSeconds(),//第二个交易节的时间;
	//		//13.00-15.15;
	//		GetTimeSection(4,exType).GetTotalSeconds() - GetTimeSection(3,exType).GetTotalSeconds(),//第三个交易节的时间;
	//		//11.30-13.30;
	//	};
	//	//中场休息时间;
	//	int timeSpaceTime[] = { timeSecSections[1] - timeSecSections[0] };
	//	return timeSecSections[0] + timeSecSections[1] + timeSecSections[2] - timeSpaceTime[0];
	//}
	//break;
	//default:
	//	break;
	//}
	return 0;
}

void ExchangeBase::get_trading_day(std::string & date) const
{
	date=boost::gregorian::to_iso_string(m_TradingDay);
}

DWORD ExchangeBase::get_trading_day() const
{
	std::string date;
	get_trading_day(date);
	try
	{
		return boost::lexical_cast<DWORD>(date);
	}
	catch (const std::exception&)
	{
		assert(false);
	}
	return 0;
}

void ExchangeBase::set_trading_day(DWORD dosDate)
{
	std::string date = std::to_string(dosDate);
	set_trading_day(date);
}

void ExchangeBase::set_trading_day(const std::string & date)
{
	boost::mutex::scoped_lock lck(m_Mutex);
	m_TradingDay = boost::gregorian::date_from_iso_string(date);
}


//获得下一个时间;
DWORD ExchangeBase::GetInstrumentTimeNext(DWORD dwDate, int ktype, DWORD dwYear)
{
	boost::posix_time::ptime	sptime;
	//if (ktype == ktypeDay) //日线
	//{
	//	if (!sptime.FromInstrumentTimeDay(dwDate))
	//		return -1;
	//	if (6 == sptime.GetDayOfWeek())	//	Friday
	//		sptime += TimeSpan(3, 0, 0, 0);//如果是周5，跳过3天
	//	else
	//		sptime += TimeSpan(1, 0, 0, 0);//其他情况跳过1天
	//	return sptime.ToInstrumentTimeDay();
	//}
	//else if (ktype == ktypeWeek)
	//{
	//	if (!sptime.FromInstrumentTimeDay(dwDate))//周线
	//		return -1;
	//	sptime += TimeSpan(7, 0, 0, 0);//跳过7天
	//	return sptime.ToInstrumentTimeDay();
	//}
	//else if (ktype == ktypeMonth)
	//{
	//	if (!sptime.FromInstrumentTimeDay(dwDate))
	//		return -1;
	//	int	nYearNew = sptime.GetYear();
	//	int nMonthNew = sptime.GetMonth();
	//	//跳过一个月
	//	nYearNew = (nMonthNew >= 12 ? nYearNew + 1 : nYearNew);//如果是12月份，年份加1
	//	nMonthNew = (nMonthNew >= 12 ? 1 : nMonthNew + 1);//如果是12月份，月份变为1
	//	DateTime	sptime2(nYearNew, nMonthNew, sptime.GetDay(), sptime.GetHour(), sptime.GetMinute(), sptime.GetSecond());

	//	if (6 == sptime2.GetDayOfWeek())	//如果当天是星期5，跳过3天
	//		sptime2 += TimeSpan(3, 0, 0, 0);
	//	else
	//		sptime2 += TimeSpan(1, 0, 0, 0);//否则跳过1天
	//	return sptime2.ToInstrumentTimeDay();
	//}
	//else if (ktype == ktypeMin5)  //5分钟线
	//{
	//	if (!sptime.FromInstrumentTimeMin(dwDate))
	//		return -1;
	//	if (sptime.GetHour() == 11 && sptime.GetMinute() >= 25)
	//		sptime += TimeSpan(0, 1, 35, 0);
	//	else if (sptime.GetHour() == 14 && sptime.GetMinute() >= 55)
	//		sptime += TimeSpan(0, 18, 35, 0);
	//	else
	//		sptime += TimeSpan(0, 0, 5, 0);
	//	return sptime.ToInstrumentTimeMin();
	//}
	//else if (ktype == ktypeMin15)//15分钟线
	//{
	//	if (!sptime.FromInstrumentTimeMin(dwDate))
	//		return -1;
	//	if (sptime.GetHour() == 11 && sptime.GetMinute() >= 15)
	//		sptime += TimeSpan(0, 1, 45, 0);
	//	else if (sptime.GetHour() == 14 && sptime.GetMinute() >= 45)
	//		sptime += TimeSpan(0, 18, 45, 0);
	//	else
	//		sptime += TimeSpan(0, 0, 15, 0);
	//	return sptime.ToInstrumentTimeMin();
	//}
	//else if (ktype == ktypeMin30)
	//{
	//	if (!sptime.FromInstrumentTimeMin(dwDate))
	//		return -1;
	//	if (sptime.GetHour() == 11 && sptime.GetMinute() >= 0)
	//		sptime += TimeSpan(0, 2, 0, 0);
	//	else if (sptime.GetHour() == 14 && sptime.GetMinute() >= 30)
	//		sptime += TimeSpan(0, 19, 0, 0);
	//	else
	//		sptime += TimeSpan(0, 0, 30, 0);
	//	return sptime.ToInstrumentTimeMin();
	//}
	//else if (ktype == ktypeMin60)
	//{
	//	if (!sptime.FromInstrumentTimeMin(dwDate))
	//		return -1;
	//	if ((sptime.GetHour() == 10 && sptime.GetMinute() >= 30) || sptime.GetHour() == 11)
	//		sptime += TimeSpan(0, 2, 30, 0);
	//	else if (sptime.GetHour() == 14 && sptime.GetMinute() >= 0)
	//		sptime += TimeSpan(0, 19, 30, 0);
	//	else
	//		sptime += TimeSpan(0, 1, 0, 0);
	//	return sptime.ToInstrumentTimeMin();
	//}
	//else
	//{
	//	return -1;
	//}
	return (DWORD)-1;
}

//获得指定时间到当前时间最近的下一个时间;
time_t ExchangeBase::GetTimeTradeLatest(time_t tmTradeFirstToday)
{
	auto	tNow = boost::posix_time::second_clock::local_time();
	auto	tTradeFirstToday=boost::posix_time::from_time_t(tmTradeFirstToday);
	time_t	tmTradeLatest = -1;

	int	nYear = tTradeFirstToday.date().year();
	int nMonth = tTradeFirstToday.date().month();
	int	nDay = tTradeFirstToday.date().day();

	if (tNow > tTradeFirstToday
		&& tNow.date().year() == nYear && tNow.date().month() == nMonth && tNow.date().day() == nDay)
	{
		tmTradeLatest = boost::posix_time::to_time_t(tNow);
		if ((tNow.time_of_day().hours() == 11 && tNow.time_of_day().minutes() >= 30)
			|| tNow.time_of_day().hours() == 12)
		{
			auto ptTradeLast=boost::posix_time::ptime(boost::gregorian::date(nYear, nMonth, nDay), boost::posix_time::time_duration(11, 30, 0));
			tmTradeLatest = boost::posix_time::to_time_t(ptTradeLast);
		}
		else if (tNow.time_of_day().hours() >= 15)
		{
			auto ptTradeLast = boost::posix_time::ptime(boost::gregorian::date(nYear, nMonth, nDay), boost::posix_time::time_duration(15, 0, 0));
			tmTradeLatest = boost::posix_time::to_time_t(ptTradeLast);
		}
	}
	else if (tNow > tTradeFirstToday)
	{
		auto ptTradeLast = boost::posix_time::ptime(boost::gregorian::date(nYear, nMonth, nDay), boost::posix_time::time_duration(15, 0, 0));
		tmTradeLatest = boost::posix_time::to_time_t(ptTradeLast);
	}

	return tmTradeLatest;
}
//
//double ExchangeBase::GetTimeTradeRatioOfOneDay(DateTime tTradeLatestDay, DateTime tNow)
//{
//	DWORD	dwSecOrder = tNow.ToInstrumentTimeSecOrder();
//	if (0 == dwSecOrder)
//		return 1;
//	if (tTradeLatestDay.GetYear() == tNow.GetYear()
//		&& tTradeLatestDay.GetMonth() == tNow.GetMonth()
//		&& tTradeLatestDay.GetDay() == tNow.GetDay())
//		return ((double)dwSecOrder) / GetTradeSecondsOfOneDay();
//	return 1;
//}

time_t ExchangeBase::GetTradeOffsetToTime(int offset, time_t tmDay)
{
	time_t	ret = tmDay - ((tmDay + 8 * 3600) % 86400);
	if (offset >= 0 && offset <= 120)
		return ret + 9 * 3600 + (30 + offset) * 60;
	if (offset > 120 && offset <= 240)
		return ret + 13 * 3600 + (offset - 120) * 60;
	return tmDay;
}

time_t ExchangeBase::GetLatestTradeTime(time_t tmNow)
{
	boost::posix_time::time_duration day_open_time{8,55,0};
	boost::posix_time::time_duration day_close_time{ 15,0,0 };

	boost::posix_time::time_duration night_open_time{ 20,55,0 };
	boost::posix_time::time_duration night_close_time{ 2,30,0 };

	auto tNow = boost::posix_time::from_time_t(tmNow);
	auto tTradeLatest = tNow;
	if (tNow.date().day_of_week() == boost::gregorian::greg_weekday::weekday_enum::Sunday)
	{
		//周日;
		tNow -= boost::gregorian::days(1);
		tTradeLatest = boost::posix_time::ptime(tNow.date(), 
			boost::posix_time::ptime::time_duration_type(15, 0, 0));
	}
	else if (tNow.date().day_of_week() == boost::gregorian::greg_weekday::weekday_enum::Monday)
	{
		//周一;
		if (tNow.time_of_day()<=boost::posix_time::time_duration(8,55,0))
		{
			tNow -= boost::gregorian::days(2);
			tTradeLatest = boost::posix_time::ptime(tNow.date(),
				boost::posix_time::ptime::time_duration_type(15, 0, 0));
		}
	}


	//if ((tNow.GetHour() == 9 && tNow.GetMinute() < 25)
	//	|| tNow.GetHour() < 9)
	//{
	//	tNow -= TimeSpan(1, 0, 0, 0);
	//	tTradeLatest = DateTime(tNow.GetYear(), tNow.GetMonth(), tNow.GetDay(), 15, 0, 0);
	//}
	//else if ((tNow.GetHour() == 11 && tNow.GetMinute() >= 30)
	//	|| tNow.GetHour() == 12)
	//{
	//	tTradeLatest = DateTime(tNow.GetYear(), tNow.GetMonth(), tNow.GetDay(), 11, 30, 0).GetTime();
	//}
	//else if (tNow.GetHour() >= 15)
	//{
	//	tTradeLatest = DateTime(tNow.GetYear(), tNow.GetMonth(), tNow.GetDay(), 15, 0, 0).GetTime();
	//}

	return boost::posix_time::to_time_t(tTradeLatest);
}

bool ExchangeBase::InTradeTime(time_t tm, int nInflateSeconds)
{
	//DateTime	t(tm);

	//// 周六周日;
	//if (7 == t.GetDayOfWeek())
	//	return false;
	//else if (1 == t.GetDayOfWeek())
	//	return false;

	//time_t	day = (t.GetTime() + 8 * 3600) % 86400;//当前时间是一天之中的第多少秒
	//time_t  day0915 = 9 * 3600 + 25 * 60 - nInflateSeconds;
	//time_t  day1130 = 11 * 3600 + 30 * 60 + nInflateSeconds;
	//time_t  day1300 = 13 * 3600 - nInflateSeconds;
	//time_t  day1515 = 15 * 3600 + nInflateSeconds;
	//if (day >= day0915 && day <= day1130)
	//	return true;
	//else if (day >= day1300  && day <= day1515)
	//	return true;
	//return false;
	auto the_trade_time = boost::posix_time::from_time_t(tm);
	if (the_trade_time.date().day_of_week()==boost::gregorian::greg_weekday::weekday_enum::Sunday)
	{
		//周日;
		return false;
	}
	else if (the_trade_time.date().day_of_week() == boost::gregorian::greg_weekday::weekday_enum::Monday)
	{
		//周一;

	}
	return true;
}


//时间计算,
//date只包括年月日,为DosDate格式,date为8位十进制数
//OK
bool ExchangeBase::FromInstrumentTimeDay(DWORD date)
{
	int nHour = 0;
	int	nMinute = 0;
	int nYear = date / 10000;
	int nMonth = (date - nYear * 10000) / 100;
	int nDay = (date - nYear * 10000 - nMonth * 100);
	struct tm atm;
	atm.tm_sec = 0;
	atm.tm_min = nMinute;
	atm.tm_hour = nHour;
	if (nDay < 1 || nDay > 31)
	{
		//日期有误;
		return false;
	}
	atm.tm_mday = nDay;
	if (nMonth < 1 && nMonth > 12)
	{
		//月份有误;
		return false;
	}
	atm.tm_mon = nMonth - 1;        // tm_mon is 0 based
	if (nYear < 1900)
	{
		//年号有误;
		return false;
	}
	atm.tm_year = nYear - 1900;     // tm_year is 1900 based
	atm.tm_isdst = -1;
	time_t	tmt = mktime(&atm);
	if (tmt == -1)		// indicates an illegal input time
	{
		//不能转成time_t
		return false;
	}
	auto retValue=
	boost::posix_time::ptime(
		boost::gregorian::date(nYear, nMonth, nDay),
		boost::posix_time::time_duration(nHour, nMinute, 0));
	return true;
}
//时间计算,
//date包括年月日时分,为DosDate格式,date为12位十进制数(20)1611291617
//OK
bool ExchangeBase::FromInstrumentTimeMin(DWORD date, DWORD year)
{
	int	nYear = year;
	if (-1 == nYear)
		nYear = 1990 + date / 100000000;
	date = date % 100000000;
	int nMonth = date / 1000000;
	int nDay = (date - nMonth * 1000000) / 10000;
	int nHour = (date - nMonth * 1000000 - nDay * 10000) / 100;
	int nMinute = (date - nMonth * 1000000 - nDay * 10000 - nHour * 100);

	struct tm atm;
	atm.tm_sec = 0;
	atm.tm_min = nMinute;
	atm.tm_hour = nHour;
	if (nDay < 1 || nDay > 31)		return false;
	atm.tm_mday = nDay;
	if (nMonth < 1 || nMonth > 12)	return false;
	atm.tm_mon = nMonth - 1;        // tm_mon is 0 based
	if (nYear < 1900)				return false;
	atm.tm_year = nYear - 1900;     // tm_year is 1900 based
	atm.tm_isdst = -1;
	time_t	tmt = mktime(&atm);
	if (tmt == -1)		// indicates an illegal input time
		return false;

	auto retValue =
		boost::posix_time::ptime(
			boost::gregorian::date(nYear, nMonth, nDay),
			boost::posix_time::time_duration(nHour, nMinute, 0));
	return true;
}

bool ExchangeBase::FromInstrumentTime(DWORD dwDate, bool bDayOrMin, DWORD dwYear)
{
	if (bDayOrMin)
		return FromInstrumentTimeDay(dwDate);
	else
		return FromInstrumentTimeMin(dwDate, dwYear);
}

boost::shared_ptr<exchange_t> ExchangeBase::toExchange()
{
	return boost::make_shared<exchange_t>(m_Exchange);
}

////转换成时期
//DWORD DateTime::ToInstrumentTimeDay()
//{
//	if (-1 == GetTime() || GetTime() < 0)
//		return -1;
//	if (0 == GetTime())
//		return 0;
//
//	return (GetYear() * 10000 + GetMonth() * 100 + GetDay());
//}
////转换成分钟
//DWORD DateTime::ToInstrumentTimeMin()
//{
//	if (-1 == GetTime() || GetTime() < 0)
//		return -1;
//	if (0 == GetTime())
//		return 0;
//
//	return ((GetYear() - 1990) * 100000000 + GetMonth() * 1000000 + GetDay() * 10000
//		+ GetHour() * 100 + GetMinute() /*+ (GetSecond()>0 || GetMillsec()>0?1:0) */);
//}
////转换成
//DWORD DateTime::ToInstrumentTimeSecOrder(DWORD dwStockExchange)
//{
//	return ToInstrumentTimeSecOrder(NULL, dwStockExchange);
//	// 	if( -1 == GetTime() || GetTime() < 0 || 0 == GetTime() )
//	// 		return 0;
//	// 
//	// 	if( GetHour() < 9 || (GetHour() == 9 && GetMinute() < 15) )
//	// 		return 0;
//	// 
//	// 	ctp_time tmStart = ctp_time(GetYear(),GetMonth(),GetDay(),9,0,0);//起始时间
//	// 	ctp_time tmEnd	= ctp_time(GetYear(),GetMonth(),GetDay(),15,15,0);//终止时间
//	// 	if( *this < tmStart )
//	// 		return 0;
//	// 	if( *this > tmEnd )
//	// 		return 14400;
//	// 	time_span	tmSpan	=	*this - tmStart;
//	// 	
//	// 	int	nSec	=	tmSpan.GetTotalSeconds();
//	// 	if( nSec >= 0 && nSec <=  4500 )  //<10.15
//	// 		return nSec;
//	// 	if( nSec > 4500 && nSec < 5400 )  //10.15--10.30
//	// 		return 4500;
//	// 	if( nSec >= 5400 && nSec <= 9000 ) //10.30-11.30
//	// 		return nSec-5400;
//	// 	if (nSec >=9000 && nSec <= 16200) //11.30-13.30
//	// 	{
//	// 		return 9000;
//	// 	}
//	// 	if (nSec > 16200)
//	// 	{
//	// 		return nSec-16200;
//	// 	}
//	// 	return 0;
//}