#include "InstrumentConfig.h"


InstrumentConfig::InstrumentConfig(void)
{
	m_iDiffDays=1;
	m_iDiffPercentDays=1;
	m_iScopeDays=1;
	m_iRatioChangeHandDays=1;
	m_iRatioVolumeDays=1;
	m_iRSDays=1;
	m_iYieldAverageDays=1;
}


InstrumentConfig::~InstrumentConfig(void)
{
}

void InstrumentConfig::SetDiffDays( int nDays )
{

}
int InstrumentConfig::GetDiffDays( )
{
	return m_iDiffDays;
}
void InstrumentConfig::SetDiffPercentDays( int nDays )
{

}
int	InstrumentConfig::GetDiffPercentDays( )
{
	return m_iDiffPercentDays;
}
void InstrumentConfig::SetScopeDays( int nDays )
{

}
int	InstrumentConfig::GetScopeDays( )
{
	return m_iScopeDays;
}
void InstrumentConfig::SetRatioChangeHandDays( int nDays )
{
}
int	InstrumentConfig::GetRatioChangeHandDays( )
{
	return m_iRatioChangeHandDays;
}
void InstrumentConfig::SetRatioVolumeDays( int nDays )
{

}

int	InstrumentConfig::GetRatioVolumeDays( )
{
	return m_iRatioVolumeDays;
}
void InstrumentConfig::SetRSDays( int nDays )
{
}
int	InstrumentConfig::GetRSDays( )
{
	return m_iRSDays;
}
void InstrumentConfig::SetYieldAverageDays( int nDays )
{

}
int	InstrumentConfig::GetYieldAverageDays( )
{
	return m_iYieldAverageDays;
}

InstrumentConfig& InstrumentConfig::GetInstance()
{
	static InstrumentConfig g_ConfigInstance;
	return g_ConfigInstance;
}

size_t InstrumentConfig::GetCacheDays()
{
	return m_iCacheDays;
}
