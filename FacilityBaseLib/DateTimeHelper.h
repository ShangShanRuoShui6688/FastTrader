#ifndef _DATETIME_HELPER_H_
#define _DATATIME_HELPER_H_

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/noncopyable.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/make_shared.hpp>

#include "DataTypes.h"
#include "../Log/logging.h"
#include "Container.h"

class DateTimeHelper:public boost::noncopyable
{
public:
	static DateTimeHelper& GetInstance()
	{
		static DateTimeHelper gDateTimeHelper;
		return gDateTimeHelper;
	}

	std::vector<std::pair<boost::shared_ptr<InstStatus>, boost::shared_ptr<InstStatus> > > 
		GetTradingTimeSections(const std::string& InstrumentID)
	{
		std::vector<std::pair<boost::shared_ptr<InstStatus>, boost::shared_ptr<InstStatus> > > TradingTimeSections;
		std::vector<boost::shared_ptr<InstStatus> >* pStatusTable = nullptr;
		auto isIter = m_LocalStatus.find(InstrumentID);
		if (isIter == m_LocalStatus.end())
		{
			//获取品种;
			InstrumentContainer& container = InstrumentContainer::get_instance();
			InstrumentInfo info;
			if(container.get_info_by_id(InstrumentID,&info))
			{
				isIter = m_LocalStatus.find(info.ProductID);
				if (isIter!=m_LocalStatus.end())
				{
					pStatusTable=&isIter->second;
				}
			}
		}
		else
		{
			pStatusTable = &isIter->second;
		}
		if (!pStatusTable)
		{
			return TradingTimeSections;
		}
		auto& statusTable = *pStatusTable;
		std::pair<boost::shared_ptr<InstStatus>, boost::shared_ptr<InstStatus> > tradingPairTime;
		for (size_t i = 0; i < statusTable.size(); ++i)
		{
			if (statusTable[i]->InstrumentStatus == IS_Continous)
			{
				tradingPairTime.first = statusTable[i];
			}
			else
			{
				if (tradingPairTime.first && tradingPairTime.first->InstrumentStatus == IS_Continous)
				{
					if ((statusTable[i]->InstrumentStatus == IS_NoTrading || statusTable[i]->InstrumentStatus == IS_Closed))
					{
						tradingPairTime.second = statusTable[i];
					}
					else
					{
						auto assumeStatus = boost::make_shared<InstStatus>(*statusTable[i]);
						assumeStatus->EnterTime = "01:00:00";
						assumeStatus->InstrumentStatus = IS_NoTrading;
						tradingPairTime.second = assumeStatus;
					}
					TradingTimeSections.push_back(tradingPairTime);
					tradingPairTime = std::pair<boost::shared_ptr<InstStatus>, boost::shared_ptr<InstStatus> >();
				}
			}
		}
		if (tradingPairTime.first && tradingPairTime.first->InstrumentStatus == IS_Continous
			&& tradingPairTime.second == nullptr)
		{
			//空出来一个;
			auto stIter = std::find_if(statusTable.begin(), statusTable.end(),
				[](boost::shared_ptr<InstStatus> pStatus)
			{
				return pStatus->EnterTime <= "02:30:00" && 
					(pStatus->InstrumentStatus ==  IS_NoTrading || pStatus->InstrumentStatus == IS_Closed) ;
			});
			if (stIter!=statusTable.end())
			{
				tradingPairTime.second = *stIter;
			}
			else
			{
				auto assumeStatus = boost::make_shared<InstStatus>(*tradingPairTime.first);
				assumeStatus->EnterTime = "01:00:00";
				assumeStatus->InstrumentStatus = IS_NoTrading;
				tradingPairTime.second = assumeStatus;
			}
			TradingTimeSections.push_back(tradingPairTime);
			tradingPairTime = std::pair<boost::shared_ptr<InstStatus>, boost::shared_ptr<InstStatus> >();
		}
		//将开盘时间放到最前面去;
		auto stIter = std::find_if(TradingTimeSections.begin(), TradingTimeSections.end(),
			[this,InstrumentID](const std::vector<std::pair<boost::shared_ptr<InstStatus>, boost::shared_ptr<InstStatus> > >::value_type& pStatusPair)
		{
			return pStatusPair.first->EnterTime == GetOpenTime(InstrumentID);
		});
		if (stIter != TradingTimeSections.end())
		{
			auto cpTradingSecs = *stIter;
			TradingTimeSections.erase(stIter);
			TradingTimeSections.insert(TradingTimeSections.begin(), cpTradingSecs);
		}
		return TradingTimeSections;
	}

	int ToInstrumentTimeSecOrder(const std::string& InstrumentID, time_t tSec)
	{
		boost::posix_time::ptime ptSec = boost::posix_time::from_time_t(tSec);
		auto ptDuraction = ptSec.time_of_day();

		auto ptOpen = boost::posix_time::duration_from_string(GetOpenTime(InstrumentID));
		auto ptClose = boost::posix_time::duration_from_string(GetCloseTime(InstrumentID));

		if (ptOpen<ptClose)
		{
			//没有夜盘;
			if (ptDuraction<=ptOpen)
			{
				return 0;
			}
			else if (ptDuraction>=ptClose)
			{
				return GetTradeSecondOfOneDay(InstrumentID);
			}
		}
		else
		{
			if (ptDuraction >= ptClose && ptDuraction < ptOpen)
			{
				//15.00到21.0
				return GetTradeSecondOfOneDay(InstrumentID);
			}
		}

		std::vector<std::pair<boost::shared_ptr<InstStatus>, boost::shared_ptr<InstStatus> > > TradingTimeSections
			= GetTradingTimeSections(InstrumentID);
		int tradingSeconds = 0;
		for (size_t i = 0; i < TradingTimeSections.size(); ++i)
		{
			auto ptEnter = boost::posix_time::duration_from_string(TradingTimeSections[i].first->EnterTime);
			auto ptLeave = boost::posix_time::duration_from_string(TradingTimeSections[i].second->EnterTime);

			if (ptLeave < ptEnter )
			{
				//时间过了一夜;
				if (ptDuraction <= ptLeave)
				{
					//0.0到1.0或2.30
					tradingSeconds += (boost::posix_time::time_duration(24, 0, 0)- ptEnter).total_seconds();
					tradingSeconds += (ptLeave - ptDuraction).total_seconds();
					break;

				}else if (ptDuraction >= ptEnter)
				{
					tradingSeconds += (ptDuraction - ptEnter).total_seconds();
					break;
				}
				else if (ptDuraction > ptLeave)
				{
					tradingSeconds += (boost::posix_time::time_duration(24, 0, 0) - ptEnter).total_seconds();
					tradingSeconds += ptLeave.total_seconds();
				}
			}
			else
			{
				if (ptDuraction < ptEnter)
				{
					if (ptOpen == ptEnter && ptOpen > ptClose )
					{
						//有夜盘的情况;
						tradingSeconds += (ptLeave - ptEnter).total_seconds();
					}
						
					continue;
				}
				else if (ptDuraction >= ptEnter && ptDuraction <= ptLeave)
				{
					tradingSeconds += (ptDuraction - ptEnter).total_seconds();
					break;
				}
				else
				{
					tradingSeconds += (ptLeave - ptEnter).total_seconds();
				}
			}
		}
		return tradingSeconds;
	
	}

	std::string GetCloseTime(const std::string& InstrumentID)
	{
		std::vector<boost::shared_ptr<InstStatus> >* pStatusTable = nullptr;
		auto isIter = m_LocalStatus.find(InstrumentID);
		if (isIter == m_LocalStatus.end())
		{
			//获取品种;
			InstrumentContainer& container = InstrumentContainer::get_instance();
			InstrumentInfo info;
			if (container.get_info_by_id(InstrumentID, &info))
			{
				isIter = m_LocalStatus.find(info.ProductID);
				if (isIter != m_LocalStatus.end())
				{
					pStatusTable = &isIter->second;
				}
			}
		}
		else
		{
			pStatusTable = &isIter->second;
		}
		if (pStatusTable)
		{
			auto& statusTable = *pStatusTable;
			for (size_t i = 0; i < statusTable.size(); ++i)
			{
				if (statusTable[i]->InstrumentStatus == IS_Closed)
				{
					return statusTable[i]->EnterTime;
				}
			}
		}
		
		return "15:00:00";
	}

	std::pair<std::string, std::string> GetAuctionMatchTime(const std::string&  InstrumentID)
	{
		std::vector<boost::shared_ptr<InstStatus> >* pStatusTable = nullptr;
		auto isIter = m_LocalStatus.find(InstrumentID);
		if (isIter == m_LocalStatus.end())
		{
			//获取品种;
			InstrumentContainer& container = InstrumentContainer::get_instance();
			InstrumentInfo info;
			if (container.get_info_by_id(InstrumentID, &info))
			{
				isIter = m_LocalStatus.find(info.ProductID);
				if (isIter != m_LocalStatus.end())
				{
					pStatusTable = &isIter->second;
				}
			}
		}
		else
		{
			pStatusTable = &isIter->second;
		}
		if (pStatusTable)
		{
			auto& statusTable = *pStatusTable;
			boost::shared_ptr<InstStatus> lastContinous;//前一次的连续交易状态;
			std::pair<std::string, std::string> retPair;
			for (size_t i = 0; i < statusTable.size(); ++i)
			{
				if (statusTable[i]->InstrumentStatus == IS_AuctionMatch)
				{
					retPair.first = statusTable[i]->EnterTime;
				}
				else
				{
					if (!retPair.first.empty() && statusTable[i]->InstrumentStatus == IS_Continous)
					{
						//开盘前为集合竞价撮合;
						retPair.second = statusTable[i]->EnterTime;
						return retPair;
					}
				}
			}
		}
		return std::make_pair("20:59:00","21:00:00");
	}

	std::string GetOpenTime(const std::string& InstrumentID)
	{
		std::vector<boost::shared_ptr<InstStatus> >* pStatusTable = nullptr;
		auto isIter = m_LocalStatus.find(InstrumentID);
		if (isIter == m_LocalStatus.end())
		{
			//获取品种;
			InstrumentContainer& container = InstrumentContainer::get_instance();
			InstrumentInfo info;
			if (container.get_info_by_id(InstrumentID, &info))
			{
				isIter = m_LocalStatus.find(info.ProductID);
				if (isIter != m_LocalStatus.end())
				{
					pStatusTable = &isIter->second;
				}
			}
		}
		else
		{
			pStatusTable = &isIter->second;
		}
		if (!pStatusTable)
		{
			return "21:00:00";
		}
		auto& statusTable = *pStatusTable;
		boost::shared_ptr<InstStatus> lastContinous;//前一次的连续交易状态;
		for (size_t i = 0; i < statusTable.size(); ++i)
		{
			if (statusTable[i]->InstrumentStatus == IS_AuctionMatch)
			{
				lastContinous = statusTable[i];
			}
			else
			{
				if (lastContinous && lastContinous->InstrumentStatus == IS_AuctionMatch)
				{
					//开盘前为集合竞价撮合;
					return statusTable[i]->EnterTime;
				}
			}
		}
		if (!lastContinous)
		{
			auto stIter = std::find_if(statusTable.begin(), statusTable.end(), 
			[](boost::shared_ptr<InstStatus> pStatus)
			{ 
				return pStatus->EnterTime == "21:00:00"
					|| pStatus->EnterTime == "09:30:00"
					;
			});
			if ( stIter != statusTable.end())
			{
				return (*stIter)->EnterTime;
			}
			return "09:00:00";
		}
		return "21:00:00";
	}

	//获取合约一天交总的交易秒数;
	int GetTradeSecondOfOneDay(const std::string& InstrumentID)
	{
		auto tsIter = m_TradeSecondsOfOneDay.find(InstrumentID);
		if (tsIter==m_TradeSecondsOfOneDay.end())
		{
			std::vector<std::pair<boost::shared_ptr<InstStatus>, boost::shared_ptr<InstStatus> > > TradingTimeSections
				= GetTradingTimeSections(InstrumentID);
			int tradingSeconds = 0;
			for (size_t i = 0; i < TradingTimeSections.size(); ++i)
			{
				auto ptEnter = boost::posix_time::duration_from_string(TradingTimeSections[i].first->EnterTime);
				auto ptLeave = boost::posix_time::duration_from_string(TradingTimeSections[i].second->EnterTime);

				if (ptLeave < ptEnter)
				{
					//时间过了一夜;
					tradingSeconds += (boost::posix_time::time_duration(24, 0, 0) - ptEnter).total_seconds();
					tradingSeconds += ptLeave.total_seconds();
				}
				else
				{
					tradingSeconds += (ptLeave - ptEnter).total_seconds();
				}
			}
			m_TradeSecondsOfOneDay[InstrumentID] = tradingSeconds;
			return tradingSeconds;
		}
		else
		{
			return tsIter->second;
		}
	}

	DateTimeHelper()
	{
		try
		{
			boost::property_tree::ptree ptInstrument;
			boost::property_tree::ini_parser::read_ini("InstrumentStatus.ini", ptInstrument);
			for (auto instIter = ptInstrument.begin(); instIter != ptInstrument.end(); ++instIter)
			{
				std::string InstrumentID = instIter->first;
				auto ptStatus = instIter->second;
				std::vector<boost::shared_ptr<InstStatus> > statusTable;
				for (auto statusIter = ptStatus.begin(); statusIter != ptStatus.end(); ++statusIter)
				{
					boost::shared_ptr<InstStatus> pStatus = boost::make_shared<InstStatus>();
					pStatus->InstrumentID = InstrumentID;
					pStatus->InstrumentStatus = (EnumInstStatus)std::stoi(statusIter->second.data());
					pStatus->EnterTime = statusIter->first;
					statusTable.push_back(pStatus);
				}
				std::sort(statusTable.begin(), statusTable.end(),
					[](const boost::shared_ptr<InstStatus>& is1, const boost::shared_ptr<InstStatus>& is2){
					return is1->EnterTime < is2->EnterTime;
				});
				m_LocalStatus.insert(std::make_pair(InstrumentID, statusTable));
			}
		}
		catch (boost::property_tree::ini_parser_error& e1)
		{
			LOGDEBUG("InstrumentStatus.ini parser error:{}", e1.what());
		}
		catch (std::exception& e1)
		{
			LOGDEBUG("InstrumentStatus.ini read error:{}", e1.what());
		}
	}
protected:
	std::map < std::string, std::vector<boost::shared_ptr<InstStatus> > > m_LocalStatus;
	std::map < std::string, int > m_TradeSecondsOfOneDay;
};

inline uint32_t ToInstrumentTimeDayOrMin(const boost::posix_time::ptime& ptTime,bool isDayOrMin=false)
{
	auto day = ptTime.date();
	auto dtime = ptTime.time_of_day();

	auto dt = ((day.year() - 1990) * 100000000 + day.month() * 1000000 + day.day() * 10000);
	if (!isDayOrMin)
	{
		dt += (dtime.hours() * 100 + dtime.minutes() /*+ (GetSecond()>0 || GetMillsec()>0?1:0) */);
	}
	return dt;
}

inline bool FromInstrumentTimeDayOrMin(boost::posix_time::ptime&  pt,uint32_t date, bool isDayOrMin = false)
{
	if (isDayOrMin)
	{
		int nHour = 0;
		int	nMinute = 0;
		int nYear = date / 10000;
		int nMonth = (date - nYear * 10000) / 100;
		int nDay = (date - nYear * 10000 - nMonth * 100);
		pt = boost::posix_time::ptime(boost::gregorian::date(nYear, nMonth, nDay),
			boost::posix_time::ptime::time_duration_type(nHour, nMinute, 0));
		return true;
	}
	else
	{
		int	nYear = 1990 + date / 100000000;
		date = date % 100000000;
		int nMonth = date / 1000000;
		int nDay = (date - nMonth * 1000000) / 10000;
		int nHour = (date - nMonth * 1000000 - nDay * 10000) / 100;
		int nMinute = (date - nMonth * 1000000 - nDay * 10000 - nHour * 100);

		pt = boost::posix_time::ptime(boost::gregorian::date(nYear, nMonth, nDay),
			boost::posix_time::ptime::time_duration_type(nHour, nMinute, 0));
		return true;
	}
	
}

#endif