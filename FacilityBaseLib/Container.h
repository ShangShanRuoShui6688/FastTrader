#ifndef _CONTAINER_H_
#define _CONTAINER_H_

#include "InstrumentInfo.h"
#include <vector>
#include <map>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include "Tick.h"

#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>


typedef	struct	variant_savevalue_t	{
	UINT	nVariantID;
	double	Value;
	bool	bNoValue;
}VARIANT_SAVEVALUE;

/***
	合约信息数组类。;
	有一个合约信息数组类的全局对象instrument_container::get_instance();保存所有股票的信息;
	用于管理所有的合约;
*/

class FACILITY_API InstrumentContainer
{
public:
	InstrumentContainer();
	virtual	~InstrumentContainer();

	bool lock();
	bool unlock();
	//标准库接口;
	std::size_t size() const;
	std::size_t size();
	void push_back(InstrumentInfo& val);
	void clear(){m_instruments.clear();}
	InstrumentInfo& operator [](std::size_t t);
	//排序函数;
	static int SortFunction(const void *s1,const void *s2);
	//设置如何排序;
	static int StockInfoCompareFunc( const void *s1,const void *s2, InstrumentContainer * pContainer, int nSortVariantID );

	//访问函数;
	virtual	int	add( InstrumentInfo &newElement );
	virtual	void resize(size_t nNewSize, int nGrowBy = -1);

	// attributes
	enum StockTypes {
		typeNone		=	0x00,
		typeMin			=	0x01,
		typeIndex		=	0x01,
		typeStrategy	=	0x02,
		typeGroup		=	0x03,
		typeDomain		=	0x04,
		typeAll			=	0x05,
		typeA			=	0x06,
		typeBond		=	0x07,
		typeFund		=	0x08,
		typeClassShaa	=	0x09,
		typeClassShab	=	0x0A,
		typeClassSzna	=	0x0B,
		typeClassSznb	=	0x0C,
		typeClassShabond=	0x0D,
		typeClassSznbond=	0x0E,
		typeClassMsmall	=	0x0F,
		typeRight		=	0x10,
		typeMax			=	0x10
	};
	// operations
	bool	GetCurrentType( int *pType, std::string * pDomain, DWORD *pdwDate );
	bool	GetPioneerTechDate( DWORD * pdwDate );
	bool	GetLatestTechDate( DWORD * pdwDate );
	bool	GetTechDateArray( std::vector<DWORD> & dwArray);
	//;
	bool	ReRetrieveFromStatic(std::vector<std::string>& instruments, bool bUpToDate=false );
	bool	RetrieveFromStatic(int nType, const std::string& lpszDomain,std::vector<std::string>& instruments, DWORD dwDate );
	bool	RetrieveSpecify( std::vector<std::string> & astr );

	bool	GetInstrumentInfo( const char * szCode, InstrumentInfo * pInfo, int * pid = NULL );
	int     GetInstrumentIndex(InstrumentInfo * pInfo);

	bool	SetCurrentInstrument( size_t nInstrumentIndex );
	bool	SetCurrentInstrument( const char * szCode );
	bool	GetCurrentInstrument( InstrumentInfo * pInfo);
	bool	GetPrevInstrument( InstrumentInfo * pInfo );
	bool	GetNextInstrument( InstrumentInfo * pInfo );

	InstrumentInfo & get_info_by_id(const std::string& szCode,const std::string& exchange_id);
	bool	get_info_by_id(const std::string& szCode, InstrumentInfo* pInfo, int * pid = nullptr);
	InstrumentInfo & get_info_by_id( size_t nID );
	InstrumentInfo & get_info_by_id_sort( size_t nID );
	InstrumentInfo & average();
	InstrumentInfo & weight_average();
	
	void	OnDataChanged( );
	bool	SetMap( );
	bool	SetAverage( PROGRESS_CALLBACK fnCallback = NULL, void *cookie = NULL, int nProgStart = 0, int nProgEnd = STKLIB_MAX_PROGRESS );
	bool	Clear( );
	void	CopyData( InstrumentContainer & src );

	char*   GetExhcnageIDByInstrumentID(const char* insID);

	static	InstrumentContainer * m_pSortContainer;
	static InstrumentContainer& get_instance();
	UINT	m_nSortVariantID;
	bool	m_bSortAscend;
	InstrumentInfo& at(size_t idx);
	bool	Sort( int nSortVariantID, bool bAscend );
	void	SetSortID( std::vector<DWORD> & auidsort );
	bool	GetMultiSortIDArray( std::vector<DWORD> & adwSortID, int lStockType, UINT nSLH, bool bAsc, size_t nCount );

	bool	GetVariantSaveValue( double *pValue, UINT nVariantID, InstrumentInfo &info, bool *pNoValue );
	void	SetVariantSaveValue( double Value, UINT nVariantID, InstrumentInfo &info, bool bNoValue );
	//取得名称;
	bool	get_names(std::vector<std::string>& names,bool bIdOrNames=true);
	bool    get_names_by_exchange(std::vector<std::string>& names, const std::string& exchange, bool bIdOrNames = true);
	bool    get_names_by_product(std::vector<std::string>& names, const std::string& product, bool bIdOrNames = true);
	
	//更新合约状态;
	bool	update_status(boost::shared_ptr<InstStatus> instStatus);
	static bool update(InstrumentContainer &container, boost::shared_ptr<BASEDATA> pBasedata, bool bAddIfNotExist);
	static bool update(InstrumentContainer &container, Instrument * pBasedata, bool bAddIfNotExist, Instrument* pBasedataLast = nullptr);
	static bool update( InstrumentContainer &container,const Tick * pReport, bool bAddIfNotExist, Tick * pReportLast=NULL );
	static bool update(InstrumentContainer &container, const std::string& InstrumentID, KdataContainer& kdata);
	void    SetType(int nType);
	void    SetDate(DWORD dwDate);
protected:
	void	ClearVariantSaveValue();
protected:
	int			m_nType;
	DWORD		m_dwDate;
	std::string	m_strDomain;
	size_t			m_nCurrentIndex;
	// 当m_nCurrentStock==-1时，即当前股票不在Container数组之内，则记录其StockCode;
	std::string	m_strCurrentStockCode;
	//平均值;
 	InstrumentInfo	m_infoAverage;
	//加权平均值;
 	InstrumentInfo	m_infoWeightAverage;
	//空值;
 	InstrumentInfo	m_infoNull;

	std::map<std::string,intptr_t >	m_map;
	std::vector<void* >			m_aptrSaveValueArray;
	//合约编号排序后的数组;
	std::vector<DWORD>		m_auidSort;
	//同步变量;
    boost::mutex	m_mutex;
	//实现;
	std::vector<InstrumentInfo> m_instruments;
	//合约的最新更新时间;
	boost::posix_time::ptime m_LastTime;
};


#endif
