﻿#ifndef _IDATA_STORE_H_
#define _IDATA_STORE_H_
#include "DataStoreLib.h"
#include "DataFormat.h"
#include "../FacilityBaseLib/InstrumentInfo.h"
#include "../FacilityBaseLib/InstrumentData.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/ExchangeContainer.h"
#include "DataTypes.h"
#include <boost/shared_ptr.hpp>
#include <vector>



/***
	读取K线数据的通用接口，各种格式的读取K线数据类必须从此类继承
*/

class	InstrumentContainer;
class   InstrumentData;
class   InstrumentInfo;

enum DataSourceType
{
	dstTypeNone=0,
	dstTypeNet=1,
	dstTypeFile=2,
	dstTypeDB=4
};

class DATASTORE_API IDataStore
{
public:
	enum DBTypes {	// 数据格式类型，目前只支持typeSelfDB（自有格式）和typeQianlong（钱龙格式）;
		dbtypeUnknown		=	0x01,
		dbtypeSelfDB		=	0x02,//支持;
		dbtypeQianlong		=	0x03,
		dbtypeShenglong		=	0x04,
		dbtypeODBC          =   0x05,//ODBC mysql支持;
		dbtypeMongoDB		=	0x06,
		dbtypeMySql			=   0x07,
		dbtypeSqlite3		=	0x08,//支持;
		dbtypeRedis			=   0x09,
		dbtypeHdf5			=	0x0A,
	};
	IDataStore();
    virtual ~IDataStore();
	static	int	GetSupportedDataType ( std::vector<std::string>& data_types );
	// 给出根目录和类型，新建一个对象;
	static	boost::shared_ptr<IDataStore> CreateStore( const char * rootpath, int nDBType = dbtypeUnknown );	
	//必须实现的接口;
public:
	virtual	int IsOK( ) { return m_bIsOK; }
	virtual	const char * GetRootPath( )	// 得到当前对象的根目录
	{return m_szRootPath;}
	virtual int Init(const std::string& initStr)=0;   //初始化
    virtual	int	GetDBTypeInfo( std::string& dbtype_name)	=	0;		// 得到当前对象的数据类型
	virtual	int	GetMaxNumber()	=	0;	// 得到股票数量;
	virtual	int	LoadCodetable( InstrumentContainer & container )	=	0;	// 读取所有股票的信息
	virtual int StoreExchange(ExchangeContainer& exchanges);
	virtual int LoadExchange(ExchangeContainer& exchanges);
	virtual	int	StoreCodetable( InstrumentContainer & container )	=	0;	// 保存代码表
	virtual	int	LoadKDataCache( InstrumentContainer & container, PROGRESS_CALLBACK fnCallback, void *cookie, int nProgStart, int nProgEnd )	=	0;	// 读取所有股票的最近日线数据缓冲
	virtual	int	LoadBasetable( InstrumentContainer & container )	=	0;	// 读取某一股票的财务资料表，包括每股收益，每股净资产等，见CBaseData
	virtual	int	StoreBasetable( InstrumentContainer & container )	=	0;	// 保存某一股票的财务资料表
	virtual	int	LoadBaseText( InstrumentData *pstock )	=	0;					// 读取某一股票的基本资料文本
	virtual	int	LoadKData(InstrumentData *pstock, int nKType, int nLength = DEFAULT_LOAD_MIN1_COUNT) = 0;			// 读取某一股票的某个周期的K线数据
	virtual	int	LoadDRData( InstrumentData *pstock )	=	0;					// 读取某一股票的除权除息资料
	virtual	int StoreDRData( InstrumentData *pstock )	=	0;					// 保存某一股票的除权除息资料
	virtual int	LoadReport(InstrumentData *pstock, int nLength = DEFAULT_LOAD_TICK_COUNT) = 0;					// 读取某一股票的行情刷新数据
	virtual int	LoadMinute(InstrumentData *pstock, int nLength = DEFAULT_LOAD_MIN1_COUNT) = 0;					// 读取某一股票的行情分时数据
	virtual int	LoadOutline( InstrumentData *pstock )	=	0;					// 读取某一股票的行情额外数据
	virtual int	StoreReport( std::vector<boost::shared_ptr<Tick> > pReport, bool bBigTrade )	=	0;		// 保存行情刷新数据
	virtual int	StoreMinute( std::vector<boost::shared_ptr<MINUTE> > pMinute )	=	0;		// 保存行情分时数据
	virtual int	StoreOutline( std::vector<boost::shared_ptr<OUTLINE> > pOutline )	=	0;	// 保存行情分时数据
	virtual	int	StoreKData( KdataContainer & kdata, bool bOverwrite = false )	=	0;			// 安装K线数据
	//选择实现的接口;
	int PrepareData(boost::shared_ptr<InstrumentData> pInstrument, int enKtype, int period = ktypeDay, int nLength = DEFAULT_LOAD_MIN1_COUNT, bool bReload = false);
public:
	virtual	bool GetFileName( std::string &sFileName, int nDataType,
				InstrumentInfo * pInfo = NULL, int nKType = ktypeDay )	=	0;		// 得到某种数据的文件名称;
	static boost::posix_time::ptime	GetTimeInitial();		// 得到初始数据日期;
	bool	GetTimeLocalRange(boost::posix_time::ptime *ptmLatest, boost::posix_time::ptime * ptmPioneer, boost::posix_time::ptime * ptmInitial);	// 得到本地数据日期区间;
	bool	GetNeedDownloadRange(InstrumentInfo &info, boost::posix_time::ptime tmBegin, 
		boost::posix_time::ptime tmEnd, boost::posix_time::ptime &tmDLBegin, boost::posix_time::ptime &tmDLEnd);	// 得到需要下载的数据日期区间;
protected:
	bool	m_bIsOK;
	char	m_szRootPath[1024];
};
class DATASTORE_API IDataInstaller
{
public:
	virtual	int	InstallCodetbl( const char * filename, const char *orgname )	=	0;	// 安装下载的代码表
	virtual	int	InstallCodetblBlock( const char * filename, const char *orgname )	=	0;	// 安装下载的板块表
	virtual	int	InstallKData( KdataContainer & kdata, bool bOverwrite = false )	=	0;			// 安装K线数据
	virtual	int	InstallBasetable( const char * filename, const char *orgname )	=	0;	// 安装财务数据
	virtual	int InstallBaseText( const char * filename, const char *orgname )	=	0;	// 安装下载的基本资料数据，一只股票一个文件
	virtual	int InstallBaseText( const char * buffer, int nLen, const char *orgname )	=	0;	// 安装基本资料数据
	virtual	int InstallNewsText( const char * filename, const char *orgname )	=	0;	// 安装新闻数据文件
	virtual	int InstallNewsText( const char * buffer, int nLen, const char *orgname )	=	0;	// 安装新闻数据
};
#endif
