#include "DataStoreManager.h"
#include "../FacilityBaseLib/Container.h"
#include "../FacilityBaseLib/Instrument.h"
//#include "../ServiceLib/platform_server.h"
//#include "../ServiceLib/OperatorManager.h"
#include "InstrumentStatus.h"
//#include "../ServiceLib/Broker.h"
#include <iostream>
DataStoreManager::DataStoreManager()
{

}

DataStoreManager::~DataStoreManager()
{
	//如果缓存里还有数据，也要存盘;
// 	if (!m_Minutes.empty())
// 	{
// 		std::map<std::string,IDataStore* >::iterator it=m_mapStores.begin();
// 		while(it!=m_mapStores.end())
// 		{
// 			if (m_Minutes.size()>0)
// 			{
// 				it->second->StoreMinute(&m_Minutes[0],m_Minutes.size());
// 			}
// 			++it;
// 		}
// 		m_Minutes.clear();
// 	}

	Clear();
}

bool DataStoreManager::Add( IDataStore* store )
{
	if (NULL == store)
	{
		return false;
	}
	std::string key(store->GetRootPath());
	std::map<std::string,IDataStore* >::iterator it=m_mapStores.find(key);
	if (m_mapStores.end()==it)
	{
		m_mapStores.insert(make_pair(key,store));
	}
	return true;
}

bool DataStoreManager::Remove( const std::string& key )
{
	std::map<std::string,IDataStore* >::iterator it=m_mapStores.find(key);
	if (m_mapStores.end()!=it)
	{
		delete it->second;
		it->second=NULL;
		m_mapStores.erase(it);
		return true;
	}
	return false;
}

bool DataStoreManager::Remove( IDataStore* store )
{
	return true;
}

void DataStoreManager::Clear()
{
	std::map<std::string,IDataStore* >::iterator it=m_mapStores.begin();
	while (m_mapStores.end()!=it)
	{
		delete it->second;
		it->second=NULL;
		++it;
	}
	m_mapStores.clear();
}

IDataStore* DataStoreManager::Find( const string& key )
{
	std::map<std::string,IDataStore* >::iterator it=m_mapStores.find(key);
	if (m_mapStores.end()!=it)
	{
		return it->second;
	}
	return NULL;
}

DataStoreManager* DataStoreManager::gInstance=NULL;


bool DataStoreManager::SetDefault( IDataStore* default_data_store )
{
	std::map<std::string,IDataStore* >::iterator it=m_mapStores.find("default");
	if (it==m_mapStores.end())
	{
		m_mapStores.insert(make_pair("default",default_data_store));
	}
	else
	{
		IDataStore* pIt=it->second;
		m_mapStores[it->first]=default_data_store;
		delete pIt;
		pIt=NULL;
	}
	return true;
}

IDataStore* DataStoreManager::GetDefault()
{
	if (m_mapStores.size()<=0)
	{
		IDataStore* defStore=IDataStore::CreateStore("file://./data",IDataStore::dbtypeSelfDB);
		if (NULL==defStore)
		{
			return NULL;
		}
		Add(defStore);
		return defStore;
	}
	std::map<string,IDataStore* >::iterator it=m_mapStores.find("default");
	if (m_mapStores.end()!=it)
	{
		return it->second;
	}
	else
	{
		it=m_mapStores.begin();
		//bool bFindSelfDB=false,bFindDatabase=false;
		while(it!=m_mapStores.end())
		{
			std::string dbTypeName;
			if (it->second->GetDBTypeInfo(dbTypeName)==IDataStore::dbtypeSelfDB)
			{
				return it->second;
			}
			++it;
		}
		return m_mapStores.begin()->second;
	}
	return NULL;
}

DataStoreManager* DataStoreManager::GetInstance()
{
	if (NULL==gInstance)
	{
		gInstance=new DataStoreManager();
		gInstance->start();
	}
	return gInstance;
}

int DataStoreManager::post_event( WPARAM wParam,LPARAM lParam )
{
	DEBUG_METHOD();
	//DEBUG_PRINTF("事件:%d,对象:%08x,类型:",wParam,lParam);
	//DEBUG_VALUE_AND_TYPE_OF(this);
	//static user_info loginedInvestor;
	switch(wParam)
	{
	case LoginFinished:
		{
			//user_info* investor=reinterpret_cast<user_info*>(lParam);
			//if (investor)
			//{
			//	loginedInvestor=*investor;
			//}
			//platform_server* pPlatormServer=platform_manager::get_instance().FindByUser(investor->user_id,investor->broker_id);
			//if (NULL==pPlatormServer)
			//{
			//	return -1;
			//}
		}
		break;
	case RtnInstrumentStatus:
		{
			//如果收盘了,应该把缓存里的数据放到数据库里去;
			InstStatus* pInstrumentStatus=reinterpret_cast<InstStatus*>(lParam);
			if (pInstrumentStatus)
			{
				//DEBUG_METHOD();
				//DEBUG_MESSAGE("合约:"<<pInstrumentStatus->InstrumentID<<",状态:"<<
				//	FieldInterceptor::get_instrument_status(pInstrumentStatus->InstStatus)<<",时间:"<<pInstrumentStatus->EnterTime);
			}
			break;
		}
	case RtnDepthMarketData:
		{
			PCOMMPACKET	pCommPacket	= reinterpret_cast<PCOMMPACKET>(lParam);
			Tick reportLast;
			if (!instrument_container::update(instrument_container::get_instance(), 
				pCommPacket->m_pReport, true, &reportLast))
				return -1;
			
			if (pCommPacket->m_dwDataType==instrument::dataReport)
			{
				std::unique_lock<std::mutex> lock(m_mutex_report);
				m_qReport.push_back(*(pCommPacket->m_pReport));
			}

			//转成分钟数据;
			MINUTE	minute;
			memset(&minute,0,sizeof(minute));
			minute.Type=1;
// 			DEBUG_MESSAGE("^-^数据库接收到行情^-^："<<pCommPacket->m_pReport->szCode<<","<<
// 				ctp_time(pCommPacket->m_pReport->TradingTime).to_string()<<"."<<pCommPacket->m_pReport->TradingMillisec);

			if( convert_REPORT_to_MINUTE( pCommPacket->m_pReport, &minute ) )
			{	
				//分钟数据的前一次值;
				std::unique_lock<std::mutex> lock(m_mutex_minute);
				
				std::map<std::string,MINUTE>::iterator miniter=m_mapPreMinuteCache.find(minute.szCode);
				if (miniter==m_mapPreMinuteCache.end())
				{
					std::pair<std::map<std::string,MINUTE>::iterator,bool> ret=
						m_mapPreMinuteCache.insert(std::pair<std::string,MINUTE>(minute.szCode,minute));
					miniter=ret.first;
					//说明这个合约以前还没来过数据，所以第一次来数据时，要加入到队列里去;
					DEBUG_MESSAGE("^-^数据库分钟数据1^-^："<<minute.szCode<<","<<
						DateTime(miniter->second.TradingTime).to_string()<<"."<<miniter->second.TradingMillisec);
				}
				else
				{
					if (miniter->second.HighestPrice<minute.HighestPrice)
					{
						miniter->second.HighestPrice=minute.HighestPrice;
					}
					if (miniter->second.LowestPrice>minute.LowestPrice)
					{
						miniter->second.LowestPrice=minute.LowestPrice;
					}
				}
				if (miniter->second.TradingTime != minute.TradingTime)
				{
					//上一分钟已经过去,数据放入队列;
					DEBUG_MESSAGE("^-^数据库分钟数据59^-^："<<minute.szCode<<","<<
						DateTime(miniter->second.TradingTime).to_string()<<"."<<miniter->second.TradingMillisec);
					m_qMinute.push(miniter->second);
					push_event(instrument::dataMinute);
					miniter->second=minute;
				}
			}
		}
		break;
	case instrument::dataBasetable:
		{
			//platform_server* pPlatormServer=platform_manager::get_instance().FindByUser(
			//	loginedInvestor.user_id,loginedInvestor.broker_id);
			//if (NULL==pPlatormServer)
			//{
			//	return -1;
			//}
			//查询所有合约;
			//instrument_container::get_instance().get_names(loginedInvestor.instruments);
			//pPlatormServer->request(RtnDepthMarketData,&loginedInvestor.instruments);
		}
		break;
	case instrument::dataExchange:
		{
			PCOMMPACKET	pCommPacket	= reinterpret_cast<PCOMMPACKET>(lParam);
			if (pCommPacket && pCommPacket->m_dwDataType==instrument::dataExchange)
			{
				std::vector<EXCHANGE>* pExchagne=reinterpret_cast<std::vector<EXCHANGE>* >(pCommPacket->m_pData);
				if (pExchagne)
				{
					if (GetDefault())
					{
						GetDefault()->StoreExchange(*pExchagne);
					}
					//查询该交易所的所有数据;
				}
			}
		}
		break;
	default:
		break;
	}
	return 0;
}

int DataStoreManager::handler( WPARAM wParam )
{
	
	switch(wParam)
	{
	case OnThreadTimerEvent:
		{
			//定时器任务;
			DEBUG_METHOD();
			DEBUG_MESSAGE(std::endl
				<<"-----------数据库负载-----------"<<std::endl
				<<"---------队列中的事件数:      "<<this->m_EventQueue.size()<<std::endl
				<<"---------未处理的Tick数:      "<<this->m_qReport.size()<<std::endl
				<<"---------未处理的Minute数:    "<<this->m_qMinute.size()<<std::endl
				<<"---------存储器个数:          "<<this->m_mapStores.size()<<std::endl
				<<"---------缓存里分钟数据:      "<<m_mapPreMinuteCache.size()<<std::endl
				<<"-----------数据库负载-----------");
			break;
		}
	case RtnDepthMarketData:
		{
			std::unique_lock<std::mutex> lock(m_mutex_report);
			std::map<std::string,IDataStore* >::iterator it=m_mapStores.begin();
			while(it!=m_mapStores.end())
			{
				if (m_qReport.size()>0)
				{
					it->second->StoreReport(&m_qReport[0],m_qReport.size(),false);
				}
				++it;
			}
			m_qReport.clear();
		}
		break;
	case instrument::dataMinute:
		{
			std::unique_lock<std::mutex> lock(m_mutex_minute);
			std::map<std::string,IDataStore* >::iterator it=m_mapStores.begin();
			std::map<std::string,minute_container> temp_minute_container;
			while(it!=m_mapStores.end())
			{
				//分钟数据归类;
				while(!m_qMinute.empty())
				{
					MINUTE& cur_min=m_qMinute.front();
					std::map<std::string,minute_container>::iterator mincontiter= temp_minute_container.find(cur_min.szCode);
					if(mincontiter==temp_minute_container.end())
					{
						minute_container tmp_container;
						std::pair<std::map<std::string,minute_container>::iterator,bool> ret=
							temp_minute_container.insert(std::pair<std::string,minute_container>(cur_min.szCode,tmp_container));
						if (ret.second)
						{
							mincontiter=ret.first;
						}
					}
					mincontiter->second.InsertMinuteSort(cur_min);
					m_qMinute.pop();
				}
				std::map<std::string,minute_container>::iterator ret_min_iter =  temp_minute_container.begin();
				while (ret_min_iter!=temp_minute_container.end())
				{
					kdata_container kdmin1(ktypeMin);
					ret_min_iter->second.ToKData(kdmin1);
					it->second->StoreKData(kdmin1);
					it->second->StoreMinute(ret_min_iter->second.data(),ret_min_iter->second.size());
					++ret_min_iter;
				}
				++it;
			}
		}
		break;
	case instrument::dataBasetable:
		{
			std::map<std::string,IDataStore* >::iterator it=m_mapStores.begin();
			while(it!=m_mapStores.end())
			{
				it->second->StoreBasetable(instrument_container::get_instance());
				++it;
			}
		}
		break;
	case instrument::dataK:
		{
			std::map<std::string,IDataStore* >::iterator it=m_mapStores.begin();
			while(it!=m_mapStores.end())
			{
// 				if (!m_kdmin1.empty())
// 				{
// 					it->second->StoreKData(m_kdmin1);
// 				}
// 				if (!m_kdmin5.empty())
// 				{
// 					it->second->StoreKData(m_kdmin5);
// 				}
// 				if (!m_kdday.empty())
// 				{
// 					it->second->StoreKData(m_kdday);
// 				}
				++it;
			}
		}
		break;
	default:
		return false;
	}
	return true;
}

bool DataStoreManager::isEnable()
{
	return !m_mapStores.empty();
}

std::string DataStoreManager::name() const
{
	return "[DataStoreManager]";
}

void DataStoreManager::on_start()
{
// 	IDataStore* sqlite3_store=IDataStore::CreateStore("G:\\workspace\\VS2008\\MiningVictory\\build\\debug\\data",IDataStore::dbtypeSqlite3);
// 	/*DataStoreManager::GetInstance()->*/Add(sqlite3_store);
	//启动定时器;
	startTimer(1000*ktypeMin);
}

void DataStoreManager::on_stop()
{
	stopTimer();
}


