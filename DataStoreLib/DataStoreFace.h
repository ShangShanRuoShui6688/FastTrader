#ifndef _DATASTORE_FACE_H_
#define _DATASTORE_FACE_H_

#include "DataStoreLib.h"
#include "IDataStore.h"
#include <queue>
#include <unordered_map>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/thread.hpp>
#include <boost/signals2.hpp>

struct IndexInstrumentData 
{
	std::string IndexID; //指数的名称;
	boost::shared_ptr<InstrumentData> IndexInstrumentPtr; //指数数据;
	std::map<std::string, boost::shared_ptr<InstrumentData> > SubInstrumentMap;//成份数据;
	std::map<std::string, boost::shared_ptr<InstrumentData> > CurSubInstrumentMap;//成份数据;
	boost::shared_ptr<Tick> LastTickPtr;//指数的最新行情;
};

//数据存储与转换器;
class DATASTORE_API DataStoreFace:public boost::noncopyable
{
public:
	DataStoreFace();
	void SetDataStore(boost::shared_ptr<IDataStore> dataStorePtr);

	boost::shared_ptr<IDataStore> GetDataStore();

	void SetStoreToDB(bool bStoreMarket);

	bool IsStoreToDB();

	void UpdateTick(boost::shared_ptr<Tick> tick);
	void UpdateExchange(boost::shared_ptr<exchange_t> exchange);
	void UpdateInstrument(boost::shared_ptr<Instrument> instrument);
	void UpdateStatus(boost::shared_ptr<InstStatus> instStatus);

	boost::shared_ptr<InstStatus> GetStatus(const std::string& InstrumentID,
		const std::string& UpdateTime);

	bool IsDayOrNight();
	//注册数据;
	int PrepareData(boost::shared_ptr<InstrumentData> pInstrument, int enKtype, int period = ktypeDay, int nLength = DEFAULT_LOAD_MIN1_COUNT, bool bReload = false);

	boost::shared_ptr<InstrumentData> PrepareData(const std::string& InstrumentID, int enKtype, int period = ktypeDay, int nLength = DEFAULT_LOAD_MIN1_COUNT, bool bReload = false);
	//注册指数;
	boost::shared_ptr<InstrumentData> PrepareIndexData(const std::string& IndexID, int enKtype,
		int period = ktypeDay, bool bReload = false);

	//获取策略里用到的K线数据;
	boost::shared_ptr<InstrumentData> GetRegInstrument(const std::string& InstrumentID);
	//行情发生;
	boost::signals2::signal<void(boost::shared_ptr<Tick>)> sigOnTick;
protected:
	std::vector<boost::shared_ptr<Tick> > m_qReport;//分时数据;
	std::vector<boost::shared_ptr<MINUTE> > m_qMinute;//分钟数据;

	//订阅的数据;
	std::map<std::string, boost::shared_ptr<InstrumentData> > m_RegInstruments;
	std::map<std::string, boost::shared_ptr<IndexInstrumentData> > m_RegIndex;
	//合约状态默认项;
	std::map < std::string, std::vector<boost::shared_ptr<InstStatus> > > m_LocalStatus;
	std::map<std::string, boost::shared_ptr<InstStatus> > m_LastStatus; 
	bool m_bStoreDB;
public:
	~DataStoreFace();
	bool start();
	void stop();
	bool join();
protected:
	void HandleTick(boost::shared_ptr<Tick> tick);
	void HandleTick4Index(boost::shared_ptr<Tick> tick);
	void HandleMinute(boost::shared_ptr<MINUTE> minute);
	void HandleStore();
	//定时器;
	void HandleTimer();
	void HandleExchangeTimer(const std::string& exchange_id,bool is_open);
	void StartStoreTimer();
protected:
	boost::shared_ptr<IDataStore> m_DataStorePtr;
	//上一个minute数据;
	std::unordered_map<std::string,boost::shared_ptr<MINUTE> > m_mapPreMinuteCache;
	//上一个report数据;
	std::unordered_map<std::string, boost::shared_ptr<Tick> > m_mapPreReportCache;

	boost::shared_ptr<boost::asio::io_service> m_IoService;
	boost::shared_ptr<boost::asio::io_service::work> m_EmptyWork;
	boost::shared_ptr<boost::thread> m_IoThread;
	//定时器;
	boost::shared_ptr<boost::asio::steady_timer> m_Timer;
	//交易所定时器;
	std::map<std::string, boost::shared_ptr<boost::asio::steady_timer> > m_ExchangeTimer;
	//品种定时器;
	std::map<std::string, boost::shared_ptr<boost::asio::steady_timer> > m_ProductTimer;

	boost::posix_time::time_duration m_CloseTime;
};
#endif
