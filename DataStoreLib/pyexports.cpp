#include <boost/python.hpp>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include "DataStoreFace.h"

BOOST_PYTHON_MODULE(DataStoreLib)
{
	using namespace boost::python;

	class_<DataStoreFace, boost::shared_ptr<DataStoreFace>, boost::noncopyable >("IStrategy", no_init)
		.def("GetRegInstrument", &DataStoreFace::GetRegInstrument)
		;
}