#ifndef _DATASTORE_MANAGER_H_
#define _DATASTORE_MANAGER_H_

#include "DataStoreLib.h"
#include "IDataStore.h"
#include "../ServiceLib/Service/StkReceiver.h"
#include "../FacilityBaseLib/Container.h"
#include "../ServiceLib/observerwiththread.h"
#include <queue>
#include <thread>
#include <mutex>


class DATASTORE_API DataStoreManager:public IObserverWithThread
{
public:
	static DataStoreManager* GetInstance();
	virtual ~DataStoreManager();
protected:
	static DataStoreManager* gInstance;
	std::map<std::string,IDataStore* > m_mapStores;
	DataStoreManager();
public:
	virtual bool SetDefault(IDataStore* default_data_store);
	virtual IDataStore* GetDefault();
	virtual bool Add(IDataStore* store);
	virtual bool Remove(const std::string& key);
	virtual bool Remove(IDataStore* store);
	virtual void Clear();
	virtual IDataStore* Find(const std::string& key);
protected:
	std::vector<Tick> m_qReport;//分时数据;
	//分钟数据的队列;
	std::queue<MINUTE> m_qMinute;
public:
	virtual int post_event( WPARAM wParam,LPARAM lParam );
	virtual int handler( WPARAM wParam );

	virtual bool isEnable();

	virtual std::string name() const;

	virtual void on_start();

	virtual void on_stop();

protected:
	std::mutex m_mutex_report;
	std::mutex m_mutex_minute;
	//上一个minute数据;
	std::map<std::string,MINUTE> m_mapPreMinuteCache;
};
#endif
